__title__ = 'Regions & conductivities'
__description__ = 'This tutorial details how to assign different conductivities to different parts of a simulated tissue slice using region-wise tagging'
__image__ = '/images/02_05B_Conductive_Heterogeneity_Fig4_nonplanar_stim.png'
__q2a_tags__ = 'experiments,examples,tissue,region,conductivity'