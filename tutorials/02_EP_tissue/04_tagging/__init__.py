__title__ = 'Tissue regions'
__description__ = 'Regions are used to manage the assignment of heterogeneous tissue properties. This tutorial explains the different approches of how regions can be defined.'
__image__ = '/images/02_04_retag-vols.png'
__q2a_tags__ = 'experiments,examples,tissue,region,mesher'