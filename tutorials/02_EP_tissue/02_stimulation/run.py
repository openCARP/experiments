#!/usr/bin/env python

"""

.. _tutorial_extracellular-stimulation:

To run the experiments of this tutorial change directories as follows:

.. code-block:: bash

   cd ${TUTORIALS}/02_EP_tissue/02_stimulation 
   


Extracellular stimulation
=========================

Transmembrane voltage can be changed by applying an extracellular field in the extracellular space.
As outlined in Sec. :ref:`electrical-stimulation`,
an electric field can be set up either by injection/withdrawal of currents in the extracellular space
or by changing the extracellular potential with voltage sources.

For testing the various types of extracellular stimulation
we generate a thin strand of tissue of 1 cm length. 
Electrodes are located at both caps of the strand 
with an additional auxiliary electrode in the very center of the strand.
An illustration of the setup is given in :numref:`fig-extra-stim-setup`.
Extracellular voltage and current stimuli are pre-configured to generate an extracellular voltage drop
across the strand of about 2 V. This corresponds to an electric field magnitude of 2 V/cm
which is sufficient to initiate action potential propagation under the cathode.

.. _fig-extra-stim-setup:

.. figure:: /images/02_02_ExtracellularStimulationSetup.png
   :width: 50 %
   :align: center

   Simple setup for testing the various configurations for extracellular stimulation.
   Three electrodes are pre-defined, electrode A and B 
   located at the left hand face and right hand face of the tissue strand, respectively,
   and an additional auxiliary electrode C sitting on top of the strand in its center.



Experiments
-----------

Several types of stimulation setups are predefined. Run

.. code-block:: bash

   ./run.py --help 

to see all exposed experimental parameters. The input parameter **stimulus** selects a stimulus configuration among the following available options:

::

   --stimulus {extra_V,extra_V_bal,extra_V_OL,extra_I,extra_I_bal,extra_I_mono}
                         pick stimulus type

   --grounded {on,off}   turn on/off use of ground wherever possible


The geometry of the three electrodes A, B and C are defined as follows:

:: 
  
   # electrode A
   stim[0].elec.p0[0] = -5050.
   stim[0].elec.p1[0] = -4950. 
   stim[0].elec.p0[1] = -100.
   stim[0].elec.p1[1] = 100.
   stim[0].elec.p0[2] = -100. 
   stim[0].elec.p1[2] = 100. 
                  
   # electrode B
   stim[1].elec.p0[0] = 4950.
   stim[1].elec.p1[0] = 5050.
   stim[1].elec.p0[1] = -100.
   stim[1].elec.p1[1] = 100.
   stim[1].elec.p0[2] = -100.
   stim[1].elec.p1[2] = 100.

   # electrode C
   stim[2].elec.p0[0] = -10.
   stim[2].elec.p1[0] = 10.
   stim[2].elec.p0[1] = -5000.
   stim[2].elec.p1[1] = 5000.
   stim[2].elec.p0[2] = -5000.
   stim[2].elec.p1[2] = 5000.


   


.. _exp-stim-extra-V:

**Experiment exp01 (extracellular voltage stimulation)**
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The stimulus option ``extra_V`` sets up stimulation 
through application of extracellular voltage corresponding to :numref:`fig-extraV-wgnd`. 
The potential electrode A is clamped to 2000 mV for a duration of 2 ms,
electrode B is grounded, i.e., :math:`\phi_{\mathrm e} = 0`.
Electrode C is not used here, electrodes A and B are configured as follows:

::

   numstim = 2   # use two electrodes, A and B

   # electrode A
   stim[0].crct.type = 2        # extracellular voltage stimulus
   stim[0].pulse.strength = 2e3      # voltage at A in mV 

   # electrode B
   stim[1].crct.type = 3        # extracellular ground


Run this experiment by executing

.. code-block:: bash

   ./run.py --stimulus extra_V --visualize


.. _exp-stim-extra-V_bal:

**Experiment exp02 (balanced extracellular voltage stimulation)**
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

As in experiment exp01, the stimulus option ``extra_V_bal`` sets up stimulation
through the application of extracellular voltage. 
The potential on the left cap is clamped to 1000 mV for a duration of 2 ms
and the right hand electrode to -1000mV. This stimulation circuit corresponds to 
the setup shown in :numref:`fig-extraV-symm-wgnd`.

::

   numstim = 3   # use all three electrodes, A, B and C

   # electrode A
   stim[0].crct.type =  2       # extracellular voltage stimulus
   stim[0].pulse.strength =  1e3     # voltage at A in mV 

   # electrode B
   stim[1].crct.type =  2       # extracellular voltage stimulus
   stim[1].pulse.strength = -1e3     # voltage at B in mV

   # electrode C
   stim[2].crct.type =  3       # extracellular ground at C
     

Realize this experiment by running

.. code-block:: bash

   ./run.py --stimulus extra_V_bal --visualize


.. _exp-stim-extra-V_OL:

**Experiment exp03 (extracellular voltage stimulation with circuit break)**
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

As in experiment exp01, the stimulus option ``extra_V_OL`` sets up stimulation 
through the application of extracellular voltage. 
The extracellular potential at electrode A is clamped to 2000 mV for a duration of 2 ms.
In contrast to the ``extra_V`` electrode A is disconnected from the source after stimulus delivery.
Therefore, the potential at A is allowed to float after the end of the stimulus,
that is, electrode A won't act as a ground after delivery of the voltage pulse.
This stimulation circuit corresponds to the setup shown in :numref:`fig-extraV-wgnd-switched`.

::

   numstim = 2   # use two electrodes, A and B

   # electrode A
   stim[0].crct.type = 5        # extracellular voltage stimulus, switched
   stim[0].pulse.strength = 2e3      # voltage at A in mV 

   # electrode B
   stim[1].crct.type = 3        # extracellular ground


To realize this scenario, run

.. code-block:: bash

   ./run.py --stimulus extra_V_OL --visualize


.. _exp-stim-extra-I:

**Experiment exp04 (extracellular current stimulation)**
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The stimulus option ``extra_I`` sets up stimulation 
through the application of an extracellular current. 
The current is injected into electrode A in the extracellular medium and withdrawn at electrode B which, in turn, is grounded.
The extracellular current strength is chosen to induce an extracellular potential drop of 2000 mV 
across the strand as before with extracellular voltage stimulation in ``extra_V``. 
This configuration corresponds to the setup shown in :numref:`fig-extraI-wGND`.
Electrode C is not used here, while electrodes A and B are configured as follows:

::

   numstim = 2   # use two electrodes, A and B

   # electrode A
   stim[0].crct.type = 1        # extracellular current stimulus
   stim[0].pulse.strength = 3.1e6    # current density at A in :math:`\mu A/cm^3`

   # electrode B
   stim[1].crct.type = 3        # extracellular ground


To see the result, run

.. code-block:: bash

   ./run.py --stimulus extra_I --visualize


.. _exp-stim-extra-I-bal:

**Experiment exp05 (balanced extracellular current stimulation)**
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Similar to experiment exp04, the stimulus option ``extra_I_bal`` sets up stimulation 
through the injection of extracellular current 
with the difference being that B is not grounded. Extracellular current is injected into electrode A
and the same amount is withdrawn from electrode B.
As this constitutes a pure `Neumann problem <https://en.wikipedia.org/wiki/Neumann_boundary_condition>`_, two scenarios can be considered.
Either electrode C is used as a grounding electrode corresponding to :numref:`fig-extraI-symm-wgnd`,
or we refrain from defining an explicit grounding electrode and use a diffuse ground,
that is, we apply an additional constraint to deal with the `nullspace <https://en.wikipedia.org/wiki/Kernel_(linear_algebra)>`_ of the problem.
The latter option corresponds to :numref:`fig-extraI-symm-diffuse`.
Both configurations also induce an extracellular potential drop of 2000 mV across the strand,
albeit in a symmetric way.
Note that in both cases, the compatibility condition must be satisfied.
This is taken care of by CARPentry internally by balancing the total current injected/withdrawn
through electrodes A and B. If balancing is imperfect, a stimulation would also occur around electrode C
as all the excess current would drain there.

In the setup with an explicit ground, electrodes are defined as:

::

    numstim = 3   # use all three electrodes, A, B and C

    # electrode A
    stim[0].crct.type =  1       # extracellular voltage stimulus
    stim[0].pulse.strength =  3.1e6   # current density at A in :math:`\mu A/cm^3` 

    # electrode B
    stim[1].crct.type =  1       # extracellular voltage stimulus
    stim[1].crct.balance  =  0       # no strength prescribed, balance with stimulus[0]

    # electrode C
    stim[2].crct.type =  3       # extracellular ground at C


To solve the pure Neumann problem without an explicit ground, the input parameter ``grounded`` is set to ``on``.
In fact, CARPentry turns on this mode automatically if a pure Neumann configuration is detected.
   

::

    numstim = 2   # use electrodes A and B 

    # electrode A
    stim[0].crct.type =  1       # extracellular voltage stimulus
    stim[0].pulse.strength =  3.1e6   # current density at A in :math:`\mu A/cm^3` 

    # electrode B
    stim[1].crct.type =  1       # extracellular voltage stimulus
    stim[1].crct.balance  =  0       # no strength prescribed, balance with stimulus[0]

In order to finally execute the last experiment, run

.. code-block:: bash

   ./run.py --stimulus extra_I_bal --visualize

"""
import os
EXAMPLE_DESCRIPTIVE_NAME = 'Stimulation'
EXAMPLE_AUTHOR = 'Gernot Plank <gernot.plank@medunigraz.at>'
EXAMPLE_DIR = os.path.dirname(__file__)
GUIinclude = True

import sys
from datetime import date

from carputils import settings
from carputils import tools
from carputils import mesh

def parser():
    parser = tools.standard_parser()
    group  = parser.add_argument_group('experiment specific options')

    group.add_argument('--stimulus',
                        default='extra_V', 
                        choices=['extra_V',
                                 'extra_V_bal',
                                 'extra_V_OL',
                                 'extra_I',
                                 'extra_I_bal',
                                 'extra_I_mono'], 
                        help='pick stimulus type')
    group.add_argument('--grounded',
                        default='on', 
                        choices=['on','off'], 
                        help='turn on/off use of ground wherever possible')
    return parser

def jobID(args):
    """
    Generate name of top level output directory.
    """
    today = date.today()
    return '{}_stim_{}_{}_np{}'.format(today.isoformat(), args.stimulus,
                                       args.flv, args.np)

@tools.carpexample(parser, jobID)
def run(args, job):

    # Generate mesh
    # Block which is thin in z direction
    geom = mesh.Block(size=(10, 0.1, 0.1))
    # Set fibre angle to 0, sheet angle to 0
    geom.set_fibres(0, 0, 90, 90)
    # Generate and return base name
    meshname = mesh.generate(geom)
    del geom

    # define electrode locations
    lstm = ['-stim[0].elec.p0[0]', -5050.,
            '-stim[0].elec.p1[0]', -4950.,
            '-stim[0].elec.p0[1]', -100.,
            '-stim[0].elec.p1[1]', 100.,
            '-stim[0].elec.p0[2]', -100.,
            '-stim[0].elec.p1[2]', 100.]
    
    rstm = ['-stim[1].elec.p0[0]', 4950.,
            '-stim[1].elec.p1[0]', 5050.,
            '-stim[1].elec.p0[1]', -100.,
            '-stim[1].elec.p1[1]', 100.,
            '-stim[1].elec.p0[2]', -100.,
            '-stim[1].elec.p1[2]', 100.]
    
    astm = []
    mod = []

    # tags generated with the above command
    IntraTags = [1]
    ExtraTags = [1]

    # make subtest specific settings
    if args.stimulus == 'extra_V':
        # left electrode imposes extracellular voltage
        lstm += ['-stim[0].crct.type', '2',
                 '-stim[0].pulse.strength', '2e3']

        if args.grounded == 'off':
            rstm = []

    elif args.stimulus == 'extra_V_bal':
        # left electrode imposes extracellular voltage
        lstm += ['-stim[0].crct.type', '2',
                 '-stim[0].pulse.strength', '1e3']
        rstm += ['-stim[1].crct.type', '2',
                 '-stim[1].pulse.strength','-1e3']

        if args.grounded == 'on': 
            astm += ['-stim[2].crct.type', '3']
        
    elif args.stimulus == 'extra_V_OL':
        # left electrode imposes extracellular voltage
        # circuit open before and after stimulus delivery
        lstm += ['-stim[0].crct.type', '5',
                 '-stim[0].pulse.strength', '2e3']

        if args.grounded == 'off':
            rstm = []

    elif args.stimulus == 'extra_I':
        # left electrode injects extracellular current
        # right electrode is grounded
        lstm += ['-stim[0].crct.type', '1',
                 '-stim[0].pulse.strength', '3.1e6']

    elif args.stimulus == 'extra_I_bal':
        # left electrode injects extracellular current
        # right electrode withdraws extracellular current
        # auxiliary electrode in the middle is grounded
        lstm += ['-stim[0].crct.type', '1',
                 '-stim[0].pulse.strength', '-3.1e6']
        rstm += ['-stim[1].crct.type', '1',
                 '-stim[1].crct.balance',  '0']
        if args.grounded == 'on': 
            astm += ['-stim[2].crct.type', '3']

    elif args.stimulus == 'extra_I_mono':
        # left electrode injects extracellular current
        # right electrode withdraws extracellular current
        # auxiliary electrode in the middle may be grounded
        # but we run a monodomain simulation
        lstm += ['-stim[0].crct.type', '1',
                 '-stim[0].pulse.strength', '3.1e6']
        rstm += ['-stim[1].crct.type', '1',
                 '-stim[1].crct.balance',  '0']
        mod  += ['-extracell_monodomain_stim', '1',
                 '-bidomain',                  '0']
  
        if args.grounded == 'on':
            astm += ['-stim[2].crct.type', '3']
        
    else:
        raise Exception('Unknown stimulus type!')

    # Add all the non-general arguments
    cmd = tools.carp_cmd(os.path.join(EXAMPLE_DIR, 'stimtest.par'))

    cmd += ['-meshname', meshname,
            '-simID',    job.ID]
    cmd += tools.gen_physics_opts(ExtraTags=ExtraTags, IntraTags=IntraTags)

    # add PDE solver options
    cmd += lstm + rstm + astm

    if args.visualize:
        cmd += ['-gridout_i', 3,
                '-gridout_e', 3]
    
    # Run simulations
    job.carp(cmd)

    
    # Do visualization
    if args.visualize and not settings.platform.BATCH:

        geom = os.path.join(job.ID, os.path.basename(meshname)+'_i')
        data = os.path.join(job.ID, 'phie.igb')
        view = os.path.join(EXAMPLE_DIR, 'stimulation.mshz')
        
        job.gunzip(data)

        # GUI
        if args.webGUI:
            folderNameExp = f'/experiments/02_stimulation_{job.ID}'
            os.mkdir(folderNameExp)
            cmdMesh = f'meshtool collect -imsh={geom} -omsh={folderNameExp}/{job.ID} -nod={data} -ifmt=carp_txt -ofmt=ens_bin'
            outMesh = os.system(cmdMesh)
            if(outMesh == 0):
                print('Meshtool - conversion successfully')
            else:
                print('Meshtool - conversion failure')
        
        # Meshalyzer
        else:
            job.meshalyzer(geom, data, view)



if __name__ == '__main__':
    run()
