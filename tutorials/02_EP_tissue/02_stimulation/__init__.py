__title__ = 'Extracellular stimulation'
__description__ = 'In this example you learn how to stimulate a tissue from the extracellular space'
__image__ = '/images/02_02_ExtracellularStimulationSetup.png'
__q2a_tags__ = 'experiments,examples,stimulus,tissue'