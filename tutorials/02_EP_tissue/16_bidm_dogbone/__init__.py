__title__ = 'Anisotropy effects'
__description__ = 'Unequal anisotropy ratios can be responsible for the formation of unexpectedly complex polarization patterns'
__image__ = '/images/02_16_dogbone_results.png'
__q2a_tags__ = 'experiments,examples,tissue'