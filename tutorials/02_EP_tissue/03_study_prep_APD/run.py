#!/usr/bin/env python

"""
.. _tutorial_adjusting_APD:

This tutorial demonstrates how to adjust ionic model parameters to generate a specific action potential duration in your simulations. To run the experiments of this tutorial change directories as follows:

.. code-block:: bash

   cd ${TUTORIALS}/02_EP_tissue/03_study_prep_APD 



Introduction
=========================

In mammalian hearts, cardiac repolarization varies between species, is spatially heterogeneous, and altered during disease. Thus, it is important to calibrate your ionic models to account for these repolarization differences. The easiest way to do this is to modify the maximum conductance for ionic currents known to alter action potential duration (APD), i.e., the time duration a cardiac myocyte repolarizes after excitation. Ideally, these values are obtained from experiments in the literature. In this exercise, you will learn how to look up the ionic current variables in your model that you can change to alter APD, and how to adjust in cable/tissue models.


Ionic model
-----------

For this example, we use the most recent version of the ten Tusscher-Panfilov ionic model for human ventricular myocytes [#Tusscher2006]_. This ionic model is labeled tenTusscherPanfilov in openCARP's LIMPET library.

1D cable model
--------------

For this exercise we compute APD in a 1.5 cm cable of epicardial ventricular myocytes. The model domain was discretized with linear finite elements with an average edge length of 0.02 cm. 

Pacing protocol
---------------

The left side of the 1D cable model is paced with 5-ms-long stimuli at twice capture amplitude for a basic cycle length and number of beats chosen by the user. 


Action potential duration 
-------------------------

Activation potential duration is computed at 80% repolarization (:math:`APD_{80}`) according to [Bayer2016]_. This is achieved by using the igbutils function igbapd as illustrated below. See the igbutils tutorial for more information on using igbapd. 

.. code-block:: bash

         igbapd -t nbeats-1*bcl --repol=80 --vup=-10 --peak-value=plateau --plateau-start=15 ./vm.igb


Usage
=========================

The following optional arguments are available (default values are indicated):

.. code-block:: bash

  ./run.py --help 
    --Mode              Options: {default,adjust}, Default: default
                        When in adjust mode, will use im_param options by user
    --im_param          Is a string for which you can input operations to 
                        perform to specific variables in a chosen ionic model. 
                        For example, 'Gmax_1*0.4,Gmax_2=0.4'
    --IMP               Ionic model name from LIMPET library. 
    --nbeats            Default: 2
                        Number of beats to perform before APD computation. In 
                        reality, this should be much larger to reach 
                        steady-state. Due to tutorial time constraints, it is 
                        very small.
    --bcl               Default: 700
                        Basic cycle length of pacing

Tasks
=========================

1. List all of the ionic channel conductances in the ten Tusscher-Panfilov model by running the command below

.. code-block:: bash

  bench --imp-info -I tenTusscherPanfilov

2. Determine the baseline APD by running the following command.

.. code-block:: bash

  ./run.py --Mode default --IMP tenTusscherPanfilov

Note, you can look at the values for the APD on each node of the cable by scrolling through the vertex values under the Highlight tab in meshalyzer.

3. Shorten APD by >30 milliseconds by adjusting the maximal conductance for the slow outward potassium current (Iks). What is the name of the variable from the output in task 1? Did you have to increase or decrease the value? 

4. This time, prolong APD by >30 altering the plateau of the action potential by modifying the maximal conductance of the L-type calcium channel current (ICaL).

In the future, you will want to run many more beats than 2. Also, the process can be automated similar to tuneCV.

.. rubric:: References

.. [#Tusscher2006] ten Tusscher KHWJ, Panfilov AV.
                 **Alternans and spiral breakup in a human ventricular tissue model.**
                 *Am J Physiol Heart Circ Physiol*, 291(3):H1088-H1100, 2006.
                 `[Pubmed] <https://www.ncbi.nlm.nih.gov/pubmed/16565318>`__
                 

"""
import os
EXAMPLE_DESCRIPTIVE_NAME = 'Adjusting action potential duration'
EXAMPLE_AUTHOR = 'Jason Bayer <jason.bayer@ihu-liryc.fr>'
EXAMPLE_DIR = os.path.dirname(__file__)
GUIinclude = True

from datetime import date
from carputils import settings
from carputils import tools

def parser():
    # Generate the standard command line parser
    parser = tools.standard_parser()
    group  = parser.add_argument_group('experiment specific options')
    # Add arguments
    group.add_argument('--Mode',
                        default = 'default',
                        choices = ['default', 'adjust'],
                        help = 'Use default or adjustment parameters')
    group.add_argument('--im_param',
                        type = str,
                        default = '',
                        help = 'List of variable modifications for the ionic currents that you want to change')
    group.add_argument('--IMP',
                        type = str,
                        default = 'tenTusscherPanfilov',
                        help = 'Ionic model name from limpet directory')
    group.add_argument('--nbeats',
                        type = int,
                        default = 2,
                        help = 'Number of beats to before outputing APD')
    group.add_argument('--bcl',
                        type = int,
                        default = 700,
                        help = 'Basic cycle length of pacing')
    return parser

def jobID(args):
    today = date.today()
    ID = '{}_nbeats_{}_bcl_{}'.format(today.isoformat(), args.nbeats, args.bcl)
    return ID


@tools.carpexample(parser, jobID, clean_pattern='{}*|(.trc)'.format(date.today().year))
def run(args, job):
            
    #####################################
    #Perform the simulation
    #####################################
    cmd  = tools.carp_cmd(os.path.join(EXAMPLE_DIR, 'Mesh_1.5cm_Cable', 'sim.par'))

    cmd += ['-stim[0].elec.vtx_file', os.path.join(EXAMPLE_DIR, 'Mesh_1.5cm_Cable', 'stim')]
    cmd += ['-meshname', os.path.join(EXAMPLE_DIR, 'Mesh_1.5cm_Cable', 'mesh')]

    if args.Mode == 'default':
        simid = os.path.join(job.ID, 'APD_default')
        cmd += ['-simID', simid,
                '-imp_region[0].im', args.IMP,
                '-stim[0].ptcl.bcl', args.bcl,
                '-stim[0].ptcl.npls', args.nbeats,
                '-tend', args.bcl*args.nbeats+0.1 ]

    if args.Mode == 'adjust':
        simid = os.path.join(job.ID, 'APD' + args.im_param)
        cmd += ['-simID', simid,
                '-imp_region[0].im', args.IMP,
                '-imp_region[0].im_param', str(args.im_param),
                '-stim[0].ptcl.bcl', args.bcl,
                '-stim[0].ptcl.npls', args.nbeats,
                '-tend', args.bcl*args.nbeats+0.1 ]

    #Run simulation
    job.carp(cmd)

    #calculate the apd
    cmd = [settings.execs.igbapd,
           '-t', (args.nbeats-1)*args.bcl,
           '--repol=80',
           '--vup=-10',
           '--peak-value=plateau',
           '--plateau-start=15',
           '--output-file={}'.format(os.path.join(simid, 'apd.dat')),
            os.path.join(simid, 'vm.igb')]
    job.bash(cmd)

    # GUI
    if args.webGUI:
        folderNameExp = f'/experiments/03_study_prep_APD_{job.ID}'
        datagui = os.path.join(simid, 'apd.dat')
        os.mkdir(folderNameExp)
        cmdMesh = f'meshtool collect -imsh=/tutorials/02_EP_tissue/03_study_prep_APD/Mesh_1.5cm_Cable/mesh -omsh={folderNameExp}/{job.ID} -nod={datagui} -ifmt=carp_txt -ofmt=ens_bin'
        outMesh = os.system(cmdMesh)
        if(outMesh == 0):
            print('Meshtool - conversion successfully')
        else:
            print('Meshtool - conversion failure')
    
    #Visualize with meshalyzer
    elif args.visualize and not settings.platform.BATCH:
        geom = os.path.join(EXAMPLE_DIR, 'Mesh_1.5cm_Cable', 'mesh')
        data = os.path.join(simid, 'apd.dat')
        view = os.path.join(EXAMPLE_DIR, 'Mesh_1.5cm_Cable', 'apd_state.mshz')
        job.meshalyzer(geom, data, view)
    
if __name__ == '__main__':
    run()
