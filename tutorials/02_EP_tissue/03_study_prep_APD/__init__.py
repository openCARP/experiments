__title__ = 'APD adjustment'
__description__ = 'This example demonstrates how to adjust ionic model parameters to generate a specific action potential duration in your simulations'
__q2a_tags__ = 'experiments,examples,tissue'