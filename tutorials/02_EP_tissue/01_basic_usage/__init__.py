__title__ = 'Basic tissue EP'
__description__ = 'This example introduces to the basics of using the openCARP executable for simulating EP at the tissue and organ scale'
__image__ = '/images/02_01_suprathreshold_stim_response.png'
__q2a_tags__ = 'experiments,examples,stimulus,tissue'