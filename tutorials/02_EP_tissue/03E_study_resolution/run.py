#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the 
# Apache License, Version 2.0 (the "License"); 
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#

#!/usr/bin/env python3
"""

Overview
========

.. code-block:: bash

    cd ${TUTORIALS}/02_EP_tissue/03E_study_resolution

This example implements the classic benchmark problem for electrophysiology introduced by Niederer et al. in 2011. [Niederer2011]_

Problem definition
==================

The geometry of the tissue model is defined by a cuboid with dimensions 20.0 x 7.0 x 3.0 mm. Fiber directions are defined to be oriented in the X axis of the cuboid.
All boundaries are assumed to have a zero-flux boundary condition. The stimulus current is delivered to a volume of 1.5 x 1.5 x 1.5 mm originating at the corner of the slab with coordinates :math:`x=y=z=0`.

.. _fig-geometry:

.. figure:: /images/02_03E_benchmark_geometry.png
   :width: 50 %
   :align: center

   Simulation domain for the electrophysiology problem. Shown are the dimensions and the evaluation points as well as the stimulus area (red box).

The ionic model that is used in the benchmark is the ten Tusscher & Panfilov model [TTP]_ with the epicardial cell model variant.
This is set in the base parameter file alongside a single cell surface-to-volume ratio of :math:`0.14 \\mu m^{-1}` and the portion of the volume that is occupied by cells (=1).
To use a given set of initial values for the state variables, we load the state variable file singlecell.sv with the parameter ``-imp_region[0].im_sv_init``.

.. code-block:: bash

    num_imp_regions = 1 
    imp_region[0].name = "Ventricle" 
    imp_region[0].im   = tenTusscherPanfilov
    imp_region[0].im_param = "flags=EPI"
    imp_region[0].cellSurfVolRatio = 0.14
    imp_region[0].volFrac = 1

The tissue is assumed to be transverse isotropic. Therefore, we set a single conductivity region with the intra- and extracellular conductivites in longitudinal and transversal directions.

.. code-block:: bash

    num_gregions    = 1
    gregion[0].g_il = 0.17
    gregion[0].g_it = 0.019
    gregion[0].g_in = 0.019
    gregion[0].g_el = 0.62
    gregion[0].g_et = 0.24
    gregion[0].g_en = 0.24

To set up the stimulus current, we define a box by providing the lower left corner :math:`p0 = (0,0,0)` and the upper right corner :math:`p1 = (1500,1500,1500)`.
The stimulation protocol is set to start at 0 s with a duration of 2 ms and a pulse strength of :math:`50000 \\mu A cm^{-3}`.

.. code-block:: bash

    num_stim                = 1
    stim[0].elec.geom_type  = 2
    stim[0].elec.p0[0]      = 0
    stim[0].elec.p0[1]      = 0
    stim[0].elec.p0[2]      = 0
    stim[0].elec.p1[0]      = 1500
    stim[0].elec.p1[1]      = 1500
    stim[0].elec.p1[2]      = 1500
    stim[0].pulse.strength  = 35.71
    stim[0].ptcl.duration   = 2.00
    stim[0].ptcl.start      = 0.00

A quantitative comparison of the benchmark simulations is achieved by comparing activation times at certain points of the geometry and along a diagonal line from one corner of the cuboid to another.
An activation time is recorded at the time the membrane potential passes through 0 mV for the first time.
This is achieved by setting the following parameters:

.. code-block:: bash

    num_LATs             =         1
    lats[0].measurand    =         0 
    lats[0].all          =         0
    lats[0].mode         =         0
    lats[0].threshold    =         0.
    lats[0].method       =         1

Usage
=====

The experiment specific options are:

.. code-block:: bash

    --tend
                duration of the simulation in ms

    --dx
                resolution of the mesh in um to be used for benchmark

    --dt
                temporal resolution in us
    
    --massLumping
                toggle mass massLumping on (1) or use full mass matrix (0)

    --plot
                generate a plot of activation times along the block diagonal
    
    --compare
                run benchmark with --dx 100 200 and 500 and make comparison plot


The user can either run single experiments by calling ``python3 run.py`` with the optional arguments ``--tend``, ``--dx``, ``--dt``, and ``--massLumping``
or run the entire benchmark setup with the argument ``--compare``.

--compare
---------

With this option you will run all simulations required for the benchmark and create a comparison plot for the activation times 
on the block diagonal with :math:`\Delta x = 0.5, 0.2, 0.1` mm resolution.

.. _fig-comparison:

.. figure:: /images/02_03E_benchmark_fullMass_results.png
   :width: 50 %
   :align: center

   Simulated activation times on the block diagonal with :math:`\Delta x = 0.5, 0.2, 0.1` mm resolution.

Notice that the accuracy of the solution deteriorates with lower resolution meshes.

--compare with mass lumping
---------------------------

Now we will run the benchmark setting with the additional parameter ``--massLumping 1`` to see how mass lumping affects the solution in the otherwise unchanged setup.

.. _fig-comparison:

.. figure:: /images/02_03E_benchmark_lumpedMass_results.png
   :width: 50 %
   :align: center

   Simulated activation times on the block diagonal with :math:`\Delta x = 0.5, 0.2, 0.1` mm resolution using mass lumping.

Mass lumping is a common numerical technique in FEM to speed up computation.
However, notice how the accuracy of the solution deteriorates even faster than before. 
This was one of the beneficial outcomes of the benchmark, which showed that mass lumping was leading to the biggest inaccuracies among finite element codes especially with lower spatial resolutions.
Therefore, you should always remember to consider the tradeoffs of this numerical technique.

.. rubric:: References

.. [Niederer2011]   *Niederer S. A. et al.*,
                    **Verification of cardiac tissue electrophysiology simulators using an <i>N</i>-version benchmark**, 
                    Philosophical Transactions of the Royal Society A: Mathematical, Physical and Engineering Sciences 369, 4331-4351, 2011.
                    `[ePrint] <https://royalsocietypublishing.org/doi/pdf/10.1098/rsta.2011.0139>`_

.. [TTP]    *ten Tusscher, K. H. W. J. & Panﬁlov, A. V.*,
            **Alternans and spiral breakup in a human ventricular tissue model**,
            Am. J. Physiol. Heart Circ. Physiol. 291, H1088-H1100, 2006.
            `[ePrint] <https://journals.physiology.org/doi/full/10.1152/ajpheart.00109.2006>`_

"""
import os
import sys
from datetime import date
import numpy as np
from matplotlib import pyplot

from carputils import tools
from carputils import mesh
from carputils.carpio.txt import open as txt_open

EXAMPLE_DESCRIPTIVE_NAME = 'N-version benchmark of cardiac tissue electrophysiology'
EXAMPLE_AUTHOR = 'Moritz Linder <moritz.linder@kit.edu>, Tobias Gerach <tobias.gerach@kit.edu>'
EXAMPLE_DIR = os.path.dirname(__file__)

# provide python2 - python3 compatibility
isPy2 = True
if sys.version_info.major > 2:
    isPy2 = False
    xrange = range


def parser():
    parser = tools.standard_parser()
    benchmark = parser.add_argument_group('N-version benchmark specific options')
    benchmark.add_argument('--tend',
                        type=float, default=50.0,
                        help='duration of the simulation in ms')
    benchmark.add_argument('--dx',
                        type=float, default=100.0,
                        help='resolution of the mesh in um to be used for '
                            +'benchmark')
    benchmark.add_argument('--dt',
                        type=float, default=20.0,
                        help = 'temporal resolution in us')
    benchmark.add_argument('--massLumping',
                        type=int, default=0,
                        choices=[0, 1],
                        help='toggle mass massLumping on (1) or use full mass matrix (0)')
    benchmark.add_argument('--plot',
                        nargs='?', const=True,
                        help='generate a plot of activation times along the '
                            +'block diagonal')
    benchmark.add_argument('--compare',
                        action='store_true',
                        help='run benchmark with --dx 100 200 and 500 and make'
                            +' comparison plot')
    
    return parser

def jobID(args):
    """
    Generate name of top level output directory.
    """
    today = date.today()
    return '{}_benchmark_{}us_{}um_lumping{}'.format(today, args.dt, args.dx, args.massLumping)


@tools.carpexample(parser, jobID)
def run(args, job):

    # Generate mesh
    # Block according to the benchmark problem. P1 corner set to origin
    geom = mesh.Block(size=(20, 7, 3), resolution=args.dx/1000, centre=(10, 3.5, 1.5))
    # Set fibre angle to 0, sheet angle to 0
    geom.set_fibres(0, 0, 90, 90)

    # Generate and return base name
    meshname = mesh.generate(geom)

    # Use base parameter options from nversion.par and use predefined state variables
    cmd = tools.carp_cmd(os.getcwd() + '/nversion.par')
    cmd += ['-imp_region[0].im_sv_init', os.getcwd() + '/singlecell.sv']

    cmd += [
        '-simID',           job.ID,
        '-meshname',        meshname,
        '-tend',            args.tend,
        '-dt',              args.dt,
        '-mass_lumping',    args.massLumping
    ]
    
    # define physics regions
    # ------------------------------------------------------------------------------
    IntraTags = [1]
    ExtraTags = [1]

    cmd += tools.gen_physics_opts(ExtraTags=ExtraTags, IntraTags=IntraTags)

    # Run simulation
    job.mpi(cmd)

    # analysis not executed if done in --dry mode
    if args.dry:
        sys.exit(0)

    # open activation time file
    act_fname = os.path.join(job.ID, 'init_acts_vm_act-thresh.dat')

    # Get activation times on diagonal
    # Get arrangement of points in mesh
    point_dims = geom.shape()
    divs = point_dims - 1

    # Work out points on diagonal
    diag_divs = 1
    for i in xrange(1, divs.min()):
        remainder = divs % i
        if (remainder == 0).all():
            diag_divs = i

    step = divs / diag_divs

    nodes = []
    for i_step in xrange(diag_divs + 1):
        dix, diy, diz = step * i_step
        nodes.append(  int(dix
                     + diy * point_dims[0]
                     + diz * point_dims[0] * point_dims[1]))

    # Read activation data
    fp = txt_open(act_fname)
    act_times = fp.data()[nodes]
    fp.close()

    if args.plot is not None:
        # Plot activation times
        fig = pyplot.figure()
        ax = fig.add_subplot(1,1,1)
        ax.plot(np.arange(diag_divs + 1), act_times, 'x-')

        if isinstance(args.plot, str):
            # Save to file
            pyplot.savefig(args.plot, bbox_inches='tight')
        else:
            pyplot.show()

    return act_times


if __name__ == '__main__':

    args = parser().parse_args()

    if args.compare:
        # Copy all other parameters except dx
        argv = []
        for arg in sys.argv[1:]:
            if arg == '--compare':
                continue
            if arg in ['--dx', '--tend']:
                raise Exception('cannot set --dx or --tend with --compare')
            argv.append(arg)

        # Run sims and get times back
        times100 = run(argv + ['--dx', '100', '--tend', '50'])
        times200 = run(argv + ['--dx', '200', '--tend', '70'])
        times500 = run(argv + ['--dx', '500', '--tend', '150'])

        # Plot activation times
        fig = pyplot.figure()
        ax = fig.add_subplot(1,1,1)

        ax.plot(np.linspace(0, 21.4, len(times100)), times100, 'rx-',
                label='\u0394x = 0.1 mm')    # $\Delta x = 0.1\\mathrm{mm}$
        ax.plot(np.linspace(0, 21.4, len(times200)), times200, 'gx-',
                label='\u0394x = 0.2 mm')    # $\Delta x = 0.2\\mathrm{mm}$
        ax.plot(np.linspace(0, 21.4, len(times500)), times500, 'bx-',
                label='\u0394x = 0.5 mm')    # $\Delta x = 0.5\\mathrm{mm}$

        ax.set_xlabel('Distance along diagonal (mm)')
        ax.set_ylabel('Activation time (ms)')
        pyplot.legend(loc='upper left')

        pyplot.show()

    else:
        # Just run the simulation
        run()
