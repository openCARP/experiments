__title__ = 'Niederer et al. benchmark'
__description__ = 'This example replicates the Niederer et al. benchmark and illustrates effects of some numerical settings including temporal and spatial discretization'
__image__ = '/images/02_03E_benchmark_geometry.png'
__q2a_tags__ = 'experiments,examples,fibre,tissue,region'