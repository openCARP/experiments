__title__ = 'Tuning conduction velocity'
__description__ = 'This tutorial introduces the background for the relationship between conduction velocity and tissue conductivity'
__image__ = '/images/02_03A_tuneCV-Setup1D.png'
__q2a_tags__ = 'experiments,examples,tunecv,conductivity,tissue'