-88.3497            # Vm
-                   # Lambda
-                   # delLambda
-                   # Tension
-                   # K_e
-                   # Na_e
-                   # Ca_e
-0.00225686         # Iion
-                   # tension_component
LRDII_F
0.105365            # Ca_i
136.656             # K_i
12.3711             # Na_i
0.00102378          # b
3.52921e-06         # d
0.999572            # f
-0.000369165        # fdiff_i_Cai_old
0.993432            # g
0.99284             # h
0.348862            # i_Cai_old
0.995157            # j
1.2482              # jsr
0.000883693         # m
1.67489             # nsr
997.61              # tcicr
11000               # tjsrol
0.000134861         # xr
0.00516537          # xs1
0.0298761           # xs2
0.99978             # ydv
0.0119789           # zdv

INa_Bond_Markov
9.51077e-05         # C_Na1
0.0139036           # C_Na2
2.50677e-05         # I1_Na
0.00122661          # I2_Na
0.00349757          # IC_Na2
0.197221            # IC_Na3
2.39636e-05         # IF_Na
1.117e-07           # O_Na

