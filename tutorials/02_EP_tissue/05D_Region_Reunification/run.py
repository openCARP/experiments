#!/usr/bin/env python

"""
This example details how to output the values of state variables over time during a simulation using
the gvec[] interface. This example is a stub to demonstrate the most basic functionality and should
be expanded when possible. To run the experiments of this example change directories as follows:

.. code-block:: bash

   cd ${TUTORIALS}/02_EP_tissue/02_EP_tissue/05D_Region_Reunification
   


Problem Setup
================================

This example will run one simulation using a 2D sheet model (1 cm x 1 cm) that has been divided into
four regions, each of which uses a different ionic model. All four models have an implementation of
the fast sodium current (:math:`I_\mathrm{Na}`), but in one of the models the state variable names are
upper-case instead of lower-case (i.e., :math:`M/H/J` vs. :math:`m/h/j`). The gvec[] interface is
used to reunify these state variables from the four regions and output three IGB files: 
``I_Na_m.igb``, ``I_Na_h.igb``, and ``I_Na_j.igb``.

Usage
================================

No optional arguments are available.

If the program is run with the ``--visualize`` option, meshalyzer will automatically 
load the produced ``I_Na_m.igb`` file, which shows :math:`I_\mathrm{Na}(t)` for the entire
simulation domain. 

What's Going On Under The Hood?
================================

The relevant part of the .par file for this example is shown below:

.. code-block:: bash

    #############################################################
    num_gvecs                = 3
    #############################################################
    gvec[0].name             = INa_m
    gvec[0].ID[0]            = m
    gvec[0].ID[1]            = m
    gvec[0].ID[2]            = M # <== Note upper-case letter
    gvec[0].ID[3]            = m
    
    gvec[1].name             = INa_h
    gvec[1].ID[0]            = h
    gvec[1].ID[1]            = h
    gvec[1].ID[2]            = H # <== Note upper-case letter
    gvec[1].ID[3]            = h
    
    gvec[2].name             = INa_j
    gvec[2].ID[0]            = j
    gvec[2].ID[1]            = j
    gvec[2].ID[2]            = J # <== Note upper-case letter
    gvec[2].ID[3]            = j

Each gvec[] structure contains information about one variable that is to be reunified and output.
Note that unlike gregion[] and imp_region[], there are no .num_ID variables here. That is because
the number of entries in each .ID array is actually determined by num_imp_regions. In this example,
the relevant imp_region details are as follows:

* ``imp_region[0].im = LuoRudy94``
* ``imp_region[1].im = Ramirez``
* ``imp_region[2].im = tenTusscherPanfilov``
* ``imp_region[3].im = MahajanShiferaw``

As noted above, state variables associated with the fast sodium current are :math:`M/H/J` in the tenTusscherPanfilov
implementation vs. :math:`m/h/j` in the other 3. This is why the gvec[].ID[2] entries, which are linked
to tenTusscherPanfilov in imp_region[2], are upper-case instead of lower-case.

"""
import os
EXAMPLE_DESCRIPTIVE_NAME = 'Region Reunification (gvec)'
EXAMPLE_AUTHOR = 'Patrick Boyle <pmjboyle@gmail.com>'
EXAMPLE_DIR = os.path.dirname(__file__)
GUIinclude = True

from datetime import date
from carputils import settings
from carputils import tools

def parser():
    parser = tools.standard_parser()
    return parser

def jobID(args):
    """
    Generate name of top level output directory.
    """
    today = date.today()
    return '{}'.format(today.isoformat())

@tools.carpexample(parser, jobID, clean_pattern='{}*|(.trc)'.format(date.today().year))
def run(args, job):
    # Setup command line and run simulation
    ExtraTags = [0, 1, 2, 3]
    IntraTags = ExtraTags.copy()

    cmd = tools.carp_cmd(os.path.join(EXAMPLE_DIR,'Region-Reunification.par'))
    # need to incorporate hard-coded paths from parameter file
    # to make the script independent from calling directory
    cmd += ['-meshname',                 os.path.join(EXAMPLE_DIR, 'TestSlabMesher2D_i')]
    #cmd += ['-imp_region[0].im_sv_init', os.path.join(EXAMPLE_DIR, 'LuoRudy94.sv')]
    #cmd += ['-imp_region[1].im_sv_init', os.path.join(EXAMPLE_DIR, 'Ramirez.sv')]
    #cmd += ['-imp_region[2].im_sv_init', os.path.join(EXAMPLE_DIR, 'tenTusscherPanfilov.sv')]
    #cmd += ['-imp_region[3].im_sv_init', os.path.join(EXAMPLE_DIR, 'MahajanShiferaw.sv')]
    cmd += ['-stim[0].elec.vtx_file',     os.path.join(EXAMPLE_DIR, 'LeftSideStimWithGaps')]
    # -------------------------------------------------------------------------
    cmd += tools.gen_physics_opts(ExtraTags=ExtraTags, IntraTags=IntraTags)
    cmd += ['-simID', job.ID]

    job.carp(cmd, '05D Region Reunification')

    # Do visualization
    if args.visualize and not settings.platform.BATCH:
        # Visualize with webGUI
        if args.webGUI:
            folderNameExp = f'/experiments/05D_Region_Reunification_{job.ID}'
            os.mkdir(folderNameExp)
            cmdMesh = f'meshtool collect -imsh=/tutorials/02_EP_tissue/05D_Region_Reunification/TestSlabMesher2D_i -omsh={folderNameExp}/{job.ID} -nod=/tutorials/02_EP_tissue/05D_Region_Reunification/{job.ID}/INa_m.igb -ifmt=carp_txt -ofmt=ens_bin'
            outMesh = os.system(cmdMesh)
            if(outMesh == 0):
                print('Meshtool - conversion successfully')
            else:
                print('Meshtool - conversion failure')
        
        #Visualize with meshalyzer
        else:
            job.meshalyzer(os.path.join(EXAMPLE_DIR, 'TestSlabMesher2D_i'),
                       os.path.join(job.ID, 'INa_m.igb'),
                       os.path.join(EXAMPLE_DIR, 'gv.mshz'))

if __name__ == '__main__':
    run()
