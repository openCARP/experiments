#!/usr/bin/env python

"""

This tutorial demonstrates how to initialize a cardiac tissue with state variables obtained from
a single-cell stimulation. To run the experiments of this tutorial change directories as follows:

.. code-block:: bash

   cd ${TUTORIALS}/02_EP_tissue/03B_study_prep_init
   



Conceptual Overview
===================

Similar to in-vitro experiments, cardiac tissue is usually stabilized by pacing it at basic cycle
length (BCL) in order to reduce beat-to-beat changes. However, stabilization might require a few
hundred beats which can take days or even weeks to complete in organ-scale models. Unlike tissue
simulations, pacing an isolated cell until it achieves its stable cycle limit is computationally
feasible. In order to save computational efforts, the single-cell model state variables (:math:`V_{\mathrm m}`, 
channel gating variables, etc.) at the end of the pacing protocol can be stored and used to initialize a
tissue model. This initialization procedure is equivalent to pacing the entire tissue model in a
space-clamped mode. Although this procedure is computationally cheap, it is biologically unrealistic
since it ignores any differences due to the activation sequence of a previous beat. If information
on the activation sequence in the cardiac tissue is available beforehand, it can be used in combination
with the single-cell initialization method to overcome this limitation.

Problem Setup
=============

This example will run a simulation using a 2D sheet model (1 cm x 1 cm) in which all cells are
initialized with single-cell model states stored in file ``INIT.sv``. The file contains initial
conditions of all ordinary differential equations (ODEs) describing channel gating and ionic
concentrations in the chosen cellular model. The initial conditions are obtained after pacing
the cell model at a prescribed BCL for a number N of beats.

Usage
=====

Run

.. code-block:: bash
   
    
    ./run.py --help 

to see the optional arguments available (default values are indicated), such as:

.. code-block:: bash

    --model             Model of the cellular action potential
                        Options: {DrouhardRoberge, tenTusscherPanfilov}
                        Default: DrouhardRoberge
    --init-method       Method to initialize the tissue model
                        Options: {None, sv_init or prepace}
                        Default: None
    --bcl               Basic cycle length for repetitive single-cell stimulation
                        Default: 500 ms (2 Hz) 
    --npls              Number of pulses applied to the single-cell with the given bcl
                        Default: 10

If the program is run with the ``--visualize`` option, meshalyzer will automatically load transmembrane
potentials :math:`V_{\mathrm m}`:

.. code-block:: bash

    ./run.py --visualize

Key Parameters
--------------

The key parameter is ``--init-method``. This will be set to one of three possible values in order
to initialize the 2D sheet model:

* none: all cells are initialized with the cellular model default states that are hard coded in openCARP
* sv_init: all cells are initialized with the cellular model states stored in file ``INIT.sv``. Model
  states are computed here by running a single-cell simulation, where the cell is paced ``npls``
  (default: 10 pulses) times with the prescribed ``bcl`` (default: 500ms). Model states are taken at
  the end of the simulation protocol.
* prepace: unlike in the sv_init initialization method, where state variables are saved into a file
  at a prescribed time instant (usually at the end of a single-cell pacing protocol), in the prepace 
  method cells are initialized with model states taken at different time instants depending on their
  a priori known activation time. The difference between the two methods is illustrated below:
            
.. figure:: /images/02_03B_study_prep_init_sv_init_vs_prepace.png
    :align: center

    Tissue initialization methods. A) A 1D tissue model with all cells initialized with the same 
    :math:`V_{\mathrm m}` as well as other model state variables (not shown) taken at the end of the
    single-cell simulation. B) The same 1D tissue as in A) but cells are initialized with :math:`V_{\mathrm m}`
    and other state variables (not shown) taken at different time instants. The time instants are
    determined based on the activation sequence known beforehand: cell 1 activates at 0, cell 2 at
    t1, ..., and finally cell 5 at time t4.

Interpreting Results
====================

The difference between the results produced with each initialization option (i.e.: none, sv_init and 
prepace) can be appreciated by looking at the spatial distribution of :math:`V_{\mathrm m}` at time t = 0 with meshalyzer
(``--visualize``):

.. figure:: /images/02_03B_study_prep_init_Vm.png
    :scale: 60%
    :align: center

    Spatial distribution of :math:`V_{\mathrm m}` at the beggining of the simulation (t = 0 ms). A) All cells in the
    tissue are initialized with default model states of the ``DrouhardRoberge`` cell model. B) All cells in the tissue are initialized
    with the same model states in file ``INIT.sv``. C) Cells in the tissue
    are initialized with model states taken at different time instants depending on their a priori known activation time.

* A) By default (``--init-method=none``), :math:`V_{\mathrm m}` at t = 0 ms will be -86.9269 mV for all cells in the tissue.
  This is because -86.9269 is the default initial condition of :math:`V_{\mathrm m}` in the ``DrouhardRoberge`` model (the initial
  condition of :math:`V_{\mathrm m}` in the ``tenTusscherPanfilov`` model is -86.2).
* B) By running with the ``--init-method=sv_init`` option, :math:`V_{\mathrm m}` at t = 0 ms will be -85.7103 mV for all
  cells in the tissue. This is because -85.7103 is the initial condition of :math:`V_{\mathrm m}` in file ``INIT.sv``,
  all model states were obtained after pacing the ``DrouhardRoberge`` model for 10 times with a bcl of 500 ms.
* C) By running with the ``--init-method=prepace`` option, :math:`V_{\mathrm m}` at t = 0 ms will vary between -85.7108 mV
  (left lower corner) and -85.6336 mV (right upper corner). This is because, unlike the previous
  methods where all cells are initialized with the same cellular model states, cells are initialized
  according to model states obtained at different time instants (see above) according to the
  activation sequence in file ``LATs.dat``:
  
.. figure:: /images/02_03B_study_prep_init_prepace_LATs.png
    :scale: 60%
    :align: center

    Activation sequence used to prepace the cells in the tissue.

Although the differences in :math:`V_{\mathrm m}` between the three initialization methods seem negligible,
they can be substantial in other state variables of the ionic model with a slower time decay such as the
intracellular calcium :math:`Ca_{\mathrm i}`. :math:`Ca_{\mathrm i} = 0.30` if ``--init-method=none`` and
:math:`Ca_{\mathrm i} = 0.172033` (see file ``INIT.sv``) if ``--init-method=sv_init`` with the bcl = 500 ms
and npls = 10. These differences increase for all state variables as the bcl is reduced.

openCARP Parameters
====================

The relevant parameters used in the tissue-scale simulation are shown below:

.. code-block:: bash

    # Model of the cellular action potential
    -imp_region[0].im = model

    # Used by the sv_init method
    -imp_region[0].im_sv_init = 'INIT.sv'

    # Used by the prepace method
    -prepacing_beats = 10
    -prepacing_lats  = 'LATs.dat'
    -prepacing_bcl   = 500
    
imp_region[] is used to assign a model of cellular action potential to all cells in the tissue. If
the ``sv_init`` initialization method is choosen, then the tissue will be initialized with cellular
model states in file ``INIT.sv``. However, if the ``prepace`` method is choosen, then the cell model
in imp_region[] will be paced ``npls`` times with the given ``bcl``. Tissue activation times in file
``LATs.dat`` are used to guide state set-up for prepacing.

Once the initial conditions are prescribed, the tissue simulation is started. In this example, a
transmembrane current of 100 uA/cm^2 is applied for a period of 1 ms to the cells located at the
middle of the left side of the tissue.

"""
import os
EXAMPLE_DESCRIPTIVE_NAME = 'Limit cycle initialization'
EXAMPLE_AUTHOR = 'Fernando Campos <fernando.campos@kcl.ac.uk>'
EXAMPLE_DIR = os.path.dirname(__file__)
GUIinclude = True

from datetime import date
from carputils import settings
from carputils import tools
from carputils import mesh

def parser():
    parser = tools.standard_parser()
    group  = parser.add_argument_group('experiment specific options')

    group.add_argument('--model',
                        default = 'DrouhardRoberge',
                        choices = ['DrouhardRoberge','tenTusscherPanfilov'],
                        help = 'Model of the cellular action potential (default is DrouhardRoberge)')

    group.add_argument('--init-method',
                        default = None,
                        choices = [None, 'sv_init', 'prepace'],
                        help = 'Method to initialize the tissue model')

    group.add_argument('--bcl',
                        default = 500, type = int,
                        help = 'Basic cycle length for repetitive single-cell stimulation')
    
    group.add_argument('--npls',
                        default = 10, type = int,
                        help = 'Number of pulses applied to the single-cell with the given bcl')
    
    return parser

def jobID(args):
    """
    Generate name of top level output directory.
    """
    today = date.today()
    return '{}_{}_init-method_{}_np{}'.format(today.isoformat(), args.model, args.init_method, args.np)

def runSingleCell(args, job):
    # Duration of the simulation
    tend = args.npls*args.bcl

    # Single-cell simulation
    cmd = ['--imp',           args.model,
           '--bcl',           args.bcl,
           '--numstim',       args.npls,
           '--duration',      tend,
           '--save-ini-file', 'INIT.sv',
           '--save-ini-time', tend]
    
    # Output options
    cmd += ['--dt',     0.1,
            '--dt-out', tend,
            '--fout={}'.format(os.path.join(job.ID, '{}_bcl_{}ms'.format(args.model,args.bcl)))]

    # Run bench
    job.bench(cmd)


@tools.carpexample(parser, jobID, clean_pattern='{}*|Trace*|(.txt)|(mesh)'.format(date.today()))
def run(args, job):

    # Generate mesh
    # Size of the wedge (mm)
    wedge_x = 10
    wedge_y = 10
    wedge_z = 0
    
    # Spatial (mm) and time (us) discretization
    dx = 0.2
    dt = 100
    geom = mesh.Block(size=(wedge_x,wedge_y,wedge_z),resolution=dx,etype='hexa')
    
    # Put the corner of the tissue block at the origin
    geom.corner_at_origin()
    
    # Add regions ------> (0,0,0) is the center of the mesh, not the left lower corner!
    tag  = 100
    prox = mesh.BoxRegion((-wedge_x/2.0,-wedge_y/2.0,0),(wedge_x/2.0,wedge_y/2.0,0),tag)
    geom.add_region(prox)
    
    # Generate and return base name
    meshname = mesh.generate(geom)
    
    # Model of the cellular action potential
    imp_reg = ['-num_imp_regions',       1,
               '-imp_region[0].num_IDs', 1,
               '-imp_region[0].ID[0]',   tag,
               '-imp_region[0].im',      args.model]
    
    # Tissue initialization
    if args.init_method == 'sv_init':
        # Run single-cell simulation
        runSingleCell(args, job)
        
        # Limit cycle initialization using single cell state file (generated by bench)
        tissue_init = ['-imp_region[0].im_sv_init', 'INIT.sv']

    elif args.init_method == 'prepace':
        # Prepace
        tissue_init = ['-prepacing_beats', args.npls,
                       '-prepacing_lats',  os.path.join(EXAMPLE_DIR, 'LATs.dat'),
                       '-prepacing_bcl',   args.bcl]
    else:
        # None
        tissue_init = ''
        
    # Tissue setup (isotropic)
    g_reg = ['-num_gregions',       1,
             '-gregion[0].num_IDs', 1,
             '-gregion[0].ID[0]',   tag,
             '-gregion[0].g_il',    0.174,
             '-gregion[0].g_it',    0.174,
             '-gregion[0].g_el',    0.625,
             '-gregion[0].g_et',    0.625]

    # Pacing protocol
    stim = ['-num_stim',                1,
            '-stim[0].crct.type',       0,
            '-stim[0].pulse.strength',  100.0,
            '-stim[0].ptcl.duration',   1.0,
            '-stim[0].ptcl.npls',       1,
            '-stim[0].elec.p0[0]',      0,
            '-stim[0].elec.p1[0]',      500,
            '-stim[0].elec.p0[1]',      (wedge_y/2*1000)-250,
            '-stim[0].elec.p1[1]',      5250]
    
    # Timning
    timing = ['-dt',      dt,
              '-tend',    50,
              '-timedt',  10,
              '-spacedt',  1]

    # Get basic command line, including solver options
    cmd = tools.carp_cmd()
    
    cmd += imp_reg
    cmd += g_reg
    cmd += tissue_init
    cmd += stim
    cmd += timing

    # Simulation ID
    simID = job.ID

    cmd += ['-meshname', meshname,
            '-simID',    simID]
    cmd += tools.gen_physics_opts(ExtraTags=[tag], IntraTags=[tag])
    
    # Output grids
    if args.visualize:
        cmd += ['-gridout_i', 2]

    # Run tissue simulation
    job.carp(cmd)



    # Do visualization
    if args.visualize and not settings.platform.BATCH:

        # Prepare file paths
        geom = os.path.join(simID, os.path.basename(meshname)+'_i')
        data = os.path.join(simID, 'vm.igb')
        view = os.path.join(EXAMPLE_DIR, 'view.mshz')

        # GUI
        if args.webGUI:
            folderNameExp = f'/experiments/03B_study_prep_init_{job.ID}'
            os.mkdir(folderNameExp)
            cmdMesh = f'meshtool collect -imsh={geom} -omsh={folderNameExp}/{job.ID} -nod={data} -ifmt=carp_txt -ofmt=ens_bin'
            outMesh = os.system(cmdMesh)
            if(outMesh == 0):
                print('Meshtool - conversion successfully')
            else:
                print('Meshtool - conversion failure')
        
        # Call meshalyzer to show Vm
        else:
            job.meshalyzer(geom, data, view)


if __name__ == '__main__':
    run()
