__title__ = 'Init tissue from cell'
__description__ = 'This tutorial demonstrates how to initialize a cardiac tissue with state variables obtained from a single-cell stimulation'
__image__ = '/images/02_03B_study_prep_init_sv_init_vs_prepace.png'
__q2a_tags__ = 'experiments,examples,parameters,lat,tissue'