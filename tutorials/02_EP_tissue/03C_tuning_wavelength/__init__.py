__title__ = 'Adjust parameters'
__description__ = 'This tutorial demonstrates how to adjust parameters in tissue simulations to match experimental data for conduction velocity, APD, and wavelength'
__image__ = '/images/02_03C_cv_calc.png'
__q2a_tags__ = 'experiments,examples,tunecv,tissue'