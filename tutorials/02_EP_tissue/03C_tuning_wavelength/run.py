#!/usr/bin/env python

r"""
.. _tutorial_wavelength-tuning:
(replace `./vm.igb` with the proper location of your file)
This tutorial demonstrates how to adjust parameters in tissue simulations to match experimental data for conduction velocity,
action potential duration, and wavelength. To run the experiments of this tutorial change directories as follows:

.. code-block:: bash

   cd ${TUTORIALS}/02_EP_tissue/03C_tuning_wavelength


Introduction
============

It is important that computer simulations in cardiac tissue corroborate experimental data. In particular,
conduction velocity (CV) is the speed at which electrical waves propagate in cardiac tissue, and action
potential duration (APD) is the time duration a cardiac myocyte repolarizes after excitation.
The multiplication of CV and APD is termed the wavelength for reentry (:math:`\lambda`), which is the
minimum length cardiac tissue dimensions have to be in order to support reentry from unidirectional
conduction block. In this exercise, the user will perform a basic example of how to adjust CV and APD in a
2D cardiac sheet of ventricular tissue to match CV and APD from experimental data recorded on the
ventricular epicardium of human ventricles with optical imaging.


Problem setup
=============

To execute simulations of this tutorial do

.. code-block:: bash

   cd ${TUTORIALS}/02_EP_tissue/03C_tuning_wavelength
   

Experimental data
-----------------

The mapping of electrical activity with optical imaging on the epicardium of human ventricles provides
an accurate measurement of CV [#Glukhov2012]_ and APD [#Glukhov2010]_. The data from these studies is shown
below and were recorded during baseline pacing with a cycle length of 1000 ms.

+----------------------------+----------------------------+----------------------------+----------------------------+----------------------------+----------------------------+
|      Tissue state          |      :math:`CV_\mathrm{l}` (cm/s) |      :math:`CV_\mathrm{t}` (cm/s) |      :math:`APD_{80}` (ms) | :math:`{\lambda}_\mathrm{l}` (cm) | :math:`{\lambda}_\mathrm{t}` (cm) |
+============================+============================+============================+============================+============================+============================+
|        Nonfailing          |               92           |                22          |               340          |               34           |               8            |
+----------------------------+----------------------------+----------------------------+----------------------------+----------------------------+----------------------------+


1D cable model
--------------

A 1.5 cm cable of epicardial ventricular myocytes is used to initially adjust CV and APD to experimentally
derived values. The model domain was discretized with linear finite elements with an average edge length of 0.02 cm.

2D sheet model
---------------

A 1.5 cm x 1.5 cm sheet of epicardial tissue is used to verify CV and APD derived in the 1D cable model.
The model domain is discretized using quadrilateral element finite elements with an average edge length of 0.02 cm,
and a longitudinal fiber direction in each element parallel to the X-axis of the sheet.
The mesh was generated using the command below according to the :ref:`mesher tutorial <run.mesher>`.

.. code-block:: bash

         mesher -size[0] 1.5 -size[1] 1.5 -size[2] 0.0 -resolution[0] 200.0 -resolution[1] 200.0 -resolution[2] 0.0

Ionic model
-----------

For this example, we use the most recent version of the ten Tusscher ionic model for human ventricular
myocytes [#Tusscher2006]_ . This ionic model is labeled tenTusscherPanfilov
in openCARP's LIMPET library. After a quick literature search, one will find that the slow outward rectifying
potassium current :math:`I_{K_{s}}` is heterogeneous across the human ventricular wall [#Pereon2000]_.
Therefore, the maximal conductance :math:`G_{K_{s}}` of :math:`I_{K_{s}}` is adjusted in order to match
the APD=340 ms derived experimentally.
Note, the default value for :math:`G_{K_{s}}` in epicardial ventricular myocytes is 0.392 nS/pF.

Pacing protocol
---------------

The left side of the 1D cable model and the center of the 2D sheet model is paced with 5-ms-long stimuli at
twice capture amplitude for a cycle length and number of beats chosen by the user.

Conduction velocity
-------------------

To determine initial conditions for the tissue conductivities along (:math:`\sigma_{il}`, :math:`\sigma_{el}`) and
transverse (:math:`\sigma_{it}`, :math:`\sigma_{et}`) the fibers in the models, tuneCV is used as described in the
tutorial :ref:`Tuning Conduction Velocities <nolink>`. The commands to obtain the two conductivities are listed below.

.. code-block:: bash

         tuneCV --converge true --tol 0.0001 --velocity 0.92 --model tenTusscherPanfilov --sourceModel monodomain --resolution 200.0

         tuneCV --converge true --tol 0.0001 --velocity 0.22 --model tenTusscherPanfilov --sourceModel monodomain --resolution 200.0

The initial conductivities resulting from tuneCV are :math:`\sigma_{il}=0.4134` S/m, :math:`\sigma_{el}=1.4849` S/m,
:math:`\sigma_{it}=0.0379` S/m, and :math:`\sigma_{et}=0.1361` S/m.

.. _tutorial-electrical-mapping:

To compute CV, activation times are computed for the last beat of the pacing protocol using the openCARP
option LATs (see tutorial on :ref:`electrical mapping <tutorial-electrical-mapping>`), with activation time
recorded at the threshold crossing of -10 mV. CV is then computed along the cable by taking the difference
in activation times at the locations 1.0 cm and 0.5 cm divided by the distance between the two points.
For the sheet model, CV is computed along the longitudinal and transverse fiber directions by taking the
difference in activation times at the locations illustrated in the figure below.
Specifically, :math:`CV_{l}` = 0.25/(L2-L1) and :math:`CV_{t}` = 0.25/(T2-T1). The locations of L1 and T1
are 0.25 cm away from the tissue center, and L2 and T2 are 0.5 cm away from the tissue center.

.. _fig-cv_calculation-2D:

.. figure:: /images/02_03C_cv_calc.png
   :width: 50 %
   :align: center


Action potential duration 
-------------------------

Action potential duration is computed at 80% repolarization (:math:`APD_{80}`) according to [Bayer2016]_.
This is achieved by using the igbutils function igbapd as illustrated below (replace `./vm.igb` with the proper location of your file).

.. code-block:: bash

         igbapd --repol=80 --vup=-10 --peak-value=plateau ./vm.igb 


Usage
=====

The following optional arguments are available (default values are indicated):

.. code-block:: bash

  ./run.py --help 
    --dimension         Options: {cable,sheet}, Default: cable
                        Choose cable for quick 1D parameter adjustments, 
                        then the 2D sheet to verify the adjustments.
    --GKs               Default: 0.392  nS/pF
                        Maximal conductance of IKs
    --Gil               Default: 0.3544 S/m
                        Intracellular longitudinal tissue conductivity
    --Gel               Default: 1.27 S/m
                        Extracellular longitudinal tissue conductivity
    --Git               Default: 0.024 S/m
                        Intracellular transverse tissue conductivity
    --Get               Default: 0.0862 S/m
                        Extracellular transverse tissue conductivity
    --nbeats            Default: 3
                        Number of beats for pacing protocol. This number
                        should be much larger to achieve steady-state
    --bcl               Default: 1000
                        Basic cycle length for pacing protocol
    --timess            Default: 1000
                        Time before applying pacing protocol

After running run.py, the results for APD, CV, and (:math:`\lambda`) can be found in the file adjustment_results.txt
within the output subdirectory for the simulation.

If the program is ran with the ``--visualize`` option, meshalyzer will automatically load the :math:`V_\mathrm{m}(t)`
for the last beat of the pacing protocol. Output files for activation and APD are also produced for each
simulation and can be found in the output directory and loaded into meshalyzer.

Tasks
=====

1. Determine the value for IKs to obtain the experimental value for APD in the cable.

2. Place this value in the sheet model to verify the model is behaving in the same manner as the simple cable model.

3. Determine a set of Gil and Gel (keep ratio for Gil/Git the same) so that both the longitudinal and transverse wavelengths are less than 8 cm.


Solutions to the tasks
=========================

1. A GKs=0.22 nS/pF is needed to obtain the APD of about 340 ms. 

.. code-block:: bash

  ./run.py --GKs 0.22 --dimension cable

2. The following command produces a similar APD in the sheet model.

.. code-block:: bash

  ./run.py --GKs 0.25 --dimension sheet

3. The following commands produce longitudinal and transverse wavelengths less than 8 cm.

.. code-block:: bash

  ./run.py --GKs 0.25 --Gil 0.02 --Git 0.0135 --dimension cable

  ./run.py --GKs 0.25 --Gil 0.02 --Git 0.0135 --dimension sheet


.. rubric:: References

.. [#Glukhov2012] Glukhov AV, Fedorov VV, Kalish PW, Ravikumar VK, Lou Q, Janks D, Schuessler RB, Moazami N, Efimov IR.
                 **Conduction remodeling in human end-stage nonischemic left ventricular cardiomyopathy.**
                 *Circulation*, 125(15):1835-1847, 2012.
                 `[Pubmed] <https://www.ncbi.nlm.nih.gov/pubmed/22412072>`__
.. [#Glukhov2010] Glukhov AV, Fedorov VV, Lou Q, Ravikumar VK, Kalish PW, Schuessler RB, Moazami N, and Efimov IR.
                 **Transmural dispersion of repolarization in failing and nonfailing human ventricle.**
                 *Circ Res*, 106(5):981-991, 2010.
                 `[Pubmed] <https://www.ncbi.nlm.nih.gov/pubmed/20093630>`__
.. [#Tusscher2006] ten Tusscher KHWJ, Panfilov AV.
                 **Alternans and spiral breakup in a human ventricular tissue model.**
                 *Am J Physiol Heart Circ Physiol*, 291(3):H1088-H1100, 2006.
                 `[Pubmed] <https://www.ncbi.nlm.nih.gov/pubmed/16565318>`__
.. [#Pereon2000] Pereon Y, Demolombe S, Baro I, Drouin E, Charpentier F, Escande D.
                 **Differential expression of kvlqt1 isoforms across the human ventricular wall.**
                 *Am J Physiol Heart Circ Physiol*, 278(6):H1908-H1915, 2000.
                 `[Pubmed] <https://www.ncbi.nlm.nih.gov/pubmed/10843888>`__

"""

#Leftover text
#|        Failing             |               91           |                16          |               400          |               36           |               6            |
#+----------------------------+----------------------------+----------------------------+----------------------------+----------------------------+----------------------------+

import os
EXAMPLE_DESCRIPTIVE_NAME = 'Adjusting wavelength to experimental data'
EXAMPLE_AUTHOR = 'Jason Bayer <jason.bayer@ihu-liryc.fr>'
EXAMPLE_DIR = os.path.dirname(__file__)
GUIinclude = False

from datetime import date
from carputils import settings
from carputils import tools
from carputils.carpio import txt
import numpy as np

def parser():
    # Generate the standard command line parser
    parser = tools.standard_parser()
    group  = parser.add_argument_group('experiment specific options')
    # Add arguments    
    group.add_argument('--dimension',
                        default = 'cable',
                        choices = ['cable', 'sheet'],
                        help = 'Dimension of cable or sheet')
    group.add_argument('--GKs',
                        type = float,
                        default = 0.392,
                        help = 'Maximal conductance for IKs')
    group.add_argument('--Gil',
                        type = float,
                        default = 0.3544,
                        help = 'Intracellular longitudinal tissue conductivity')
    group.add_argument('--Gel',
                        type = float,
                        default = 1.27,
                        help = 'Extracellular longitudinal tissue conductivity')
    group.add_argument('--Git',
                        type = float,
                        default = 0.024,
                        help = 'Intracellular transverse tissue conductivity')
    group.add_argument('--Get',
                        type = float,
                        default = 0.0862,
                        help = 'Extracellualr transverse tissue conductivity')
    group.add_argument('--nbeats',
                        type = int,
                        default = 2,
                        help = 'Number of beats for pacing protocol')
    group.add_argument('--bcl',
                        type = int,
                        default = 1000,
                        help = 'BCL for paced beats')
    group.add_argument('--timess',
                        type = int,
                        default = 1000,
                        help = 'Time to let tissue reach steady-state before pacing')
    return parser


def jobID(args):
    today = date.today()
    ID = '{}_GKs-{}_Gil-{}_Git-{}_{}_nbeats_{}_bcl_{}'.format(today.isoformat(), args.GKs, args.Gil, args.Git, args.dimension, args.nbeats, args.bcl)
    return ID


@tools.carpexample(parser, jobID, clean_pattern='{}*|(.trc)'.format(date.today().year))
def run(args, job):

    imparam = 'GKs={}'.format(args.GKs)
    tsav_state = (args.nbeats-1)*args.bcl + args.timess
    tend_state = tsav_state + 0.1
    tend_b10 = tsav_state + args.bcl
    IntraTags = [1] # element labels of intracellular space
    ExtraTags = [1] # element labels of extracellular space

    # Either do cable or sheet computations

    #####################################
    # CABLE
    #####################################
    if args.dimension == 'cable':

        ###########################################
        # First do the longitudinal CV calculations
        #####################################

        #Run the steady state cable simulation
        cmd = tools.carp_cmd(os.path.join(EXAMPLE_DIR, 'tuning_wavelength_state_cable.par'))
        simid = os.path.join(job.ID, 'Tuning_Wavelength_State')

        cmd += ['-simID', simid,
                '-write_statef', 'statefile',
                '-meshname', os.path.join(EXAMPLE_DIR, 'Mesh_1.5cm_Cable', 'mesh'),
                '-imp_region[0].im_param', imparam,
                '-gregion[0].g_il', args.Gil,
                '-gregion[0].g_el', args.Gel,
                '-gregion[0].g_it', args.Git,
                '-gregion[0].g_et', args.Get,
                '-gregion[0].g_in', args.Git,
                '-gregion[0].g_en', args.Get,
                '-stim[0].ptcl.start', args.timess,
                '-stim[0].ptcl.bcl', args.bcl,
                '-stim[0].ptcl.npls', args.nbeats-1,
                '-stim[0].elec.vtx_file', os.path.join(EXAMPLE_DIR, 'Mesh_1.5cm_Cable', 'stim'),
                '-tsav[0]', tsav_state,
                '-tend', tend_state]
        cmd += tools.gen_physics_opts(ExtraTags=ExtraTags, IntraTags=IntraTags)

        # Run simulation
        job.carp(cmd)

        # Run the last beat simulation
        cmd = tools.carp_cmd(os.path.join(EXAMPLE_DIR, 'tuning_wavelength_b10_cable.par'))
        simid = os.path.join(job.ID, 'Tuning_Wavelength_b10')

        cmd += ['-simID', simid,
                '-start_statef', os.path.join(job.ID, 'Tuning_Wavelength_State', 'statefile.{}.0'.format(tsav_state)),
                '-imp_region[0].im_param', imparam,
                 '-meshname', os.path.join(EXAMPLE_DIR, 'Mesh_1.5cm_Cable', 'mesh'),
                '-gregion[0].g_il', args.Gil,
                '-gregion[0].g_el', args.Gel,
                '-gregion[0].g_it', args.Git,
                '-gregion[0].g_et', args.Get,
                '-gregion[0].g_in', args.Git,
                '-gregion[0].g_en', args.Get,
                '-tend', tend_b10,
                '-stim[0].ptcl.start', tsav_state,
                '-stim[0].ptcl.bcl', args.bcl,
                '-stim[0].elec.vtx_file', os.path.join(EXAMPLE_DIR, 'Mesh_1.5cm_Cable', 'stim')]
        cmd += tools.gen_physics_opts(ExtraTags=ExtraTags, IntraTags=IntraTags)

        # Run simulation
        job.carp(cmd)

        # Now compute the APD
        cmd = [settings.execs.igbapd,
               '--repol=80',
               '--vup=-10',
               '--peak-value=plateau',
               '--output-file={}'.format(os.path.join(simid, 'apd.dat')),
                os.path.join(simid, 'vm.igb')]

        #Run simulation
        job.bash(cmd)

        if not args.dry:
            #Now compute the APD at 1cm
            apdfile = os.path.join(simid, 'apd.dat')
            APDs = txt.read(apdfile)
            LAPD = APDs[37]
    
            #Now compute the CV between 0.5 and 1.0
            actfile = os.path.join(simid, 'init_acts_depol-thresh.dat')
            LATs = txt.read(actfile)
            LCV1 = LATs[25]
            LCV2 = LATs[50]
            LCV = (0.5/(LCV2-LCV1))*1000.0
            LWL = LAPD*LCV*0.001

        #########################################
        #Second do the transverse CV calculations
        #########################################

        #Run the steady state cable simulation
        cmd = tools.carp_cmd(os.path.join(EXAMPLE_DIR, 'tuning_wavelength_state_cable.par'))
        simid = os.path.join(job.ID, 'Tuning_Wavelength_State')

        cmd += ['-simID', simid,
                '-write_statef', 'statefile',
                '-meshname', os.path.join(EXAMPLE_DIR, 'Mesh_1.5cm_Cable', 'mesh'),
                '-imp_region[0].im_param', imparam,
                '-gregion[0].g_il', args.Git,
                '-gregion[0].g_el', args.Get,
                '-gregion[0].g_it', args.Git,
                '-gregion[0].g_et', args.Get,
                '-gregion[0].g_in', args.Git,
                '-gregion[0].g_en', args.Get,
                '-stim[0].ptcl.start', args.timess,
                '-stim[0].ptcl.bcl', args.bcl,
                '-stim[0].ptcl.npls', args.nbeats-1,
                '-stim[0].elec.vtx_file', os.path.join(EXAMPLE_DIR, 'Mesh_1.5cm_Cable', 'stim'),
                '-tsav[0]', tsav_state,
                '-tend', tend_state]
        cmd += tools.gen_physics_opts(ExtraTags=ExtraTags, IntraTags=IntraTags)

        #Run simulation
        job.carp(cmd)

        #Run the last beat simulation
        cmd = tools.carp_cmd(os.path.join(EXAMPLE_DIR, 'tuning_wavelength_b10_cable.par'))
        simid = os.path.join(job.ID, 'Tuning_Wavelength_b10')

        cmd += ['-simID', simid,
                '-start_statef', os.path.join(job.ID, 'Tuning_Wavelength_State', 'statefile.{}.0'.format(tsav_state)),
                '-imp_region[0].im_param', imparam,
                '-meshname', os.path.join(EXAMPLE_DIR, 'Mesh_1.5cm_Cable', 'mesh'),
                '-gregion[0].g_il', args.Git,
                '-gregion[0].g_el', args.Get,
                '-gregion[0].g_it', args.Git,
                '-gregion[0].g_et', args.Get,
                '-gregion[0].g_in', args.Git,
                '-gregion[0].g_en', args.Get,
                '-tend', tend_b10,
                '-stim[0].ptcl.start', tsav_state,
                '-stim[0].ptcl.bcl', args.bcl,
                '-stim[0].elec.vtx_file', os.path.join(EXAMPLE_DIR, 'Mesh_1.5cm_Cable', 'stim')]
        cmd += tools.gen_physics_opts(ExtraTags=ExtraTags, IntraTags=IntraTags)

        #Run simulation
        job.carp(cmd)

        #Now compute the APD
        if not args.dry:
            cmd  = [settings.execs.igbapd,
                    '--repol=80',
                    '--vup=-10',
                    '--peak-value=plateau',
                    '--output-file={}'.format(os.path.join(simid, 'apd.dat')),
                     os.path.join(simid, 'vm.igb')]
    
            #Run simulation
            job.bash(cmd)

            #Now compute the APD at 1cm
            apdfile = os.path.join(simid, 'apd.dat')
            APDs = txt.read(apdfile)
            TAPD = APDs[37]
    
            #Now compute the CV between 0.5 and 1.0
            actfile = os.path.join(simid, 'init_acts_depol-thresh.dat')
            LATs = txt.read(actfile)
            TCV1 = LATs[25]
            TCV2 = LATs[50]
            TCV = (0.5/(TCV2-TCV1))*1000.0
            TWL = TAPD*TCV*0.001

    
            #####################################
            # Last, write the results
            #####################################
            result_str  = '\nResults for APD and CV in cable along fibers\n'
            result_str += 'APD = {:6.2f} (ms)\n'.format(LAPD)
            result_str += 'CV  = {:6.2f} (cm/s)\n'.format(LCV)
            result_str += 'WL  = {:6.2f} (cm)\n'.format(LWL)
            result_str += '\n\nResults for APD and CV in cable transverse fibers\n'
            result_str += 'APD = {:6.2f} (ms)\n'.format(TAPD)
            result_str += 'CV  = {:6.2f} (cm/s)\n'.format(TCV)
            result_str += 'WL  = {:6.2f} (cm)\n'.format(TWL)
            print(result_str)

            resultsfile = os.path.join(job.ID, 'adjustment_results.txt')
            with open(resultsfile, 'wt') as fp:
                fp.write(result_str)

        # Do visualization
        if args.visualize:           
            # Visualize with GUI
            if args.webGUI:
              folderNameExp = f'/experiments/03C_tuning_wavelength_{job.ID}'
              os.mkdir(folderNameExp)
              cmdMesh = f'meshtool collect -imsh=/tutorials/02_EP_tissue/03C_tuning_wavelength/Mesh_1.5cm_Cable/mesh -omsh={folderNameExp}/{job.ID} -nod=/tutorials/02_EP_tissue/03C_tuning_wavelength/{job.ID}/Tuning_Wavelength_b10/vm.igb -ifmt=carp_txt -ofmt=ens_bin'
              outMesh = os.system(cmdMesh)
              if(outMesh == 0):
                  print('Meshtool - conversion successfully')
              else:
                  print('Meshtool - conversion failure')
            
            #Visualize with meshalyzer
            else:
                geom = os.path.join(EXAMPLE_DIR, 'Mesh_1.5cm_Cable', 'mesh.pts')
                data = os.path.join(job.ID, 'Tuning_Wavelength_b10', 'vm.igb')
                view = os.path.join(EXAMPLE_DIR, 'vm.mshz')
                job.meshalyzer(geom, data, view)

    

    #####################################
    # SHEET
    #####################################
    if args.dimension == 'sheet':

        meshdir = os.path.join(EXAMPLE_DIR, 'Mesh_1.5cm_Sheet', 'mesh')
        stimdir = os.path.join(EXAMPLE_DIR, 'Mesh_1.5cm_Sheet', 'stim')

        ###########################################
        # First do the longitudinal CV calculations
        ###########################################

        # Run the steady state cable simulation
        cmd = tools.carp_cmd(os.path.join(EXAMPLE_DIR, 'tuning_wavelength_state_cable.par'))
        simid = os.path.join(job.ID, 'Tuning_Wavelength_State')

        cmd += ['-simID', simid,
                '-write_statef', 'statefile',
                '-imp_region[0].im_param', imparam,
                '-gregion[0].g_il', args.Gil,
                '-gregion[0].g_el', args.Gel,
                '-gregion[0].g_it', args.Git,
                '-gregion[0].g_et', args.Get,
                '-gregion[0].g_in', args.Git,
                '-gregion[0].g_en', args.Get,
                '-stim[0].ptcl.start', args.timess,
                '-stim[0].ptcl.bcl', args.bcl,
                '-stim[0].ptcl.npls', args.nbeats-1,
                '-stim[0].elec.vtx_file', stimdir,
                '-tsav[0]', tsav_state,
                '-tend', tend_state,
                '-meshname', meshdir]
        cmd += tools.gen_physics_opts(ExtraTags=ExtraTags, IntraTags=IntraTags)

        #Run simulation
        job.carp(cmd)

        # Run the last beat simulation
        cmd = tools.carp_cmd(os.path.join(EXAMPLE_DIR, 'tuning_wavelength_b10_cable.par'))
        simid = os.path.join(job.ID, 'Tuning_Wavelength_b10')

        cmd += ['-simID', simid,
                '-start_statef', os.path.join(job.ID, 'Tuning_Wavelength_State', 'statefile.{}.0'.format(tsav_state)),
                '-imp_region[0].im_param', imparam,
                '-gregion[0].g_il', args.Gil,
                '-gregion[0].g_el', args.Gel,
                '-gregion[0].g_it', args.Git,
                '-gregion[0].g_et', args.Get,
                '-gregion[0].g_in', args.Git,
                '-gregion[0].g_en', args.Get,
                '-tend', tend_b10,
                '-stim[0].ptcl.start', tsav_state,
                '-stim[0].ptcl.bcl', args.bcl,
                '-stim[0].elec.vtx_file', stimdir,
                '-meshname', meshdir]
        cmd += tools.gen_physics_opts(ExtraTags=ExtraTags, IntraTags=IntraTags)

        # Run simulation
        job.carp(cmd)

        # Now compute the APD
        cmd = [settings.execs.igbapd,
               '--repol=80',
               '--vup=-10',
               '--peak-value=plateau',
               '--output-file={}'.format(os.path.join(simid, 'apd.dat')),
                os.path.join(simid, 'vm.igb')]

        #Run simulation
        job.bash(cmd)

        #Now compute the APD at 1cm
        apdfile = os.path.join(simid, 'apd.dat')
        APDs = txt.read(apdfile)
        LAPD = APDs[2862]
        TAPD = APDs[3837]

        #Now compute the CV between 0.5 and 1.0
        actfile = os.path.join(simid, 'init_acts_depol-thresh.dat')
        LATs = txt.read(actfile)
        LCV1 = LATs[2862]
        LCV2 = LATs[2875]
        LCV = (0.26/(LCV2-LCV1))*1000.0
        LWL = LAPD*LCV*0.001
        TCV1 = LATs[3837]
        TCV2 = LATs[4825]
        TCV = (0.26/(TCV2-TCV1))*1000.0
        TWL = TAPD*TCV*0.001

                
        #####################################
        #Last, write the results
        #####################################

        result_str = '\nResults for APD and CV in sheet along fibers\n'
        result_str += 'APD = {:6.2f} (ms)\n'.format(LAPD)
        result_str += 'CV  = {:6.2f} (cm/s)\n'.format(LCV)
        result_str += 'WL  = {:6.2f} (cm)\n'.format(LWL)
        result_str += '\n\nResults for APD and CV in sheet transverse fibers\n'
        result_str += 'APD = {:6.2f} (ms)\n'.format(TAPD)
        result_str += 'CV  = {:6.2f} (cm/s)\n'.format(TCV)
        result_str += 'WL  = {:6.2f} (cm)\n'.format(TWL)
        print(result_str)

        resultsfile = os.path.join(job.ID, 'adjustment_results.txt')
        with open(resultsfile, 'wt') as fp:
            fp.write(result_str)

        # Do visualization
        if args.visualize and not settings.platform.BATCH:

          # Visualize with GUI
          if args.webGUI:
            folderNameExp = f'/experiments/03C_tuning_wavelength_{job.ID}'
            os.mkdir(folderNameExp)
            cmdMesh = f'meshtool collect -imsh=/tutorials/02_EP_tissue/03C_tuning_wavelength/Mesh_1.5cm_Sheet/mesh -omsh={folderNameExp}/{job.ID} -nod=/tutorials/02_EP_tissue/03C_tuning_wavelength/{job.ID}/Tuning_Wavelength_b10/vm.igb -ifmt=carp_txt -ofmt=ens_bin'
            outMesh = os.system(cmdMesh)
            if(outMesh == 0):
                print('Meshtool - conversion successfully')
            else:
                print('Meshtool - conversion failure')
          
          #Visualize with meshalyzer
          else:  
            geom = os.path.join(EXAMPLE_DIR, 'Mesh_1.5cm_Sheet', 'mesh.pts')
            data = os.path.join(job.ID, 'Tuning_Wavelength_b10', 'vm.igb')
            view = os.path.join(EXAMPLE_DIR, 'vm.mshz')
            job.meshalyzer(geom, data, view)

if __name__ == '__main__':
    run()
