#!/usr/bin/env python

r"""
.. _tutorial_conduction-velocity-restitution:

This example demonstrates how to compute conduction velocity restitution in cardiac tissue. To run the experiments of this tutorial do

.. code-block:: bash

    cd ${TUTORIALS}/02_EP_tissue/03D_conduction_velocity_restitution



Introduction
=========================
Conduction velocity restitution is an important property of cardiac tissue. As pacing frequency is increased, conduction velocity will become slower. For this tutorial, the user will be shown how to construct conduction velocity restitution curves to describe the conduction properties of cardiac tissue in response to various pacing protocols.

Problem setup
=========================

1D cable model
--------------
A 1.0 cm cable of epicardial ventricular myocytes is used to generate a CV restitution curve for a user defined pacing protocol. The model domain was discretized with linear finite elements with an average edge length of 0.01 cm. 

Ionic model
-----------
This tutorial uses the most recent version of the ten Tusscher-Panfilov ionic model for human ventricular myocytes `[Tusscher2006] <https://pubmed.ncbi.nlm.nih.gov/16565318/>`_ . This ionic model is labeled tenTusscherPanfilov in openCARP's LIMPET library.

Pacing protocol
---------------
The left side of the 1D cable model is paced with 5-ms-long stimuli at twice capture amplitude for an S1S2 restitution pacing protocol defined by the user inputs.The user sets the cycle length and number of beats for S1 pacing, and the range of cycle lengths to apply for the S2.

Conduction velocity
-------------------
Activation times are computed for each S2 beat of the pacing protocol using the openCARP option LATs (see tutorial X). CV is then computed along the cable by taking the difference in activation times at the locations 0.25 cm and 0.75 cm divided by the distance between the two points.

To run the experiments of this tutorial do

.. code-block:: bash

    cd ${TUTORIALS}/02_EP_tissue/03D_conduction_velocity_restitution



Usage
=========================
The following optional arguments are available (default values are indicated):

.. code-block:: bash

  ./run.py --help 
    --Gil               Default: 0.3650 S/m
                        Intracellular longitudinal tissue conductivity
    --Gel               Default: 1.3111 S/m
                        Extracellular longitudinal tissue conductivity
    --nbeats            Default: 5
                        Number of beats for S1 pacing at CI1
    --CI0               Default: 300 ms
                        Shortest S2 coupling interval
    --CI1               Default: 500 ms
                        S1 cycle length and longest S2 coupling interval
    --CIinc             Default: 25 ms
                        Decrement for time interval from CI1 to CI0

After running run.py, the CI and CV are output into the ASCII file CVrestitution_tenTusscherPanfilov_bcl\_.....

If the program is run with the ``--visualize`` option, the CV restitution curve in the file above will be plotted using pythons plotting functions. 

Tasks
=========================
1. Determine the minimum S2 CI for the default parameters

2. Determine the effect of decreasing Gil by 75% on the minimum S2 CI.

3. Determine the effect of increasing Gil by 50% on the minimum S2 CI.

Solutions to the tasks
=========================
1. The minimum CI for the default parameters is 325 ms.

.. code-block:: bash

  ./run.py --CI0 300 --visualize --np 2

2. Reducing Gil by 75% slows down CV and shifts the minimum S2 CI to 300 ms.

.. code-block:: bash

  ./run.py --CI0 275 --Gil 0.09125 --visualize --np 2

3. Increasing Gil by 50% speeds up CV and shifts the minimum S2 CI to 375 ms.

.. code-block:: bash

  ./run.py --CI0 275 --Gil 0.5475 --visualize --np 2


.. rubric:: References

.. [#Tusscher2006] ten Tusscher KHWJ, Panfilov AV.
                 **Alternans and spiral breakup in a human ventricular tissue model.**
                 *Am J Physiol Heart Circ Physiol*, 291(3):H1088-H1100, 2006.
                 `[Pubmed] <https://www.ncbi.nlm.nih.gov/pubmed/16565318>`__

"""

EXAMPLE_DESCRIPTIVE_NAME = 'Computing conduction velocity restitution'
EXAMPLE_AUTHOR = 'Jason Bayer <jason.bayer@ihu-liryc.fr>'
GUIinclude = True

import os
import sys

from datetime import date
from carputils import settings
from carputils import tools
from carputils import mesh
from carputils import testing
from matplotlib import pyplot
import matplotlib.pyplot as plt

def parser():
    # Generate the standard command line parser
    parser = tools.standard_parser()
    group  = parser.add_argument_group('experiment specific options')

    # Add arguments    
    group.add_argument('--Gil',
                        type=float, 
                        default=0.3650,
                        help='Intracellular longitudinal tissue conductivity')
    group.add_argument('--Gel',
                        type=float, 
                        default=1.3111,
                        help='Extracellular longitudinal tissue conductivity')
    group.add_argument('--nbeats',
                        type=int, 
                        default=5,
                        help='Number of beats for S1 pacing at CI1')
    group.add_argument('--CI0',
                        type=int, 
                        default=275,
                        help='Shortest coupling interval')
    group.add_argument('--CI1',
                        type=int, 
                        default=500,
                        help='Longest coupling interval')
    group.add_argument('--CIinc',
                        type=int, 
                        default=25,
                        help='Decrement for coupling interval')

    return parser

def plotResults(ci,cv,xmin,xmax,ymin,ymax, webgui, idExp):
    if (webgui):
        datadic = {'labels':{'labelsAB':[]}, \
                'datasets':{'xlim':[xmin,xmax], 'ylim':[ymin,ymax],'labelXY':['Coupling Interval (ms)', 'Conduction velocity (cm/s)'],\
                'valueX':ci, 'valueY1':cv, 'valueY2':[]}}
        
        with open(idExp + "_" + "matplotM.txt", 'w') as f:
            for key, value in datadic.items():
                for key2, value2 in value.items():
                    f.write('%s\n' % (value2))
    else:
        # Plot CV vs DI
        fig = pyplot.figure()
        ax = fig.add_subplot(1,1,1)

        ax.plot(ci, cv, 'rx-')

        ax.set_xlabel('Coupling Interval (ms)')
        ax.set_ylabel('Conduction velocity (cm/s)')
        ax.set_ylim(ymin,ymax)
        ax.set_xlim(xmin,xmax)

        pyplot.show()

def jobID(args):
    today = date.today()
    ID = '{}__Gil-{}_Gel-{}_CI0-{}_CI1-{}'.format(today.isoformat(), args.Gil, args.Gel, args.CI0, args.CI1)
    return ID

@tools.carpexample(parser, jobID, clean_pattern='^(\d{4}-\d{2}-\d{2})|(mesh)|(.sv)|(.log)|(.txt)|(.dat)|^(imp_)')
def run(args, job):

    #Run restituteCV
    cmd = [settings.execs.restituteCV, 
    #cmd = [ 'restituteCV',
            '--model', 'tenTusscherPanfilov',
            '--CI0', args.CI0,
            '--CI1', args.CI1,
            '--CIinc', args.CIinc,
            '--numCycles', args.nbeats,
            '--bcl', args.CI1,
            '--gi', args.Gil,
            '--ge', args.Gel ]
      
    # run tuneCV 
    job.bash(cmd)

    #Process the data into two columns
    if args.visualize and not args.dry:
        cvfile = './CVrestitution_tenTusscherPanfilov_bcl_' + str(args.CI1) + '_beats_' + str(args.nbeats) + '.dat'
        file = open(cvfile, 'r')
        lines=file.readlines()
        ci = []
        cv = []
        cnt = -2
        diffci = 0
        diffcv = 0
        for line in lines:
            if cnt > -1:
                p = line.split()
                if  p[1] != 'inf':
                    ci.append(float(p[0]))
                    cv.append(float(p[1]))                    
            cnt += 1
        file.close()
        plotResults(ci,cv,min(ci)-10,max(ci)+10,min(cv)-0.1,max(cv)+0.1,args.webGUI,args.ID)

if __name__ == '__main__':
    run()
