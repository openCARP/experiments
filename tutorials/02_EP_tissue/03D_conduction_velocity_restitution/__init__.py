__title__ = 'CV restitution'
__description__ = 'This example demonstrates how to compute conduction velocity restitution in cardiac tissue'
__q2a_tags__ = 'experiments,examples,tissue'