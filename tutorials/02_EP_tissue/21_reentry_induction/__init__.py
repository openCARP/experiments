__title__ = 'Reentry induction'
__description__ = 'This example shows the influence of different induction protocol on reentry initiation and maintenance'
__image__ = '/images/02_21_beat2.gif'
__q2a_tags__ = 'experiments,examples,tissue,lat,reentry'