#!/usr/bin/env python

r"""
.. _tutorial-protocols:

Overview
========

Computational studies can be a useful tool to get deeper insights into 
reentrant arrhythmias, such as atrial fibrillation (AF). Many different 
protocols have been proposed to test AF inducibility both in vivo and 
in silico. In this tutorial, we introduce openCARP examples of some 
state-of-the-art methods as the rapid pacing (RP) and the phase singularity 
distribution method (PSD). Furthermore, we cover the recently proposed 
PEERP (pacing at the end of the effective period) protocol to induce 
arrhythmia [#azzolinPEERP]_. In [#azzolinPEERP]_, all these protocols  
were applied to bi-atrial volumetric models. Here, we present a simple tissue 
patch setup simulation to reduce the computational cost but still demonstrate 
the fundamental properties of each method.

Objectives
==========

This tutorial aims to:

* Provide access to state-of-the-art reentry induction protocols.

* Examine the effect of the different parameters in each protocol on arrhythmia
  induction.

* Highlight that the choice of the inducing protocol has an influence on both
  initiation and maintenance of arrhythmia.


Setup
=====

A 2D tissue patch of size 5 cm x 5 cm and average edge length of 0.4 mm is 
generated. We include a variant of the Courtemanche et al. ionic model 
reflecting AF-induced remodeling [#loewe14a]_ 
to simulate the case of persistent AF. A conduction velocity of 0.3 m/s is
obtained by adjusting intra- and extracellular conductivities. A circular 
fibrotic region of radius 1.42 cm is added at the center of the tissue.
To account for the presence of scar tissue, we set 30% of the elements in this 
fibrotic region to almost non-conductive (1e-7 S/m). 
In the other 70%, several ionic conductances were rescaled (50% ``gK1``, 
60% ``gNa`` and 50% ``gCaL``) to consider effects of cytokines[#roneyfib]_.

To run the experiments of this example, change directories as follows:

.. code-block:: bash

   cd ${TUTORIALS}/02_EP_tissue/21_reentry_induction


Protocols
=========

The protocols included in this tutorial are:

* **Prepace:**
    consists of a series of pulses at a fixed basic cycle length to let the
    tissue reach a stable limit cycle. An activation time map is computed for
    the last beat. This method will generate an intermediate state to be loaded
    in the other protocols. This is not a protocol to induce arrhythmia.

* **Rapid pacing:**
    consists of a train of electrical stimulations with decreasing coupling 
    interval. You can give multiple beats with the same cycle length and 
    check for arrhythmia induction:
    #) at the end of the protocol (RP:math:`_\mathrm{E}`)
    #) after each beat (RP:math:`_\mathrm{B}`).

* **Phase singularity distribution:**
    consists of manually placing phase singularities on the geometrical model
    and then solving the Eikonal equation to estimate the activation time map 
    [#PSD]_. Based on this initial state, you can simulate electrical wave  
    propagation by solving the monodomain equation.

* **Pacing at the end of the effective refractory period:**
    triggers ectopic beats at the end of the effective refractory period, 
    automatically computed as the minimum coupling interval at which the  
    action potential could propagate in the tissue. Method presented in 
    Azzolin et al. [#azzolinPEERP]_.


We suggest to run the experiments with multiple processors to speed up the 
simulation (``--np >2``).


Main Input Parameters
=====================

The following input parameters are exposed to steer the experiment:
 
::
 
  --protocol {prepace,RP_E,RP_B,PSD,PEERP}     
                        Protocol to run (default: PEERP)
  --slabsize SLABSIZE 
                        Block side length in [um] (default: 50000 um).
  --resolution RESOLUTION
                        mesh resolution in [um] (default: 400 um).
  --cv cv
                        conduction velocity in m/s (default: 0.3 m/s)              


.. _prepace_exp:

Prepace experiment
==================

We can get close to a stable limit cycle in the tissue simulation by running a 
prepace protocol to prepare for the actual arrhythmia induction protocols. 
By default, it will stimulate 5 planar waves beats at a basic 
cycle length of 500 ms from the left side of the block and compute an activation
map of the last beat.

.. code-block:: bash

   ./run.py --np 2 --protocol prepace --prepace_bcl 500 --prebeats 4 --visualize

We show the activation spread in terms of transmembrane voltage V:math:`_\mathrm{m}`
during prepacing in :numref:`fig-tutorial-protocols-prepace`.

.. _fig-tutorial-protocols-prepace:

.. figure:: /images/02_21_prepace.gif
   :width: 75%
   :align: center

   Prepace of the tissue patch. 
   The spread of activation and repolarization is shown in terms of 
   transmembrane voltage V:math:`_\mathrm{m}`.


Phase singularity distribution experiment
=========================================

We place a phase singularity at the center of the tissue patch and solve the 
monodomain system using the last activation time map obtained by solving the 
Eikonal solution as initial state [#PSD]_].

.. code-block:: bash

   ./run.py --np 2 --protocol PSD --visualize

The phase map produced by solving the Laplace equation after placing a 
phase singularity in the center is shown 
in :numref:`fig-tutorial-protocols-laplace`.

.. _fig-tutorial-protocols-laplace:

.. figure:: /images/02_21_phase_laplace.png
   :width: 75%
   :align: center
 
   The phase ranges from -pi to +pi.

The last activation time yielded by the Eikonal system is shown in 
:numref:`fig-tutorial-protocols-LAT`.

.. _fig-tutorial-protocols-LAT:

.. figure:: /images/02_21_LAT.png
   :width: 75%
   :align: center
 
   Last activation time map ranging from 0 to 168 ms.

The spread of activation in terms of transmembrane voltage V:math:`_\mathrm{m}`
is shown in :numref:`fig-tutorial-protocols-PSD`. The activation time map 
obtained by solving the Eikonal equation was used to initalize a mondomain 
simulation.

.. _fig-tutorial-protocols-PSD:

.. figure:: /images/02_21_PSD.gif
   :width: 75%
   :align: center
 
   The spread of activation and repolarization is shown in terms of 
   transmembrane voltage V:math:`_\mathrm{m}`.


.. _RP_E:

Rapid pacing with arrhythmia checking at the end of the experiment
==================================================================

The rapid pacing with arrhythmia checking at the end consists of a train of 
pulses with decreasing coupling interval and the success of arrhythmia 
initiation is inspected only after all electrical stimulations were applied. 
The state saved after prepacing (:ref:`prepace experiment <prepace_exp>`) is 
used as initial state.

To run the example, execute:

.. code-block:: bash

   ./run.py --np 2 --protocol RP_E --start_bcl 200 --end_bcl 130 --max_n_beats_RP 1 --visualize

We will get a sustained reentry after the end of the pacing protocol.
We show the activation spread in terms of transmembrane voltage 
V:math:`_\mathrm{m}` in :numref:`fig-tutorial-protocols-RP_E_130`.

.. _fig-tutorial-protocols-RP_E_130:

.. figure:: /images/02_21_RP_E_end_bcl_130.gif
   :width: 75%
   :align: center

   Reentry induced and maintained by implementing a rapid train of stimulations 
   with shortest coupling interval of 130 ms. 
   The spread of activation and repolarization is shown in terms of 
   transmembrane voltage V:math:`_\mathrm{m}`.

In order to investigate what happens if we stop at a coupling interval of 140 ms, run:

.. code-block:: bash

   ./run.py --np 2 --protocol RP_E --start_bcl 200 --end_bcl 140 --max_n_beats_RP 1 --visualize

In this case, we will not get a sustained reentry. Conversely, we can notice how
the last beat is terminating the arrhythmia initiated by the beat before. This highlights
how during a pacing protocol we can also terminate a previoulsy initiated reentry.
We show the activation spread in terms of transmembrane voltage 
V:math:`_\mathrm{m}` in :numref:`fig-tutorial-protocols-RP_E_140`.

.. _fig-tutorial-protocols-RP_E_140:

.. figure:: /images/02_21_RP_E_end_bcl_140.gif
   :width: 75%
   :align: center

   No reentry induced by a rapid train of stimulations with shortest coupling 
   interval of 140 ms. The spread of activation and repolarization is shown in 
   terms of transmembrane voltage V:math:`_\mathrm{m}`.


Rapid pacing with arrhythmia checking after every beat experiment
=================================================================

The rapid pacing with arrhythmia checking after every beat consists of a train 
of pulses with decreasing coupling interval and the success of arrhythmia 
initiation is inspected after each stimulation. If an arrhythmia is induced, we 
will no longer stimulate. The state saved after prepacing 
(:ref:`prepace experiment <prepace_exp>`) is used as initial state.

To run the example:

.. code-block:: bash

   ./run.py --np 2 --protocol RP_B --start_bcl 200 --end_bcl 130 --max_n_beats_RP 1 --visualize

We get a sustained reentry after the beat with a coupling interval of 150 ms, 
therefore the algorithm will stop pacing even if the the shortest coupling 
interval was not reached yet. This means that it could happen to induce and 
maintain a reentrant wave even without giving the whole train of 
pulses as in the previous experiment (which gives the reentry induced by a 
coupling interval of 130 ms as final output).


Pacing at the end of the effective refractory period experiment
===============================================================

We use the state saved after prepacing (:ref:`prepace experiment <prepace_exp>`)
as initial state. The first ectopic beat is given at the minimum coupling 
interval time which allows for wave propagation.
The timing of the ectopic beat V:math:`n+1` is defined by identifing the end of the 
refractory period of beat V:math:`n` automatically.

To run the example:

.. code-block:: bash

   ./run.py --np 2 --protocol PEERP --max_n_beats_PEERP 2 --visualize

We induce a reeentrant wave with only 2 ectopic beats after the last prepacing 
beat. We show the activation spread resulting from the first beat in terms of 
transmembrane voltage V:math:`_\mathrm{m}` in :numref:`fig-tutorial-protocols-PEERP_1_no`.

.. _fig-tutorial-protocols-PEERP_1_no:

.. figure:: /images/02_21_beat1_no_reentry.gif
   :width: 75%
   :align: center

   One beat at the end of the effective refractory period did not induce 
   reentrant arrhythmia. 

We show the activation spread resulting from the second beat in terms of 
transmembrane voltage V:math:`_\mathrm{m}` in :numref:`fig-tutorial-protocols-PEERP_2`.

.. _fig-tutorial-protocols-PEERP_2:

.. figure:: /images/02_21_beat2.gif
   :width: 75%
   :align: center

   The second ectopic beat induced a sustained arrhythmia. 

.. rubric:: References

.. [#azzolinPEERP]  *Azzolin L, Doessel O, Loewe A*,
                     **A Reproducible Protocol to Assess Arrhythmia Vulnerability in silico: Pacing at the End of the Effective Refractory Period.**, 
                     Frontiers in Physiology. 2021 April;12:656411.
                     Doi: 10.3389/fphys.2021.656411
                     `[Journal] <https://www.frontiersin.org/articles/10.3389/fphys.2021.656411/full>`__

..  [#loewe14a]     *Loewe A, Wilhelms M, Doessel O, Seemann G*
                     **Influence of chronic atrial fibrillation induced remodeling in a computational electrophysiological model.**
                     Biomedical Engineering. 2014 Jan;59(suppl 1):S929--S932.
                     Doi: 10.1515/bmt-2014-5012__

..  [#roneyfib]     *Roney CH, Bayer JD, Zahid S, Meo M, Boyle PM, Trayanova NA, Haissaguerre M, Dubois R, Cochet H, 
                     Vigmond EJ*
                     **Modelling methodology of atrial fibrosis affects rotor dynamics and electrograms.**
                     Europace. 2016 Dec;18(suppl 4):iv146-iv155.
                     `[Pubmed] <https://pubmed.ncbi.nlm.nih.gov/28011842>`__
                     `[PMC] <https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6279153/>`__

..  [#PSD]          *Jacquemet V*
                     **An Eikonal approach for the initiation of reentrant cardiac propagation in reaction-diffusion models.** 
                     IEEE Trans Biomed Eng. 2010;57(9):2090-2098.
                     `[Pubmed] <https://pubmed.ncbi.nlm.nih.gov/20515704>`__


"""
import os
EXAMPLE_DESCRIPTIVE_NAME = 'Reentry induction protocols'
EXAMPLE_AUTHOR = 'Luca Azzolin <luca.azzolin@kit.edu>'
EXAMPLE_DIR = os.path.dirname(__file__)
GUIinclude = False

import sys
import numpy as np
import shutil
import random
from scipy import spatial
from numpy import linalg as LA
from datetime import date

from carputils.carpio import txt
from carputils import settings
from carputils import tools
from carputils import mesh
from carputils import model

def parser():
    parser = tools.standard_parser()
    group  = parser.add_argument_group('experiment specific options')
    group.add_argument('--model',
                        type = str,
                        default = "Courtemanche", 
                        help='ionic model name ')
    group.add_argument('--giL', default=0.14, help='intracellular longitudinal conductivity')
    group.add_argument('--geL', default=0.7, help='extracellular longitudinal conductivity')
    group.add_argument('--cv',
                        type=float, 
                        default=0.3,
                        help='conduction velocity in m/s')
    group.add_argument('--protocol',
                        default = "PEERP", type = str,
                        help = 'Protocol used to induce arrhythmia: \
                        "prepace": Prepace the tissue with the given bcl and number of beats, this is needed for ; \
                        "RP_E": Rapid Pacing with activity checking at the End of the protocol; \
                        "RP_B": Rapid Pacing with activity checking after each Beat; \
                        "PSD": Phase Singularity Distribution method; \
                        "PEERP": Pacing at the End of the Effective Refractory Period')
    group.add_argument('--slabsize',
                        default = 50000., type = float,
                        help = 'Side length of 3D slab (tetra) in microns.')
    group.add_argument('--resolution',
                        default = 400., type = float,
                        help = 'Edge length of triangular slab elements in microns.')
    group.add_argument('--M_lump',
                        type=int,
                        default='1',
                        help='set 1 for mass lumping, 0 otherwise. Mass lumping will speed up the simulation. Use with regular meshes.')
    group.add_argument('--stim_size',
                        type=str,
                        default='2000.0',
                        help='stimulation edge square size in micrometers')
    group.add_argument('--prepace_strength',
                        type=float, default=70,
                        help='prepacing injected transmembrane current in uA/cm^2 (2Dcurrent) or uA/cm^3 (3Dcurrent)')
    group.add_argument('--strength',
                        type=float, default=30,
                        help='stimulation transmembrane current in uA/cm^2 (2Dcurrent) or uA/cm^3 (3Dcurrent)')
    # Prepace
    group.add_argument('--prepace_bcl',
                        type=float, default=500.0,
                        help='initial basic cycle length in ms')

    group.add_argument('--prebeats',
                        type = int,
                        default = 4, 
                        help='Number of beats to prepace the tissue')
    
    # Parameters of PSD
    group.add_argument('--PSD_bcl',
                        type=float,
                        default=160,
                        help='BCL in ms')   
    group.add_argument('--radius',
                        type=float, 
                        default=10000.0,
                        help='radius of circles in which to set the phase from -pi to +pi')
    group.add_argument('--seeds',
                        type=int, 
                        default=50,
                        help='# of initial seeds in which to set the phase from -pi to +pi')
    group.add_argument('--chirality',
                        type=int, 
                        default=-1,
                        help='Chirality of the rotation: -1 for counterclockwise, 1 for clockwise')

    # Parameters of RP
    group.add_argument('--step',
                        type=float, 
                        default=10,
                        help='Step to decrease BCL in ms')
    group.add_argument('--max_n_beats_RP',
                        type=int,
                        default=1,
                        help='Max number of beats for th RP')
    group.add_argument('--start',
                        type=float, default=2000.0,
                        help='initial basic cycle length in ms')
    group.add_argument('--start_bcl',
                        type=float, default=200.0,
                        help='initial basic cycle length in ms')
    group.add_argument('--end_bcl',
                        type=float, default=130.0,
                        help='last basic cycle length in ms')

    # Parameters of PEERP
    group.add_argument('--max_n_beats_PEERP',
                        type=int,
                        default=2,
                        help='Max number of beats for the PEERP')
    group.add_argument('--APD_percent',
                        type=float, default=94.0,
                        help='action potential duration percentage to set as first guess to find the end of the effective refractory period')
    group.add_argument('--tol',
                        type=float, default=1.0,
                        help='tolerance to compute the end of the effective refractory period')
    return parser

def jobID(args):
    """
    Generate name of top level output directory.
    """
    today = date.today()
    out_DIR = '{}_{}'.format(today.isoformat(), args.protocol)
    return out_DIR

@tools.carpexample(parser, jobID)
def run(args, job):

    converge_CV(args,job)
    meshname, geom, mesh = setupMesh(args)

    fibrosis_dir = "fibrosis_block_{}um_resolution_{}um".format(args.slabsize, args.resolution)
    
    f_slow_conductive = fibrosis_dir + '/elems_slow_conductive'
    f_fib = fibrosis_dir + '/f_fib'
    if os.path.isfile(f_slow_conductive + '.regele') or not os.path.isfile(f_fib + '.regele'):
        elems = np.loadtxt(meshname+'.elem', skiprows=1, usecols=(1,2,3),dtype=int)
        pts = np.loadtxt(meshname+'.pts', skiprows=1)
        centroids = []
        for elem in elems:
            centroid = tri_centroid(pts, elem)
            centroids.append(centroid)
        
        tree = spatial.cKDTree(centroids)
        
        random.seed(1)
        centre = np.asarray([args.slabsize/2, args.slabsize/2, 0.])
        elements_in_fibrotic_reg = tree.query_ball_point(centre, args.slabsize/3.5)
        elems_not_conductive = set(random.sample(elements_in_fibrotic_reg, int(len(elements_in_fibrotic_reg)*0.3)))
        elements_in_fibrotic_reg = set(elements_in_fibrotic_reg) - elems_not_conductive

        try:
            os.makedirs(fibrosis_dir)
        except OSError:
            print ("Creation of the directory %s failed" % fibrosis_dir)
        else:
            print ("Successfully created the directory %s " % fibrosis_dir)

        file = open(f_slow_conductive + '.regele', 'w')
        file.write(str(len(elems_not_conductive)) + '\n')
        for i in elems_not_conductive:
            file.write(str(i) + '\n')
        file.close()

        file = open(f_fib + '.regele', 'w')
        file.write(str(len(elements_in_fibrotic_reg)) + '\n')
        for i in elements_in_fibrotic_reg:
            file.write(str(i) + '\n')
        file.close()

    # Region dynamic retagging
    
    dyn_reg = ['-numtagreg', 2 ]
    dyn_reg.extend(tagregopt(0, 'type',        4))
    dyn_reg.extend(tagregopt(0, 'elemfile',    f_fib))
    dyn_reg.extend(tagregopt(0, 'tag',         2))
    dyn_reg.extend(tagregopt(1, 'type',        4))
    dyn_reg.extend(tagregopt(1, 'elemfile',    f_slow_conductive))
    dyn_reg.extend(tagregopt(1, 'tag',         3))
    # Add all the non-general arguments
    cmd = tools.carp_cmd() 

    cmd += ['-meshname',   meshname,
            '-gridout_i',  2, 
            '-experiment', 3,
            '-simID',      job.ID]
    cmd += dyn_reg

    job.carp(cmd)

    # Add all the general arguments
    cmd = tools.carp_cmd(os.path.join(EXAMPLE_DIR, 'parameters.par'))

    cmd += ['-num_gregions', 2,
            '-gregion[0].num_IDs ', 2,
            '-gregion[0].ID[0]', 1,
            '-gregion[0].ID[1]', 2,
            '-gregion[0].g_il ', args.giL,
            '-gregion[0].g_it ', args.giL, 
            '-gregion[0].g_in ', args.giL,
            '-gregion[0].g_el ', args.geL,
            '-gregion[0].g_et ', args.geL,
            '-gregion[0].g_en ', args.geL,
            '-gregion[1].num_IDs ', 1,
            '-gregion[1].ID[0]', 3,
            '-gregion[1].g_il ', 0.0000001,
            '-gregion[1].g_it ', 0.0000001, 
            '-gregion[1].g_in ', 0.0000001,
            '-gregion[1].g_el ', 0.0000001,
            '-gregion[1].g_et ', 0.0000001,
            '-gregion[1].g_en ', 0.0000001]
    
    meshname = job.ID+'/block_i'
    simid = job.ID
    
    steady_state_dir = "prepace_block_{}um_resolution_{}um_cv_{}_with_{}_beats_at_{}_bcl_lump_{}".format(args.slabsize, args.resolution, args.cv, args.prebeats, args.prepace_bcl, args.M_lump)

    # Define sinus node stimulus location
    sinus = mesh.block_bc_opencarp(geom, 'stim', 0, 'x')

    # Run protocol
    if args.protocol == "PSD":
        centre = np.asarray([args.slabsize/2., args.slabsize/2., 0.])
        centre_str = ','.join([str(i) for i in centre])
        if not os.path.exists("endo"):
            os.makedirs("endo")
        os.system("meshtool extract surface -msh={} -surf=endo/endo -ofmt=carp_txt -coord={} -edge=30".format(meshname, centre_str))
        xyz = np.loadtxt('endo/endo.surfmesh.pts', skiprows=1)
        triangles = np.loadtxt('endo/endo.surfmesh.elem', skiprows=1, usecols = (1,2,3), dtype = int)
        model.induceReentry.PSD(args, job, cmd, meshname, xyz, triangles, centre)
    elif args.protocol == "prepace":
        tissue_init = single_cell_initialization(args, job, steady_state_dir)
        model.prepace.prepace(args, job, cmd, meshname, sinus, steady_state_dir, tissue_init)
    elif args.protocol == "RP_B":
        if not os.path.isfile(steady_state_dir+'/vm_last_beat.igb'):
            print("Prepacing the tissue model")
            tissue_init = single_cell_initialization(args, job, steady_state_dir)
            model.prepace.prepace(args, job, cmd, meshname, sinus, steady_state_dir, tissue_init)
        centre = np.asarray([args.slabsize/7., args.slabsize/2., 0.])
        simid = model.induceReentry.RP_B(args, job, cmd, meshname, centre, steady_state_dir)
    elif args.protocol == "RP_E":
        if not os.path.isfile(steady_state_dir+'/vm_last_beat.igb'):
            print("Prepacing the tissue model")
            tissue_init = single_cell_initialization(args, job, steady_state_dir)
            model.prepace.prepace(args, job, cmd, meshname, sinus, steady_state_dir, tissue_init)
        centre = np.asarray([args.slabsize/7., args.slabsize/2., 0.])
        simid = model.induceReentry.RP_E(args, job, cmd, meshname, centre, steady_state_dir)
    elif args.protocol == "PEERP":
        if not os.path.isfile(steady_state_dir+'/vm_last_beat.igb'):
            print("Prepacing the tissue model")
            tissue_init = single_cell_initialization(args, job, steady_state_dir)
            model.prepace.prepace(args, job, cmd, meshname, sinus, steady_state_dir, tissue_init)
        centre = np.asarray([args.slabsize/7., args.slabsize/2., 0.])
        simid = model.induceReentry.PEERP(args, job, cmd, meshname, sinus, centre, steady_state_dir)
    else:
        print("Protocol not implemented, please choose one protocol between ones listed")
        sys.exit()

    # Do visualization
    if args.visualize:

        if args.protocol == "prepace":
            data = os.path.join(steady_state_dir, 'vm_last_beat.igb')
        else:
            data = os.path.join(simid, 'vm.igb')

        job.meshalyzer(meshname, data)


def setupMesh(args):
    # user input
    UM2MM      = 1./1000
    slabsize   = args.slabsize * UM2MM                      # in mm
    resolution = args.resolution * UM2MM                    # in mm
    thickness  = 0.0 * UM2MM
    centre     = np.asarray([slabsize/2., slabsize/2., thickness/2.]) # in mm

    # generate mesh (units in mm) and return basename
    # lower left of the mesh will be the origin
    geom = mesh.Block(size = (slabsize,    slabsize,   thickness),
                      centre = centre,
                      etype = 'tri', 
                      resolution = resolution)

    meshname = mesh.generate(geom)

    return meshname, geom, mesh

def remove_trash(args, job):
    for dirname, dirnames, filenames in os.walk('.'):
        for folder in dirnames:
            if folder.startswith("imp_") or folder.startswith("meshes"):
                shutil.rmtree(folder, ignore_errors = True)
                
        for file in filenames:
            if file.startswith(args.model):
                if os.path.isfile(file):
                    os.remove(file)
                    
    if os.path.isfile('tuneCV.log'):
        os.remove('tuneCV.log')
    if os.path.isfile('Trace_0.dat'):
        os.remove('Trace_0.dat')
    if os.path.isfile('results.dat'):
        os.remove('results.dat')    
    
def converge_CV(args,job):
    length = 2   
    tol = 0.001
    lumping = 'False'
    if args.M_lump:
        lumping = 'True'
    # create command line   
    cmd = [settings.execs.TUNECV, 
            '--model', args.model,
            '--modelpar', "GCaL-55%,GK1+100%,factorGKur-50%,Gto-65%,GKs+100%,maxIpCa+50%,maxINaCa+60%,GKr*1.6",
            '--converge', 'True',
            '--velocity', str(args.cv),
            '--resolution', str(args.resolution), 
            '--gi', str(args.giL), 
            '--ge', str(args.geL), 
            '--dt', 20, 
            '--tol', str(tol), 
            '--length', str(length),
            '--lumping', lumping,
            '--silent']

    # run tuneCV 
    job.bash(cmd)

    #read results file and update Gil and Gel of this model
    tuning_results = np.loadtxt('results.dat')
    args.giL = tuning_results[1]
    args.geL = tuning_results[2]
    print (args.model , ' Tissue conductivities for CV = ' , tuning_results[0])
    print ('---------------------------------------------')
    print ('gi = ' , tuning_results[1], 'ge = ' , tuning_results[2])
    print ('---------------------------------------------')

    print ('gi (S/m)' , args.giL)
    print ('ge (S/m)', args.geL)  
    remove_trash(args,job)

def tagregopt( reg, field, val ) :
    return ['-tagreg['+str(reg)+'].'+field, val ]

def tri_centroid(nodes, element):
    x1 = nodes[element[0],0]
    x2 = nodes[element[1],0]
    x3 = nodes[element[2],0]

    y1 = nodes[element[0],1]
    y2 = nodes[element[1],1]
    y3 = nodes[element[2],1]

    z1 = nodes[element[0],2]
    z2 = nodes[element[1],2]
    z3 = nodes[element[2],2]

    return [(x1+x2+x3)/3, (y1+y2+y3)/3, (z1+z2+z3)/3]

def single_cell_initialization(args, job, steady_state_dir):

    duration = 20*args.prepace_bcl
    
    GCaL = [0.45, 0.225]
    GK1 = [2, 1]
    GNa = [1, 0.6]
    factorGKur = [0.5, 0.5]
    Gto = [0.35, 0.35]
    GKs = [2, 2]
    maxIpCa = [1.5, 1.5]
    maxINaCa = [1.6, 1.6]
    GKr = [1.6, 1]

    tissue_init = []
    for k in range(len(GCaL)):
        init_file =  steady_state_dir+'/init_values_stab_{}_bcl_{}.sv'.format(k,args.prepace_bcl)
        cmd = [settings.execs.BENCH,
        '--imp',                args.model,
        '--imp-par',            'GCaL*{},GK1*{},GNa*{},factorGKur*{},Gto*{},GKs*{},maxIpCa*{},maxINaCa*{}'.format(GCaL[k],GK1[k],GNa[k],factorGKur[k],Gto[k],GKs[k],maxIpCa[k],maxINaCa[k]),
        '--bcl',                args.prepace_bcl,
        '--dt-out',             duration,
        '--stim-curr',          9.5,
        '--stim-dur',           2,
        '--numstim',            20,
        '--duration',           duration, 
        '--stim-start',         0,
        '--dt',                 20/1000,
        '--fout='               + steady_state_dir + '/tVI_stabilization_{}'.format(k),
        '-S',                   duration,
        '-F',                   init_file,
        '--no-trace',           'on']

        job.bash(cmd)

        tissue_init += ['-imp_region[{}].im_sv_init'.format(k), init_file]

    return tissue_init
    
if __name__ == '__main__':
    run()

