__title__ = 'Boundary conditions'
__description__ = 'Computing Laplace-Dirichlet maps provide an elegant tool for describing the distance between defined boundaries'
__image__ = '/images/02_13_laplace.png'
__q2a_tags__ = 'experiments,examples,tissue,solver'