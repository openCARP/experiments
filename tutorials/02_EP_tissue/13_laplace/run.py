#!/usr/bin/env python

"""

.. _tutorial-laplace-solver:

Introduction
============

Computing Laplace-Dirichlet maps provide an elegant tool for describing the
distance between defined boundaries. Specialized software routines exploiting
these maps in order to assign :ref:`ventricular fibers and sheets
<tutorial_laplace-dirichlet-fibers>` or to determine the set of elements to
receive :ref:`heterogeneous conductivities <tutorial-conductive-heterogeneity>`
are frequently used in the :ref:`carputils framework <carputils>`. To run the experiments of this example change directories as follows:

.. code-block:: bash

   cd ${TUTORIALS}/02_EP_tissue/13_laplace
   


Experimental Setup
==================

The geometry and the electrodes are defined as follows:

.. _fig-laplace-solve-setup:

.. figure:: /images/02_13_laplace.png
    :width: 50 %
    :align: center

    An extracellular voltage stimulus (STIM) is applied to the lower boundary
    of a quadratic tetrahedral FE model with dimensions 10.0mm x 0.1mm x 10.0mm.
    The ground electrode (GND) is assigned to the left face side of mesh.


Problem-specific openCARP Parameters
=====================================

The relevant part of the .par file for this example is shown below:

::

    experiment           = 2        # perform Laplace solve only
    bidomain             = 1

    # ground electrode
    stim[0].elec.p0[0]       =   -50.   # par-file units are always microns!
    stim[0].elec.p1[0]       =    50.
    stim[0].elec.p0[1]       =   -50.
    stim[0].elec.p1[1]       = 10050.
    stim[0].elec.p0[2]       =   -50.
    stim[0].elec.p1[2]       =   150.
    stim[0].crct.type        =     3    # extracellular ground

    # stimulus electrode
    stim[1].elec.p0[0]       =   -50.
    stim[1].elec.p1[0]       = 10050.
    stim[1].elec.p0[1]       =   -50.
    stim[1].elec.p1[1]       =    50.
    stim[1].elec.p0[2]       =   -50.
    stim[1].elec.p1[2]       =   150.
    stim[1].crct.type        =     2    # extracellular voltage
    stim[1].ptcl.duration    =     1.
    stim[1].pulse.strength   =     1.

    # set isotropic conductivities everywhere
    num_gregions         = 1
    gregion[0].g_il      = 1
    gregion[0].g_it      = 1
    gregion[0].g_el      = 1
    gregion[0].g_et      = 1


Experiment
==========

To run this experiment, do

.. code-block:: bash

    ./run.py --visualize


.. Note::

    - Set uniform conductivities everywhere to not disturb the Laplace solution
    - Exclude any geometry parts (e.g. bath) you do not need the Laplace solution for. :ref:`meshtool<mesh-data-extraction>` will help you to reintegrate any computed features back into its 'parent'.
    - openCARP needs the bidomain flag to be set to 1. Otherwise the program will abort with a hint to change this specific setting.
    - One may scale the values of the Laplace solution through setting the stimulus strengths to e.g. 0 (stimulus[0], GND) and 1 (stimulus[1].strength) as shown above.
    - The example collects stimulation nodes by defining an inclosing volume (x0,x0+xd,y0,y0+yd,z0,z0+zd). Alternatively, vertices can directly be addressed by including a :ref:`vertex file<vertex-file>`.

Expected result:

.. _fig-laplace-solve-result:

.. figure:: /images/02_13_laplace_result.png
    :width: 50 %
    :align: center

Nodal Boundary Conditions
=========================

Instead of having to define a different stimulus for each different Dirichlet value, specific nodes can be assigned different values in one file.
The relevant parameters to change are below:

::

    stim[1].ptcl.type =     2    # extracellular voltage
    stim[1].elec.vtx_file =     BCs
    stim[1].elec.vtx_fcn  =     1
    stim[1].pulse.strength =     1.

When ``stim[1].elec.vtx_fcn`` is non-zero, the ``vtx_file``, *BCs.vtx* in this case, has :ref:`vertex adjustment format <vertex-adj-file>` in which the nodal strength is specified along with the node number. 
The nodal strengths in the file are then multiplied by ``stim[1].pulse.strength``.
"""
import os
EXAMPLE_DESCRIPTIVE_NAME = 'Laplace Solver'
EXAMPLE_AUTHOR = 'Anton J Prassl <anton.prassl@medunigraz.at>'
EXAMPLE_DIR = os.path.dirname(__file__)
GUIinclude = True

from datetime import date
from carputils.carpio import txt
from carputils import settings
from carputils import tools
from carputils import mesh
import numpy as np

def parser():
    parser = tools.standard_parser()
    return parser


def jobID(args):
    """
    Generate name of top level output directory.
    """
    today = date.today()
    return '{}_laplace'.format(today.isoformat())


@tools.carpexample(parser, jobID)
def run(args, job):

    # Generate mesh
    # Block which is thin in z direction (in mm)
    geom = mesh.Block(size=(10, 10, 100./1000.), centre=(5.,5.,.05), resolution=100./1000.)
    #geom = mesh.Block(size=(10, 10, 100. / 1000.), resolution=100. / 1000.)
    # Set fibre angle to 0, sheet angle to 0
    geom.set_fibres(0, 0, 90, 90)
    # Generate and return base name
    meshname = mesh.generate(geom)

    # Query for element labels
    _, etags, _ = txt.read(meshname + '.elem')
    etags = np.unique(etags)
    IntraTags = etags.tolist().copy()
    ExtraTags = etags.tolist().copy()

    # Set up conductivity
    cond = ['-num_gregions',    1,
            '-gregion[0].g_il', 1,
            '-gregion[0].g_it', 1,
            '-gregion[0].g_el', 1,
            '-gregion[0].g_et', 1]

    # Define the geometry of the stimulus at one end of the block
    stimuli = ['-num_stim', 2]

    # Add ground
    # stimuli += mesh.block_boundary_condition(geom, 'stim', 0, 'x', True)
    stimuli += ['-stim[0].elec.p0[0]', -50.,
                '-stim[0].elec.p1[0]', 50.,
                '-stim[0].elec.p0[1]', -50.,
                '-stim[0].elec.p1[1]', 10050.,
                '-stim[0].elec.p0[2]', -50.,
                '-stim[0].elec.p1[2]', 150.,
                '-stim[0].crct.type', 3]
    
    stimuli += ['-stim[0].crct.type', 3]

    # Add voltage
    # stimuli += mesh.block_boundary_condition(geom, 'stim', 1, 'y', True)
    stimuli += ['-stim[1].elec.p0[0]', -50.,
                '-stim[1].elec.p1[0]', 10050.,
                '-stim[1].elec.p0[1]', -50.,
                '-stim[1].elec.p1[1]', 50.,
                '-stim[1].elec.p0[2]', -50.,
                '-stim[1].elec.p1[2]', 150.]

    stimuli += ['-stim[1].crct.type', 2,
                '-stim[1].ptcl.start',    0,
                '-stim[1].ptcl.duration', 1,
                '-stim[1].pulse.strength', 1]

    # Get basic command line, including solver options
    ## Add all the non-general arguments
    cmd = tools.carp_cmd()

    cmd += ['-simID',      job.ID,
            '-meshname',   meshname,
            '-experiment', 2, # Laplace solve
            '-bidomain',   1] # Laplace solve needs `bidomain=1`
    cmd += stimuli
    cmd += cond
    cmd += tools.gen_physics_opts(ExtraTags=ExtraTags, IntraTags=IntraTags)

    if args.visualize:
        cmd += ['-gridout_i', 3]
        cmd += ['-gridout_e', 3]

    # Run simulation
    job.carp(cmd, 'Boundary Conditions')

    # Do visualization
    if args.visualize and not settings.platform.BATCH:

        geom = os.path.join(job.ID, os.path.basename(meshname)+'_i')
        data = os.path.join(job.ID, 'phie.igb')
        view = os.path.join(EXAMPLE_DIR, 'laplace.mshz')
        
        # GUI
        if args.webGUI:
            folderNameExp = f'/experiments/13_laplace_{job.ID}'
            os.mkdir(folderNameExp)
            cmdMesh = f'meshtool collect -imsh={geom} -omsh={folderNameExp}/{job.ID} -nod={data} -ifmt=carp_txt -ofmt=ens_bin'
            outMesh = os.system(cmdMesh)
            if(outMesh == 0):
                print('Meshtool - conversion successfully')
            else:
                print('Meshtool - conversion failure')
        
        # Meshalyzer
        else:
            job.meshalyzer(geom, data, view)


if __name__ == '__main__':
    run()
