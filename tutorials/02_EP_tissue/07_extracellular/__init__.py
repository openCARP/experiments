__title__ = 'Extracellular potentials and ECGs'
__description__ = 'This tutorial explains the background of computing extracellular potentials and ECG using different techniques'
__image__ = '/images/02_07_extPotentials_ECG.png'
__q2a_tags__ = 'experiments,examples,tissue,ecg,monodomain,bidomain,pseudo-bidomain'