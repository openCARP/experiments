#!/usr/bin/env python

"""
This example details how to assign a gradient of cell-scale properties using the adjustments interface. To run the experiments of this example change directories as follows:

.. code-block:: bash

   cd ${TUTORIALS}/02_EP_tissue/05E_Smooth_Gradient_Heterogeneities
   


Problem Setup
================================

This example will run one simulation using a 2D sheet model (1 cm x 1 cm) in which all elements and nodes
are in the same imp_region[] and gregion[] but heterogeneity in cell-scale dynamics is imposed at the cell
scale in gradient patterns using the adjustments interface.

By default, :math:`I_\mathrm{Kr}` is modulated from 0x to 5x from left to right and
:math:`I_\mathrm{to}` is modulated from 0x to 5x from bottom to top.
Both gradients are linear. The resulting pattern of excitation is shown below:

.. _fig-smooth-gradient-heterogeneities:

.. figure:: /images/02_05E_Smooth_Gradient_Heterogeneities_Fig1_Animation.gif
    :scale: 100%
    :align: center

    Membrane voltage over time (:math:`V_\mathrm{m}(t)`) for the example in which
    :math:`I_\mathrm{Kr}` is modulated from 0x to 5x from left to right and
    :math:`I_\mathrm{to}` is modulated from 0x to 5x from bottom to top.
    Different-coloured stars indicate points where AP traces are shown in
    the graph below (:numref:`fig-smooth-gradient-heterogeneities-APs`).


The stars are colour-coded and indicate the approximate locations from which the action potential traces
shown below were extracted:

.. _fig-smooth-gradient-heterogeneities-APs:
    
.. figure:: /images/02_05E_Smooth_Gradient_Heterogeneities_Fig2_Example_APs.png
    :scale: 60%
    :align: center

    Action potential traces extracted from the four points indicated in
    :numref:`fig-smooth-gradient-heterogeneities`.

As expected, :math:`I_\mathrm{to}` modulation leads to an abolished notch along the bottom edge of the sheet but
an exaggerated notch along the top edge. :math:`I_\mathrm{kr}` modulation in the sheet leads to a progressive
shortening of action potential duration from left to right as more repolarizing current becomes available
to the cells.

Usage
================================

The following optional arguments are available (default values are indicated):

.. code-block:: bash

  ./run.py --help
    --xgrad_var         Options: {GCaL,GKs,GKr,Gto}, Default: GKr
                        parameter that should be varied along the left-to-right (X) gradient
    --xgrad_left_scf    Default: 0.0
                        scaling factor at left side of the X gradient
    --xgrad_right_scf   Default: 5.0
                        scaling factor at right side of the Y gradient
    --xgrad_flip        left becomes right, right becomes left

    --ygrad_var         Options: {GCaL,GKs,GKr,Gto}, Default: Gto
                        parameter that should be varied along the bottom-to-top (Y) gradient
    --ygrad_bottom_scf  Default: 0.0
                        scaling factor at bottom of Y gradient
    --ygrad_top_scf     Default: 5.0
                        scaling factor at top of Y gradient
    --ygrad_flip        down becomes up, up becomes down

If the program is run with the ``--visualize`` option, meshalyzer will automatically
load the :math:`V_\mathrm{m}(t)` sequence corresponding to the run simulation. Output files for
activation and repolarisation sequences as well as APD are produced for each simulation
and can be found in the output directory and loaded into meshalyzer.

The --xgrad[...] and --ygrad[...] parameters can be modified to impose different gradients
in the x and y directions (i.e., left-to-right and bottom-to-top, respectively) in four
different parameters: :math:`G_\mathrm{CaL}`, :math:`G_\mathrm{Ks}`, :math:`G_\mathrm{Kr}`, and :math:`G_\mathrm{to}`,
which correspond to the L-type :math:`Ca^{2+}`, slow delayed rectifier :math:`K^{+}`,
rapid delayed rectifier :math:`K^{+}`, and transient outward :math:`K^{+}` currents, respectively.

Notes and Precautions
================================

* The ``--xgrad_var`` and ``--ygrad_var`` parameters should not be set to the same value,
  otherwise the simulation will not behave as expected.
* The ``-adjustments[]`` interface in openCARP can only be used to modify cell-scale
  properties that correspond to a state variable in the ionic model being used.

Use the :ref:`--imp-info <imp-info>` argument of ``bench``
to find out which variables are eligible to bet set on a nodal basis.
There are actually two types of variables listed as *state variables*, (1) actual state variables which describe the
ionic model state and evolve with each time step, and (2) model parameters which affect behaviour but do not change.
Thus, state variables can be intialized on a nodal basis.
Parameters which can be set on a nodal basis
must be listed as parameters, i.e., being listed up top,
as well as being listed as state variables. For example, in the case of the ``tenTusscherPanfilov``
ionic model used in this example, the following are parameters which can be set nodally:

.. code-block:: bash

  > bench --imp=tenTusscherPanfilov --imp-info
  tenTusscherPanfilov:
  [...]
    State variables:
                         GCaL
                         GKr
                         GKs
                         Gto

Under the Hood
==================
In the parameter file, the following options are used to adjust nodal values. First, if you wish to adjust N variables::

    num_adjustments = N

For each adjustment structure, i.e., ``adjustment[0], ..., adjustment[N-1]``, the following fields are set:

======== ==============================  ======================================================================
Field    Meaning                         Value
======== ==============================  ======================================================================
file     file specifying adjustment      see the :ref:`vertex-adj-file` format
variable variable to adjust              There are 2 forms. For global variables (Vm,Lambda,delLambda,Na_e,Ca_e,...)
                                         it is simply the variable.
                                         For ionic model state variables, it takes the form ``X.Y`` where ``X`` is the
                                         ionic model and ``Y`` is the state_variable, e.g., ``tenTusscherPanfilov.Gto``
dump     output adjusted values on grid  0|1
======== ==============================  ======================================================================


"""
import os
EXAMPLE_DESCRIPTIVE_NAME = 'Smooth gradient heterogeneities (ionic adjustment)'
EXAMPLE_AUTHOR = 'Patrick Boyle <pmjboyle@gmail.com>'
EXAMPLE_DIR = os.path.dirname(__file__)
GUIinclude = False

import sys
import numpy as np
from datetime import date
from carputils.carpio import txt
from carputils import mesh
from carputils import settings
from carputils import tools

isPy2 = True
if sys.version_info.major > 2:
    isPy2 = False
    xrange = range


def parser():
    parser = tools.standard_parser()
    group  = parser.add_argument_group('experiment specific options')

    group.add_argument('--xgrad_var',
                       type = str, default = 'GKr',
                       choices = ['GCaL', 'GKs', 'GKr', 'Gto'],
                       help = 'parameter that should be varied along the left-to-right (X) gradient')
    group.add_argument('--ygrad_var',
                       type = str, default = 'Gto',
                       choices = ['GCaL', 'GKs', 'GKr', 'Gto'],
                       help = 'parameter that should be varied along the bottom-to-top (Y) gradient')
    group.add_argument('--xgrad_left_scf',
                       type = float, default = 0.0,
                       help='scaling factor at left side of the X gradient')
    group.add_argument('--xgrad_right_scf',
                       type = float, default=5.0,
                       help = 'scaling factor at right side of the Y gradient')
    group.add_argument('--ygrad_bottom_scf',
                       type = float, default = 0.0,
                       help = 'scaling factor at bottom of Y gradient')
    group.add_argument('--ygrad_top_scf',
                       type = float, default = 5.0,
                       help = 'scaling factor at top of Y gradient')
    group.add_argument('--xgrad_flip',
                       action = 'store_true',
                       help = 'left becomes right, right becomes left')
    group.add_argument('--ygrad_flip',
                       action = 'store_true',
                       help = 'down becomes up, up becomes down')
    return parser

def jobID(args):
    """
    Generate name of top level output directory.
    """
    today = date.today()
    # return '{}'.format(today.isoformat())
    return '{}_Xgrad-{}_{}-to-{}_Ygrad-{}_{}-to-{}'.format(today.isoformat(),
                                                         args.xgrad_var, args.xgrad_left_scf, args.xgrad_right_scf,
                                                         args.ygrad_var, args.ygrad_bottom_scf, args.ygrad_top_scf)

def setupMesh(args):
    #
    UM2MM      = 0.001
    slabsize   = 10000 * UM2MM  # in mm
    resolution =   100 * UM2MM  # in mm
    centre     = np.asarray([slabsize/2., slabsize/2., 0.]) # in mm

    # generate mesh (units in mm) and return basename
    # lower left of the mesh will be the origin
    geom = mesh.Block(size = (slabsize, slabsize, 0.),
                      centre = centre,
                      etype = 'tri',
                      resolution = resolution)

    # incorporate tag regions (divide block into four regions along y-axis)
    # ^ y
    # |
    # o -> x
    #
    # ------------ (y_r3)
    # |  tag 3   |
    # ------------ (y_r2)
    # |  tag 2   |
    # ------------ (y_r1)
    # |  tag 1   |
    # ------------ (y_r0)
    # |  tag 0   |
    # ------------ (0)

    y_r0   = 1.*slabsize/4. # in mm
    y_r1   = 2.*slabsize/4. # in mm
    y_r2   = 3.*slabsize/4. # in mm
    y_r3   = 4.*slabsize/4. # in mm

    # define blocks relative to the mesh centre (mesher wants it this way):
    ll_box_r0 = np.asarray([0.,   0., 0.])-centre; ur_box_r0 = np.asarray([slabsize, y_r0, 0.])-centre # in mm
    ll_box_r1 = np.asarray([0., y_r0, 0.])-centre; ur_box_r1 = np.asarray([slabsize, y_r1, 0.])-centre # in mm
    ll_box_r2 = np.asarray([0., y_r1, 0.])-centre; ur_box_r2 = np.asarray([slabsize, y_r2, 0.])-centre # in mm
    ll_box_r3 = np.asarray([0., y_r2, 0.])-centre; ur_box_r3 = np.asarray([slabsize, y_r3, 0.])-centre # in mm

    box_r0 = mesh.BoxRegion(ll_box_r0, ur_box_r0, 0) # in mm
    box_r1 = mesh.BoxRegion(ll_box_r1, ur_box_r1, 1) # in mm
    box_r2 = mesh.BoxRegion(ll_box_r2, ur_box_r2, 2) # in mm
    box_r3 = mesh.BoxRegion(ll_box_r3, ur_box_r3, 3) # in mm

    geom.add_region(box_r0) # mesher expects boxes relative to the center!!!
    geom.add_region(box_r1)
    geom.add_region(box_r2)
    geom.add_region(box_r3)

    meshname = mesh.generate(geom)

    # Query for element labels
    _, etags, _ = txt.read(meshname + '.elem')
    etags = np.unique(etags)

    return meshname, etags


def make_adjf(var, ptsfile, vbase, vmin, vmax, xmin, xmax, vgrad, dry=False):

    adjfn_base = '{0}_{1:4.2f}-{2:4.2f}_{3}'.format(var, vmin, vmax, 'vgrad' if vgrad else 'hgrad')

    if not dry:
        pts, npts = txt.read(ptsfile)
        adjf      = open(adjfn_base + '.adj', 'w')
        datf      = open(adjfn_base + '.dat', 'w')

        # add adj file header
        adjf.write(str(npts) + '\nintra\n')

        # Read in .pts file and output .adj and .dat files
        for i in range(npts):
            node = pts[i,:]
            x    = node[1] if vgrad else node[0]
            v    = vbase * (vmin + (vmax - vmin) * ((x - xmin) / (xmax - xmin)))
            adjf.write('{} {}\n'.format(i,v))
            datf.write('{}\n'.format(v))

        datf.close()
        adjf.close()
    return adjfn_base

def get_gbase(x):
    return {'GCaL':  3.98e-05,
            'GKr':   0.153,
            'GKs':   0.392,
            'Gto':   0.294}.get(x, 0.0)

@tools.carpexample(parser, jobID, clean_pattern='^(\d{4}-\d{2}-\d{2})|(meshes)|(GKr*)|(Gto*)|(.trc)')
def run(args, job):

    # generate mesh and return basename
    meshname, etags = setupMesh(args)

    # assign element tags to extra- und intracellular space
    IntraTags = etags.tolist().copy()  # 0 is a myocardial tag! By convention this should not be the case!
    ExtraTags = etags.tolist().copy()

    xgrad_adjfn = make_adjf(args.xgrad_var, os.path.join(meshname + '.pts'),
                            get_gbase(args.xgrad_var),
                            args.xgrad_left_scf  if not args.xgrad_flip else args.xgrad_right_scf,
                            args.xgrad_right_scf if not args.xgrad_flip else args.xgrad_left_scf,
                            0, 10000, 0, dry=args.dry)

    ygrad_adjfn = make_adjf(args.ygrad_var, os.path.join(meshname + '.pts'),
                            get_gbase(args.ygrad_var),
                            args.ygrad_bottom_scf if not args.ygrad_flip else args.ygrad_top_scf,
                            args.ygrad_top_scf    if not args.ygrad_flip else args.ygrad_bottom_scf,
                            0, 10000, 1, dry=args.dry)

    cmd  = tools.carp_cmd(os.path.join(EXAMPLE_DIR, 'Smooth-Gradient-Heterogeneity-tenTusscherPanfilov.par'))
    cmd += tools.gen_physics_opts(ExtraTags=ExtraTags, IntraTags=IntraTags)
    cmd += ['-simID',                  job.ID,
            '-meshname',               meshname,
            '-stim[0].elec.vtx_file',   os.path.join(EXAMPLE_DIR, 'LeftSideStim'),
            '-num_adjustments',        2,
            '-adjustment[0].variable', 'tenTusscherPanfilov.{}'.format(args.xgrad_var),
            '-adjustment[0].file',     xgrad_adjfn + '.adj',
            '-adjustment[1].variable', 'tenTusscherPanfilov.{}'.format(args.ygrad_var),
            '-adjustment[1].file',     ygrad_adjfn + '.adj']
    job.carp(cmd, '05E Smooth Gradient Heterogeneities')

    # Calculate and output difference in APD between Sim1 and Sim2 for each node in the grid
    if not args.dry:
        depol = txt.read(os.path.join(job.ID, 'init_acts_depol-thresh.dat'))
        repol = txt.read(os.path.join(job.ID, 'init_acts_repol-thresh.dat'))
        apd   = np.subtract(repol, depol)

        apdFile = os.path.join(job.ID, 'APD.dat')
        txt.write(apdFile, apd)

    # Do visualization
    if args.visualize and not settings.platform.BATCH:
    
        # Visualize with GUI
        if args.webGUI:
            folderNameExp = f'/experiments/05E_Smooth_Gradient_Heterogeneities_{job.ID}'
            os.mkdir(folderNameExp)
            cmdMesh = f'meshtool collect -imsh=/tutorials/02_EP_tissue/05E_Smooth_Gradient_Heterogeneities/{meshname} -omsh={folderNameExp}/{job.ID} -nod=/tutorials/02_EP_tissue/05E_Smooth_Gradient_Heterogeneities/{job.ID}/vm.igb -ifmt=carp_txt -ofmt=ens_bin'
            outMesh = os.system(cmdMesh)
            if(outMesh == 0):
                print('Meshtool - conversion successfully')
            else:
                print('Meshtool - conversion failure')
        
        #Visualize with meshalyzer
        else:
            geom = meshname
            data = os.path.join(job.ID, 'vm.igb')
            view = os.path.join(EXAMPLE_DIR, 'vm.mshz')

            job.meshalyzer(geom, data, view)

if __name__ == '__main__':
    run()

