__title__ = 'Smooth gradient'
__description__ = 'This example details how to assign a gradient of single cell properties using the adjustments interface'
__image__ = '/images/02_05E_Smooth_Gradient_Heterogeneities_Fig2_Example_APs.png'
__q2a_tags__ = 'experiments,examples,tissue,gradient'