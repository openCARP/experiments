__title__ = 'Region vs. gradient heterogeneities'
__description__ = 'This tutorial introduces the concepts of region-based and gradient-based heterogeneities for assigning spatially varying properties '
__image__ = '/images/02_05A_Regions_vs_Gradients_Fig4_APDComparison.png'
__q2a_tags__ = 'experiments,examples,tissue,region,gradient'