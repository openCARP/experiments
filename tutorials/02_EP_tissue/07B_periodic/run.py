#!/usr/bin/env python

"""
.. _periodic-bc-tutorial:


Periodic Boundary Conditions
============================

.. sectionauthor:: Edward Vigmond <edward.vigmond@u-bordeaux.fr>

Periodic boundary conditions connect the left edge of a sheet to the right, or the top to the  bottom.
This, for example models a cylinder very simply while allowing visualization of the whole domain. 
The intact atria or ventricles are better approximated by sheets with periodic boundary conditions (b.c.'s) and not the simpler no flux boundaries. Indeed, reentries last longer on periodic domains since they do not annilhilate at boundary edges.

The implementation of periodic b.c.'s is not straightforward since adding the equations equating nodes on the left 
boundary to those on the right boundary
(or top and bottom) 
is mathematically trivial but destroys the symmetry of the system, and, thereby, invalidates many conjugate gradient preconditioners.
A workaround is to define line connections linking the edges of the tissue, and assigning ridiculously large conductivity
values to these links, effectively short circuiting the nodes. The mesher program will generate these links. To run the experiments of this example change directories as follows:

.. code-block:: bash

   cd ${TUTORIALS}/02_EP_tissue/07B_periodic
   



Experimental Setup
------------------

.. _fig-period-bcs:

.. figure:: /images/02_07B_perCnnx.png
   :align: center
   :width: 30%

   A sheet showing periodic boundary conditions in X implemented with linear elements (in pink) joining the left
   and right edges.

.. figure:: /images/02_07B_perbcs.gif
   :align: center
   :width: 30%

   A point stimulus delivered off-center on a piece of tissue with periodic boundary conditions
   in both directions.

Initiating Reentry
------------------

The standard cross-shock method of reentry initiation will fail with periodic b.c.'s.
The initial line stimulus will produce two wavefronts which collide, and the quarter sheet S2 will quickly go everywhere
and annihilate itself. 
There are two simple ways to induce reentry.

1. We define a mesh without the periodic connections and run an intial portion of the silulation on that.
   Just after the S2, we save the state and restart on the mesh with the periodic b.c.'s enforced.
2. We temporarily block propagation across an edge by clamping the transmembrane voltage and 
   then turning off the clamp after the S2.


Experiments
-----------

The below defined experiments demonstrate initiating reentry in a sheet of tissue with periodic boundary conditions.

Run

.. code-block:: bash

    ./run.py --help

to see all exposed experimental parameters

::

  --bc              boundary condition imposed (noflux,Xper,Yper,XYper)
  --tend TEND       Duration of simulation (ms)
  --size SIZE       size of square sheet (cm)
  --S2-start        time to apply quarter sheet S2 stimulus (<0=no S2)
  --block-Vm         transmembrane voltage of block line [-86 mV]
  --block-dur        duration of block [0 ms]
  --cnnxG           conductivity of periodic connections [10000 S/m]
  --monitor [delay] monitor progress Press "r" in the model window to refresh. 
                    Default wait 30s seconds to launch.

**Experiment exp01**
^^^^^^^^^^^^^^^^^^^^

Try to create a wave which propagates forever. Find a suitable time and Vm clamp voltage for turning off the block.

.. code-block:: bash

   ./run.py --tend 300 --size 2 --monitor=10 --np 10 --bc=Yper --block-dur=? --block-Vm ?? 

One solution is  here: :abbr:`block-Vm (-80)` and :abbr:`block-dur (90)` 

**Experiment exp02**
^^^^^^^^^^^^^^^^^^^^

Create a rotor on a block using the cross-shock protocol

.. code-block:: bash

   ./run.py --size 2 --np=4 --monitor=15 --block-Vm ? --block-dur=? --S2-start ? 

A larger sheet may make it easier. 

Under the Hood
==============

The ``mesher`` program takes the argument ``-periodic`` with the following possible values

===== ============= ========== 
Value Periodic BC's Region ID
===== ============= ==========
0     None           -
1     in X           1234
2     in Y           1235
3     in X and Y     1234/1235
===== ============= ========== 

The longitudinal conductivity for ``g_il`` in the appropriate conductivity regions should then be 
set very high so that there is very little voltage drop across the connection ::

    gregion[1].numIDs = 2
    gregion[1].ID     = 1234 1235
    gregion[1].g_il   = 1.0e4

For the temporary block, use a stimulus with ``stimtype=9``.

"""
import os
EXAMPLE_DESCRIPTIVE_NAME = 'Periodic Boundary Conditions'
EXAMPLE_AUTHOR = 'Edward Vigmond <edward.vigmond@u-bordeaux.fr>'
EXAMPLE_DIR = os.path.dirname(__file__)
GUIinclude = False

from datetime import date

from carputils import settings
from carputils import tools
from carputils import mesh
from carputils import model

def parser():
    parser = tools.standard_parser()
    group = parser.add_argument_group("experiment specific options")
    group.add_argument('--tend',
                        type = float, default = 1000.,
                        help = 'Duration of simulation (%(default)s ms)')
    group.add_argument('--bc',
                        default = "noflux",
                        choices=['noflux','Xper', 'Yper', 'XYper'],
                        help = 'boundary conditions (%(default)s)' )
    group.add_argument('--size',
                        type = float, default = 4.,
                        help = 'tissue dimensions (%(default)s cm square)')
    group.add_argument('--S2-start',
                        type = float, default = -1.,
                        help = 'time at which to apply cross shock (%(default)s ms)')
    group.add_argument('--block-Vm',
                        type = float, default = -86.,
                        help = 'Vm clamp of temporary block (%(default)s mV)')
    group.add_argument('--block-dur',
                        type = float, default = 0.,
                        help = 'duration of temporary edge blocks (%(default)s ms)')
    group.add_argument('--cnnxG',
                        type = float, default = 100000.,
                        help = 'conductivity of periodic connection (%(default)s S/m)')
    group.add_argument('--monitor',
                        type = float,
                        nargs = '?',
                        const = 30.,
                        default = None,
                        help = 'monitor progress visually. Press "r" in the model window to refresh. '
                               'Default wait 30s seconds to launch.')
    return parser

def jobID(args):
    """
    Generate name of top level output directory.
    """
    today = date.today()
    return '{}_periodic_{}_np_{}_{}_{}'.format(today.isoformat(), args.tend,
                                             args.np, args.bc, args.size)

@tools.carpexample(parser, jobID)
def run(args, job):

    # Generate mesh (units in mm)
    periodic  = 1 if 'X' in args.bc else 0
    periodic += 2 if 'Y' in args.bc else 0
    geom = mesh.Block(size=(args.size*10,args.size*10,0), periodic=periodic)

    # Generate and return base name
    meshname = mesh.generate(geom)

    IDs = []
    if 'X' in args.bc: IDs.append(1234)
    if 'Y' in args.bc: IDs.append(1235) # Ln elements
    if IDs:
        gPer = [model.ConductivityRegion([1]), model.ConductivityRegion(IDs, g_il=args.cnnxG, g_el=args.cnnxG)]

    # define S1 stimulus
    edge = args.size*10000./2.
    S1 = [model.Stimulus('S1', x0=-edge-1, y0=-edge+198, xd=edge*2+2, yd=400, strength=80, duration=2, start=0)]
    if args.S2_start >= 0:
        S1.append( model.Stimulus('S2', x0=-edge, y0=-edge, xd=edge, yd=edge, strength=80, duration=2, start=args.S2_start))

    if 'X' in args.bc:
        S1.append(model.Stimulus('Xblock', x0=-edge-1, y0=-edge-1, xd=2, yd=edge*2+2, strength=args.block_Vm, duration=args.block_dur, stimtype=9,
            tau_edge=0, tau_plateau=1.0e6))

    if 'Y' in args.bc:
        S1.append(model.Stimulus('Yblock', x0=-edge-1, y0=-edge-1, xd=edge*2+2, yd=2, strength=args.block_Vm, duration=args.block_dur, stimtype=9,
            tau_edge=0, tau_plateau=1.0e6))


    # Get basic command line, including solver options
    cmd = tools.carp_cmd() + ['-simID', job.ID]

    IntraTags = [1]
    IntraTags.extend(IDs) # element tag 1 is used by default, BCs add up more labels
    ExtraTags = IntraTags.copy()
    cmd += tools.gen_physics_opts(ExtraTags=ExtraTags, IntraTags=IntraTags)

    # adds timulus options
    cmd += model.optionlist(S1)
    
    # maybe add conductivity options for periodic connections
    if len(IDs):
        cmd += model.optionlist(gPer)

    cmd += ['-meshname',               meshname,
            '-tend',                   args.tend,
            '-dt',                     25,
            '-spacedt',                 1,
            '-imp_region[0].im',       'DrouhardRoberge',
            '-imp_region[0].im_param', 'APDshorten*8']

    # Visualization
    if args.visualize or args.monitor :
        # Prepare file paths
        geom = meshname
        data = os.path.join(job.ID, 'vm.igb')
        view = os.path.join(EXAMPLE_DIR, 'view.mshz')

    if not args.webGUI:
        meshproc = job.meshalyzer(geom, data, view, monitor=args.monitor ) if args.monitor else None

    # Run simulation
    job.carp(cmd, 'Periodic Boundary Conditions')

    if args.visualize and not settings.platform.BATCH:
        # Visualize with GUI
        if args.webGUI:
            folderNameExp = f'/experiments/07B_periodic_{job.ID}'
            os.mkdir(folderNameExp)
            cmdMesh = f'meshtool collect -imsh=/tutorials/02_EP_tissue/07B_periodic/{geom} -omsh={folderNameExp}/{job.ID} -nod=/tutorials/02_EP_tissue/07B_periodic/{job.ID}/vm.igb -ifmt=carp_txt -ofmt=ens_bin'
            outMesh = os.system(cmdMesh)
            if(outMesh == 0):
                print('Meshtool - conversion successfully')
            else:
                print('Meshtool - conversion failure')
        
        #Visualize with meshalyzer
        else:
            job.meshalyzer(geom, data, view)

    if not args.webGUI:
        if meshproc: meshproc.wait()

# Define some tests
# -----------------------------------------------------------------------------
# none
# -----------------------------------------------------------------------------

if __name__ == '__main__':
    run()
