__title__ = 'Periodic boundary conditions'
__description__ = 'Periodic boundary conditions connect the left edge of a sheet to the right, or the top to the bottom'
__image__ = '/images/02_07B_perCnnx.png'
__q2a_tags__ = 'experiments,examples,tissue'