__title__ = 'Local activation time'
__description__ = 'This example demonstrates how to compute local activation times (LATs) and action potential durations (APDs) of cells in a cardiac tissue'
__image__ = '/images/02_08_lats_results_LATs_vm.png'
__q2a_tags__ = 'experiments,examples,tissue,lat'