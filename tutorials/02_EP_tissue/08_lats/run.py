#!/usr/bin/env python

"""

This example demonstrates how to compute local activation times (LATs) and action potential durations
(APDs) of cells in a cardiac tissue. To run the experiments of this example change directories as follows:

.. code-block:: bash

   cd ${TUTORIALS}/02_EP_tissue/08_lats
   


.. _tutorial-electrical-mapping:

Electrical Mapping
===================

The wavelength of the cardiac impulse, given by the product of conduction velocity (CV) and refractory
period, is of utmost importance for the study of arrhythmia mechanisms. The assessment of the CV as
well as the refractory period of the cardiac action potential (AP) relies on the determination of
activation and repolarization times, respectively. Experimentally, these are usually calculated based
on 1) transmembrane voltages :math:`V_{\mathrm m}` measured by glass micro-electrodes or in optical
mapping; or 2) extracellular potentials :math:`\phi_{\mathrm e}` measured at the surface of the tissue.

The estimation of activation/repolarization times is based on an event detector that records the
instants when the selected event type occurs. Local activation time (LAT), for instance, is usually
taken as the time of maximum :math:`\mathrm{d}V_{\mathrm m}/\mathrm{d}t` or the minimum :math:`\mathrm{d}\phi_{\mathrm e}/\mathrm{d}t`:

.. figure:: /images/02_08_lats_detec_LATs_derivatives.png
    :scale: 60%
    :align: center

    Action potential in an uncoupled cell. Upper panel: transmembrane potential :math:`V_{\mathrm m}`
    and its time derivative :math:`\mathrm{d}V_{\mathrm m}/\mathrm{d}t`. Lower panel: extracellular potential :math:`\phi_{\mathrm e}`
    and its time derivative :math:`\mathrm{d}\phi_{\mathrm e}/\mathrm{d}t`. LAT is usually taken as the time of maximum
    :math:`\mathrm{d}V_{\mathrm m}/\mathrm{d}t` or the minimum :math:`\mathrm{d}\phi_{\mathrm e}/\mathrm{d}t`.

Instead of using derivatives, LATs can also been estimated by the crossing of a threshold value provided
that the chosen value reflects the sodium channel activation since they are responsible for the rising
phase of AP. One could, for instance, pick the time instant when :math:`V_{\mathrm m}` crosses -10 mV
with a positive slope. Equally, one could look for crossing of :math:`V_{\mathrm m}` with a negative
slope to detect repolarization events. AP duration (APD) can then be computed as the difference between
repolarization and activation times. The detection of activation (:math:`t_{\mathrm{act}}`) and repolarization
(:math:`t_{\mathrm{rep}}`) times based on threshold crossing is illustrated below:

.. figure:: /images/02_08_lats_detect_LATs_threshold.png
    :scale: 60%
    :align: center

    Activation (LAT) and repolarization times :math:`t_{\mathrm{act}}` and :math:`t_{\mathrm{rep}}`
    computed using the threshold crossing method. :math:`APD = t_{\mathrm{rep}} - t_{\mathrm{act}}`.

Problem Setup
=============

This example will run a simulation using a 2D sheet model (1 cm x 1 cm). A transmembrane stimulus
current of 100 uA/cm^2 is applied for a period of 1 ms at the middle of the left side of the tissue.
A ground electrode is placed at the opposite side of the stimulating electrode. Simulation of electrical
activity is based on the bidomain equations. Dynamics of the cardiac myocytes within the tissue are
described using the Modified Beeler-Router Drouhard-Roberge (DrouhardRoberge) model. Activation times are obtained
either from :math:`V_{\mathrm m}` or :math:`\phi_{\mathrm e}` using a threshold crossing method, whereas
repolarization can only be obtained by threshold crossing from :math:`V_{\mathrm m}`. Estimation of
repolarization times from :math:`\phi_{\mathrm e}` is out of the scope of this example.

Usage
=====

To run this example, execute:

.. code-block:: bash

   cd ${TUTORIALS}/02_EP_tissue/08_lats
    
    ./run.py --help 

The following optional arguments are available (default values are indicated):

.. code-block:: bash

    --quantity          Quantity/variable/signal used to compute activation time
                        Options: {vm,phie}
                        Default: vm
    --act-threshold     The threshold value for determining activation from vm or phie
                        Default: -10 mV
    --rep-threshold     The threshold value for determining repolarization from vm
                        Default: -70 mV
    --show-APDs         Visualize APDs instead of LATs

If the program is run with the ``--visualize`` option, meshalyzer will automatically load the computed
activation sequence (LATs). If the ``--show_APDs`` is given, then the computed APDs will be loaded:

.. code-block:: bash

    Visualize LATs:
    ./run.py --visualize
    
    Visualize APDs:
    ./run.py --show-APDs

Interpreting Results
====================

Differences between activation sequences computed from :math:`V_{\mathrm m}` or :math:`\phi_{\mathrm e}`
and/or with different thresholds can be appreciated with meshalyzer (``--visualize``). The activation
sequence for the default parameters looks like this: 

.. figure:: /images/02_08_lats_results_LATs_vm.png
    :scale: 60%
    :align: center

    Activation sequence computed from :math:`V_{\mathrm m}` (``--act-threshold=-10``) obtained after
    delivering a stimulus current in the middle of the left side tissue.

.. note::

    * It is important to note that while computing activation from :math:`V_{\mathrm m}` -lats[].mode
      has to be set to zero. This is because :math:`V_{\mathrm m}` is about -85 mV in cardiac cells
      at rest and rapidly rises to about +40 mV during the activation phase. This means :math:`V_{\mathrm m}`
      will cross the threshold with a positive slope. While computing repolarization, on the other
      hand, :math:`V_{\mathrm m}` goes from positive values back to the rest. The threshold will be
      then crossed with a negative slope.
    
.. figure:: /images/02_08_lats_results_LATs_phie.png
    :scale: 60%
    :align: center

    Activation sequence computed from :math:`\phi_{\mathrm e}` (``--act-threshold=-10``) obtained
    after delivering a stimulus current in the middle of the left side tissue.

.. note::

    * Unlike the shape of :math:`V_{\mathrm m}` which remains pretty much the same in all cells,
      :math:`\phi_{\mathrm e}` morphology may vary depending on its position in relation to both
      stimulation and ground electrodes. Thus, LATs obtained based on :math:`\phi_{\mathrm e}`
      might differ from those obtained from :math:`V_{\mathrm m}`.
      
    * If the criteria for threshold crossing detection is never satisfied, the resulting entry in
      the output file will be -1. See action sequence obtained from :math:`\phi_{\mathrm e}`.

The computed APDs using the default parameters looks like this (use ``--show-APDs`` instead of ``--visualize``):

.. figure:: /images/02_08_lats_results_APDs.png
    :scale: 60%
    :align: center

    Action potential durations computed using the default thresholds (``--act-threshold=-10`` and
    ``--rep-threshold=-70``) after delivering a stimulus current in the middle of the left side of
    the tissue.

openCARP Parameters
====================

The relevant parameters used in the tissue-scale simulation are shown below:

.. code-block:: bash

    # Number of events to detect
    -num_LATs  = 2
    
    # Event 1: activation
    -lats[0].ID         = 'ACTs'
    -lats[0].all        = 0
    -lats[0].measurand  = 0
    -lats[0].threshold  = -10
    -lats[0].mode       = 0
    
    # Event 2: repolarization
    -lats[1].ID         = 'REPs'
    -lats[1].all        = 0
    -lats[1].measurand  = 0
    -lats[1].threshold  = -70
    -lats[1].mode       = 1

num_LATs referes to the number of events we want to detect. In this example there are two events:
activation and repolarization. -lats[0] are options to compute activation while -lats[1] are options
to compute repolarization. -lats[].ID is the name of output file; -lats[].all tells openCARP if it should
detect only the first event (-lats[].all = 0), i.e., first beat, of all of them (-lats[].all = 1);
-lats[].measurand refers to the quantity: :math:`V_{\mathrm m}` (0) or :math:`\phi_{\mathrm e}` (1);
-lats[].threshold is the threshold value for determining the event; finally, -lats[].mode tells openCARP
to look for threshold crossing with a either a positive or negative slope.

"""
import os
EXAMPLE_DESCRIPTIVE_NAME = 'Electrical Mapping'
EXAMPLE_AUTHOR = 'Fernando Campos <fernando.campos@kcl.ac.uk>'
EXAMPLE_DIR = os.path.dirname(__file__)
GUIinclude = True

import sys
from datetime import date
from carputils.carpio import txt
from carputils import settings
from carputils import tools
from carputils import mesh

isPy2 = True
if sys.version_info.major > 2:
    isPy2 = False
    xrange = range


def parser():
    parser = tools.standard_parser()
    group  = parser.add_argument_group('experiment specific options')

    group.add_argument('--quantity',
                        type = str, default='vm',
                        choices = ['vm','phie'],
                        help = 'Quantity/variable to be used to compute local activation timess (the default is transmembrane potential vm)')
    group.add_argument('--act-threshold',
                        type = float, default = -10,
                        help = 'Crossing threshold to be used to compute local activation times')
    group.add_argument('--rep-threshold',
                        type = float, default = -70,
                        help = 'Crossing threshold to be used to compute local repolarization times')
    group.add_argument('--show-APDs',
                        action = 'store_true',
                        help = 'Visualize action potential durations (APDs) instead of local activation times (LATs)')
    return parser

def jobID(args):
    """
    Generate name of top level output directory.
    """
    today = date.today()
    return '{}_lats_{}_act-thr_{}mV_rep-thr_{}mV'.format(today.isoformat(), args.quantity, args.act_threshold, args.rep_threshold)


@tools.carpexample(parser, jobID)
def run(args, job):
    """
    Run example
    """
    
    # Generate mesh
    # Size of the wedge (mm)
    wedge_x = 10
    wedge_y = 10
    wedge_z = 0
    
    # Spatial (mm) and time (us) discretization
    dx = 0.2
    dt = 100
    geom = mesh.Block(size=(wedge_x,wedge_y,wedge_z), resolution=dx, etype='hexa')
    
    # Put the corner of the tissue block at the origin
    geom.corner_at_origin()
    
    # Add regions ------> (0,0,0) is the center of the mesh, not the left lower corner!
    tag  = 100
    prox = mesh.BoxRegion((-wedge_x/2.0,-wedge_y/2.0,0), (wedge_x/2.0,wedge_y/2.0,0), tag)
    geom.add_region(prox)
    
    # Generate and return base name
    meshname = mesh.generate(geom)

    # Model of the cellular action potential
    imp_reg = ['-num_imp_regions',       1,
               '-imp_region[0].name',    "MYOCARDIUM",
               '-imp_region[0].num_IDs', 1,
               '-imp_region[0].ID[0]',   tag,
               '-imp_region[0].im',      "DrouhardRoberge"]
    
    # Tissue setup (isotropic)
    g_reg = ['-num_gregions',       1,
             '-gregion[0].num_IDs', 1,
             '-gregion[0].ID[0]',   tag,
             '-gregion[0].g_il',    0.174,
             '-gregion[0].g_it',    0.174,
             '-gregion[0].g_el',    0.625,
             '-gregion[0].g_et',    0.625]

    # Pacing protocol
    stim = ['-num_stim',                2,
            '-stim[0].crct.type',       0,
            '-stim[0].pulse.strength',  100.0,
            '-stim[0].ptcl.duration',   1.0,
            '-stim[0].ptcl.npls',       1,
            '-stim[0].elec.p0[0]',      0,
            '-stim[0].elec.p1[0]',      500,
            '-stim[0].elec.p0[1]',      (wedge_y/2*1000)-250,
            '-stim[0].elec.p1[1]',      (wedge_y/2*1000)+250,
            '-stim[1].crct.type',       3,
            '-stim[1].elec.p0[0]',      (wedge_x*1000)-100,
            '-stim[1].elec.p1[0]',      (wedge_x*1000)+400,
            '-stim[1].elec.p0[1]',      (wedge_y/2*1000)-100,
            '-stim[1].elec.p1[1]',      (wedge_y/2*1000)]
    
    # Activation / repolarization times
    if args.quantity == 'phie':
        measurand = 1
        mode = 1
    else:
        measurand = 0
        mode = 0
        
    lat = ['-num_LATs',           2,
           '-lats[0].ID',         'ACTs',
           '-lats[0].all',        0,
           '-lats[0].measurand',  measurand,
           '-lats[0].mode',       mode,
           '-lats[0].threshold', args.act_threshold,
           '-lats[1].ID',        'REPs',
           '-lats[1].all',        0,
           '-lats[1].measurand',  0,
           '-lats[1].mode',       1,
           '-lats[1].threshold', args.rep_threshold]
    
    # Timning
    timing = ['-dt',      dt,
              '-tend',    300,
              '-timedt',   10,
              '-spacedt',   1]


    # Get basic command line, including solver options
    cmd = tools.carp_cmd()
    
    cmd += imp_reg
    cmd += g_reg
    cmd += stim
    cmd += lat
    cmd += timing
    cmd += tools.gen_physics_opts(ExtraTags=[tag], IntraTags=[tag])

    simID = os.path.join(job.ID)
    cmd += ['-meshname', meshname,
            '-bidomain', 1,
            '-simID',    simID]

    if args.visualize or args.show_APDs:
        cmd += ['-gridout_i', 2]

    # Run simulation
    job.carp(cmd, 'Local Activation Time')

    # Compute APDs
    if not args.dry:
        LATs = txt.read(os.path.join(simID, 'init_acts_ACTs-thresh.dat'))
        REPs = txt.read(os.path.join(simID, 'init_acts_REPs-thresh.dat'))
        APDs = REPs - LATs
        txt.write(os.path.join(simID, 'APDs.dat'), APDs)

    # Do visualization
    if args.visualize and not settings.platform.BATCH and not args.show_APDs:

        # Prepare file paths
        geom = os.path.join(simID, os.path.basename(meshname)+'_i')
        data = os.path.join(simID, 'init_acts_ACTs-thresh.dat')
        view = os.path.join(EXAMPLE_DIR, 'LATs.mshz')

        # GUI
        if args.webGUI:
            folderNameExp = f'/experiments/08_lats_{job.ID}'
            os.mkdir(folderNameExp)
            cmdMesh = f'meshtool collect -imsh={geom} -omsh={folderNameExp}/{job.ID} -nod={data} -ifmt=carp_txt -ofmt=ens_bin'
            outMesh = os.system(cmdMesh)
            if(outMesh == 0):
                print('Meshtool - conversion successfully')
            else:
                print('Meshtool - conversion failure')
        
        # Call meshalyzer to show Vm
        else:
            job.meshalyzer(geom, data, view)
        
    if args.show_APDs and not settings.platform.BATCH:
        # Prepare file paths
        geom = os.path.join(simID, os.path.basename(meshname)+'_i')
        data = os.path.join(simID, 'APDs.dat')

        # GUI
        if args.webGUI:
            folderNameExp = f'/experiments/08_lats_{job.ID}'
            os.mkdir(folderNameExp)
            cmdMesh = f'meshtool collect -imsh={geom} -omsh={folderNameExp}/{job.ID} -nod={data} -ifmt=carp_txt -ofmt=ens_bin'
            outMesh = os.system(cmdMesh)
            if(outMesh == 0):
                print('Meshtool - conversion successfully')
            else:
                print('Meshtool - conversion failure')
                        
        # Call meshalyzer to show Vm
        else:
            view = os.path.join(EXAMPLE_DIR, 'APDs.mshz')
            job.meshalyzer(geom, data, view)


if __name__ == '__main__':
    run()
