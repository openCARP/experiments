# Tutorials to teach usage of openCARP modeling environment

## What is this repository for?

This repository contains a number of tutorials for teaching the basics of openCARP using
the carputils framework. 
Tutorials are provided for most (but so far not all, feel free to add!) openCARP features. 
These examples are intended to transfer basic user knowhow in an efficient way. 
The scripts for building the tutorials are designed as mini experiments, 
which should also serve as a basic building block for building more complex experiments.

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up?

The tutorials rely upon a proper installation of openCARP including all relevant executables,
carputils and, for single cell tutorials, also limpetPyQtGUI. 
Detailed installation instructions can be found [online](https://opencarp.org/download/installation).


## Contribution guidelines

* See our [CONTRIBUTING document](https://opencarp.org/community/contribute)
* Examples should be designed in a way that they can put in read-only folders and 
  that they can be run from any folder.

## Who do I talk to?

* Find the current list of experiments maintainers on the [web page](https://opencarp.org/about/people#maintainers).