__title__ = 'Meshalyzer'
__description__ = 'meshalyzer is a 4D visualization tool for openCARP allowing interactions to investigate data'
__image__ = '/images/06_02_vis_meshalyzer_favicon.png'
__q2a_tags__ = 'meshalyzer'
__GUIinclude__ = False