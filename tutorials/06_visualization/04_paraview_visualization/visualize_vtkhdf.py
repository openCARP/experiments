import paraview.simple as pvs
import argparse

def arg_parser():
    parser = argparse.ArgumentParser(description='Analysis of AP recordings')
    parser.add_argument('--file_path',
                        type=str,
                        default="",
                        help='path to file (with file extension)')
    parser.add_argument('--temporal_data',
                        type=str,
                        default="",
                        help='name of array containing scalar data')
    return parser.parse_args()


def main(args):
    # load the VTK file
    vtk_file = args.file_path
    data = pvs.OpenDataFile(vtk_file)

    # display the data
    display = pvs.Show(data)

    # set up the render view
    view = pvs.GetActiveViewOrCreate('RenderView')
    view.ResetCamera()

    # list available point data arrays
    print("Point data arrays:", data.PointData.keys())

    # define the point data array to visualize
    color_array = args.temporal_data
    display.ColorArrayName = ['POINTS', color_array]
    pvs.ColorBy(display, ('POINTS', color_array))

    # apply a color map
    color_map = pvs.GetColorTransferFunction(color_array)
    color_bar = pvs.GetScalarBar(color_map, view)
    color_bar.Title = color_array
    color_bar.ComponentTitle = ''
    display.SetScalarBarVisibility(view, False)

    time_steps = data.TimestepValues

    # animate through time steps
    for i, t in enumerate(time_steps):
        # set the current time step
        animation_scene = pvs.GetAnimationScene()
        animation_scene.TimeKeeper.Time = t

        # print the current time step for debugging
        print(f"Rendering time step: {t}")

        # render the frame
        pvs.Render()

    # Interact with the visualization (optional)
    pvs.Interact()


if __name__ == '__main__':
    args = arg_parser()
    main(args)