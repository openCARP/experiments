__title__ = 'Paraview visualization'
__description__ = 'This is an exemple to visualize the time series of a simple tissue geometry in ParaView.'
__image__ = '/images/06_04_paraview_visualisation.png'
__q2a_tags__ = 'vtk,tissue'