#!/usr/bin/env python3
#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the 
# Apache License, Version 2.0 (the "License"); 
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
"""
Overview
========
.. code-block:: bash

    cd ${TUTORIALS}/06_visualization/04_paraview_visualization

(1) Example of the visualisation of the time series development of the transmembrane voltage on a simple tissue geometry with the help of ParaView. 
(2) Example of the visualisation of the time series development of the transmembrane voltage on a advanced tissue geometry using a differnt file format.
(3) Example of using Paraview's trace function to automate your workflow.

Example 1: Simple Tissue Geometry
=================================

The mesh geometry as well as the additionally required files were obtained from ``${TUTORIALS}/02_EP_tissue/02_stimulation`` and stored in the folder called ``meshes``. In order to visualise the excitation on the mesh in ParaView, the transmembrane voltage, which is stored in the ``.igb`` file and the mesh file were merged with meshtool collect. The following command can be used to display all the options offered by meshtool collect.

.. code-block:: bash

    meshtool collect

As previously mentioned, the first step is to merge the time series of the transmembrane voltage (``.igb`` file) and the mesh geometry (``block_i``) into one file using meshtool collect. In this case, the merged file is simply called ``result.case`` and the default file format (``ens_bin``) is used.

.. code-block:: bash

    meshtool collect -imsh=/meshes/block_i -omsh=/meshes/result -nod=/meshes/vm.igb

The resulting file format can be changed by specifying the ``-ofmt`` option. However, for large meshes, keeping the merged file as small as possible is advisable to ensure high visualisation quality at all times. After the files have been merged with meshtool collect, the results can be assessed in ParaView. Therefore, select the ``.case`` file and load it into ParaView. The time course of the transmembrane voltage can be observed after pressing the play button.

.. figure:: /images/06_04_paraview_visualisation.png
    :width: 50 %
    :align: center

    This image shows the temporal development of the transmembrane voltage on a simple cuboid.

Note: In case the mesh is very large, the conversion to other file formats should be considered.


Example 2: Advanced Tissue Geometry
===================================

Here, we present a method for handling large meshes and files with extensive temporal data by converting them into the recently designed VTKHDF file format, which ensures seamless compatibility with ParaView (version 5.12.0 or later). VTKHDF supports the storage of both static and dynamic meshes along with associated temporal data in a single file. For the sake of simplicity, we use the same example as above for demonstration.

.. code-block:: bash

    python3 igb_to_vtkhdf.py --igb_path /meshes/vm.igb --pts_path /meshes/block.pts --elem_path /meshes/block.elem --output_path exp23.vtkhdf --cell_type 10 --max_memory 2 --stride 2

This example works for static meshes only. For dynamic meshes, please refer to https://www.kitware.com/how-to-write-time-dependent-data-in-vtkhdf-files/ or contact one of the example authors.

For visualization of temporal data with more than 10,000 time steps, ParaView's GUI slows down the rendering time drastically causing stuttering animations. In this case, we recommend to either increase the stride (reduce total amount of time steps) or use PvPython, which is not affected by the performance limitations of the GUI and is capable of rendering meshes with 600,000 elements and 100,000 time steps (64 GB) still at 25 fps.

Here is an example script for the visualization using PvPython:

.. code-block:: bash

    /Applications/ParaView-5.13.1.app/Contents/bin/pvpython visualize_vtkhdf.py --file_path exp23.vtkhdf --temporal_data Vm

This python file collects geometrical (pts, elem) and temporal (igb) data into a single file in VTKHDF format. Temporal data can be read and written sequentially using any desired chunk / RAM size. This allows for conversion of very large files and smooth visualizations using PvPython. Temporal data can also be downsampled during conversion.


Example 3: Trace Function of Paraview
=====================================

Paraview offers various functions that help you to automate your workflow or make it reproducible. One of these is the trace function. Using the trace function, you can record certain steps or patterns you take in the graphical user interface, such as a rotation around the azimuth axis, for which Paraview automatically generates python code. The generated code can then be further customised or imported directly and applied to other meshes. The following code example was generated automatically after activating the trace function (``Tools --> Start Trace``). As it is not easy to rotate a mesh by exactly -60° using only the mouse, the rotation was carried out and then the value was adjusted to -60°, subsequently.

.. code-block:: bash

    # trace generated using paraview version 5.11.1

    #### import the simple module from the paraview
    from paraview.simple import *
    #### disable automatic camera reset on 'Show'
    paraview.simple._DisableFirstRenderCameraReset()

    # get active view
    renderView1 = GetActiveViewOrCreate('RenderView')

    renderView1.ResetActiveCameraToNegativeZ()

    # reset view to fit data
    renderView1.ResetCamera(False)

    renderView1.AdjustAzimuth(-60.0)

    #================================================================
    # addendum: following script captures some of the application
    # state to faithfully reproduce the visualization during playback
    #================================================================

    # get layout
    layout1 = GetLayout()

    #--------------------------------
    # saving layout sizes for layouts

    # layout/tab size in pixels
    layout1.SetSize(2228, 1506)

With the help of the trace function you can intuitively figure out keywords, function names and arguments that you can use as the basis for your customised python script.
"""

import os

EXAMPLE_DESCRIPTIVE_NAME = 'Paraview visualization'
EXAMPLE_AUTHOR = 'Clara Sales Bellés <csales@unizar.es>, Hermenegild Arevalo <hermenegild@simula.no>, Christian Götz <christian.goetz@partner.kit.edu>, Pascal Maierhofer <pascal.maierhofer@kit.edu>, Moritz Linder <moritz.linder@kit.edu>'
EXAMPLE_DIR = os.path.dirname(__file__)
