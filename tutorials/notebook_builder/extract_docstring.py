#!/usr/bin/env python
import ast
import sys

code = ast.parse(sys.stdin.read())
docstring = ast.get_docstring(code)
print(docstring)
