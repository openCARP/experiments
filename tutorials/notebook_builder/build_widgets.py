import sys
from widget_builder import *

def get_ionic_args(parse):
    usual_imps = ['EP', 'IMP', 'ep', 'imp', 'model']
    my_ionic = 'ionic_argument = None\n'
    for imp in usual_imps:
        if get_action(parse._actions, imp):
            my_ionic = 'ionic_argument = "%s"\n' % imp

    usual_params = ['EP-par', 'params', 'im_param']
    my_param = 'ionic_param = None\n'
    for param in usual_params:
        if get_action(parse._actions, param):
            my_param = 'ionic_param = "%s"\n' % param

    return my_ionic + my_param

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print(f"usage: {sys.argv[0]} <input_script>")
        exit(1)

    tutorial = sys.argv[1][:-3].replace('/','.')
    parse = module_parser(tutorial)
    my_widgets = build_generated_widgets(tutorial, parse._actions)

    # also assign for the ionic_argument and
    my_widgets += get_ionic_args(parse)

    print(my_widgets)
