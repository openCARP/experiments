from collections import OrderedDict
import importlib
import sys

def module_parser(example_tutorial):
    sys.path.append("..")
    modrun = importlib.import_module(
        # '01_EP_single_cell.01_basic_bench.run'
        example_tutorial.replace('/','.').replace('...','')
       )
    return modrun.parser()

def parse_option(opt_strings):
    index = 1
    if opt_strings[0][1] == "-":
        index = 2
    return opt_strings[0][index:]

def parse_help(help_string):
    if help_string:
        return help_string.replace('\n','').replace("'","`")
    return ""

def get_action(actions, option):
    """Find argparse actions that has the option_string corresponding to option"""
    for action in actions:
        if parse_option(action.option_strings) == option:
            return action
    # print(f"Warning: option {option} not found")

def reindent(s, numSpaces):
    return "".join([(numSpaces * ' ') + line.replace('\n', '') + "\n" for line in s.split("\n")])

def build_selection(action, choices):
    widget_str = "widgets.Dropdown("
    widget_str += f"options = {choices}"
    if action.default:
        widget_str += f", value = '{action.default}'"
    widget_str += f", description = '{parse_option(action.option_strings)}'"
    widget_str += ", disabled=False,)\n"

    return widget_str

def build_textbox(action, default=''):
    widget_str = "widgets.Text("
    if action.default:
        widget_str += f"value=\"{default}\","
    # widget_str += f"""placeholder='{action.help}',
    widget_str += f"""placeholder='Type something',
        description='{parse_option(action.option_strings)}',
        disabled=False
    )\n"""

    return widget_str

def build_checkbox(action, default=False):
    widget_str = f"""widgets.Checkbox(
        value={default},
        description='{parse_option(action.option_strings)}',
        disabled=False,
        indent=False
    )\n"""

    return widget_str

def build_int(action):
    default_value = int(action.default)
    widget_str = f"""widgets.IntText(
        value={default_value},
        description='{parse_option(action.option_strings)}',
        disabled=False
    )\n"""

    return widget_str

def build_float(action):
    default_value = float(action.default)
    widget_str = f""" widgets.FloatSlider(
        value={default_value},
        min=0,
        max={2*default_value},
        step={default_value/100},
        description='{parse_option(action.option_strings)}',
        disabled=False,
        continuous_update=False,
        orientation='horizontal',
        readout=True,
        readout_format='.1f',
    )\n"""

    return widget_str

def build_widget(action):
    if action.choices:
        if type(action.choices) == type(OrderedDict().keys()):
            choices = [k for k in action.choices]
        else:
            choices = action.choices
        return build_selection(action, choices)
    if action.type == str:
        return build_textbox(action,default=action.default)
    if action.type == bool:
        return build_checkbox(action)
    if action.default:
        if type(action.default) == list:
            return build_textbox(action, default="%s"%action.default)
        if type(action.default) == bool:
            return build_checkbox(action, default=action.default)

        try:
            int(action.default)
            return build_int(action)
        except ValueError:
            return build_textbox(action)
    return build_textbox(action)

def build_example(build_actions):
    my_widgets = "\ngenerated_widgets = dict()\n\n"
    for i in range(len(build_actions)):
        my_widgets += f"widget_{build_actions[i].dest} = "
        my_widgets += build_widget(build_actions[i])
        my_widgets += f"label_{build_actions[i].dest} = widgets.Label('{parse_help(build_actions[i].help)}')\n"
        my_widgets += f"generated_widgets['{parse_option(build_actions[i].option_strings)}'] ="
        my_widgets += f"widgets.HBox([widget_{build_actions[i].dest}, label_{build_actions[i].dest}])\n\n"

    my_widgets += "return generated_widgets"

    return my_widgets

def build_generated_widgets(example_tutorial, build_actions):
    my_widgets  = "# %% [markdown]\n"
    my_widgets += "# # Tutorial " + example_tutorial
    my_widgets += "\n\n# %%"
    my_widgets += "\nimport ipywidgets as widgets\n"
    my_widgets += "\n\ndef generate_widgets():\n"
    my_widgets += reindent(build_example(build_actions), 4)
    my_widgets += "\n\n# %%\n"

    return my_widgets

def build_notebook(example_tutorial,build_actions, imp_option_name):
    my_widgets = "from imppar_widgets import *\n\n"
    my_widgets += build_generated_widgets(example_tutorial, build_actions)
    main_run = f"""
generated_widgets = generate_widgets()
imp_widget = generated_widgets.pop('{imp_option_name}')
if (type(imp_widget.children[0]) == widgets.widget_string.Text):
    imp_dropdown = widgets.Dropdown(options = list_imps(),
                                    #value = imp_widget.children[0].value,
                                    description = imp_widget.children[0].description,
                                    disabled=False,)
    imp_widget.children = ([imp_dropdown, imp_widget.children[1]])

for v in generated_widgets.values():
    display(v)

def f(x):
    return widgets.VBox([v for v in imppar_widgets(x).values()])

im = widgets.interact_manual(f, x=imp_widget.children[0])
im.widget.children[1].description = 'Show parameters'
"""
    my_widgets += 'if __name__ == "__main__":'
    my_widgets += reindent(main_run, 4)

    return my_widgets
