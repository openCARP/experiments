import subprocess
import glob
import ipywidgets as widgets

def imppar_widgets(imp):
    command = ["/usr/local/bin/bench",
               f"--imp={imp}",
               "--imp-info"]

    call = subprocess.run(command, capture_output=True)

    parameters_values = call.stdout.decode("utf-8").split('Parameters:')[1].split('State variables:')[0].split('\n')

    widget_parameters = dict()
    parameters = []
    for line in parameters_values:
        if len(line.split())==2:
            parameter, value = line.split()
            parameters.append(parameter)
            widget_parameters[parameter] = widgets.Text(placeholder="=" + value,
                                                        description=parameter + ":",
                                                        disabled=False)
    return widget_parameters

def list_imps():
    command = ["/usr/local/bin/bench",
               "--list-imps"]

    call = subprocess.run(command, capture_output=True)

    L = list()
    for item in call.stdout.decode("utf-8").split('Ionic models:')[1].split('Plug-ins:')[0].split('\n'):
        if item: L.append(item.replace("\t",""))
    return L

# %%
def get_altered_parameters(parameters):
    altered_parameters = ""
    for k, v in parameters.items():
        if v.value:
            if altered_parameters:
                    altered_parameters += ","
            altered_parameters += k + v.value
    return altered_parameters

def get_command(generated_widgets, altered_parameters, path='', ionic_param ='EP-par'):
    command = "python3 " + path + "run.py "
    for k,v in generated_widgets.items():
        param = v.children[0].value
        if param:
            command += " --{} {}".format(k, param)
    if altered_parameters:
        command += " --{} '{}'".format(ionic_param, altered_parameters)
    return command

def command_widget():
    command = widgets.Textarea(
        value="",
        placeholder='Type something',
        description='$ ',
        disabled=False,
        layout=widgets.Layout(height="200px", width="auto")
    )
    return command

def example_selection():
    example_scripts = glob.glob("*/*/widgets.py")

    scripts_to_build = []
    for f in example_scripts:
        name = f
        scripts_to_build.append(name.replace("widgets.py",""))

    example_tutorial=widgets.Dropdown(
        options=scripts_to_build,
        value=scripts_to_build[0],
        description='Tutorial:',
        disabled=False,
       )
    return example_tutorial

def display_widgets(widg):
    out = widgets.Output(layout={'border': '1px solid black'})
    with out:
        for k,v in widg.items():
            display(v)
    display(out)
