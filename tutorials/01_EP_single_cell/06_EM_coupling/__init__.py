__title__ = 'Electromechanical coupling'
__description__ = 'Couple an electrophysiological cell model to a tension model'
__image__ = '/images/01_06_elmech.png'
__q2a_tags__ = 'examples,experiments,parameters,land'