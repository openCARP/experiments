#!/usr/bin/env python

r"""
.. _single-cell-EM-tutorial:

Modeling EM at the cellular level
=================================

This tutorial introduces the basic steps of setting up electromechanical (EM) simulations in an isolated
myocytes.
For this sake an electrophysiology (EP) model of cellular dynamics must be paired with a myofilament
model for generating active stresses.
This is achieved by selecting an EP model of cellular dynamics and an active
stress model as a plug-in.
Setting up, initializing and testing of such myocyte-myofilament combinations is
performed with our single cell tool :ref:`bench <bench>`.
There are three coupling variants implemented which are suitable depending on a
particular question to be addressed:

This setup aims to replicate single cell stretch experiments as they are performed with setups.

.. _fig-single-cell-stretcher:

.. figure:: /images/01_06_MyocyteStretcherSetup.png
   :width: 50%
   :align: center

   Setup for single cell stretch experiments. The myocyte is activated with a
   supra-threshold stimulus current to initiate action potentials at a pacing
   cycle length of 500 ms. The length of the myocytes and thus the sarcomere
   stretch can be prescribed as a function of time.


Experimental Parameters
-----------------------

The following parameters are exposed to steer the experiment:

.. code-block:: bash

  --experiment {active | passive}
                        Default is active. In the passive case any stimulation is turned off
                        and the myocyte remains quiescent. This can be used in the
                        strongly coupled case to observe the effect of step changes in length
                        upon EP states.


  --EP                  pick human EP model (default is tentusscherpanfilov)


  --stress              pick stress model (default is stress_niederer)

  --stretch STRETCH     prescribe constant stretch over each cycle (default is 1.)

  --init INIT           pick state variable initialization file (default is none)

  --duration DURATION   pick duration of experiment (default is 500 ms)

  --bcl BCL             pick basic cycle length (default is 500 ms)

  --fs FS               provide trace file to prescribe fiber stretch (default is none)

  --fsDot FSDOT         provide trace file to prescribe fiber stretch rate
                        (default is none)

  --ref FOLDER          folder holding .bin files of a reference solution to be plotted in comparison

Each experiment stores the state of myocyte and myofilament in the current directory in a file
to be used as initial state vector in subsequent experiments.


.. _Stress_Niederer-model:

Activation-based active stress model
------------------------------------

Such models are suitable for being used in clinical EM modeling study
as these models - owing to their small number of parameters and their fairly direct relation
to clinically measurable parameters such as peak pressure, :math:`\hat{P}`
or the maximum rate of rise of pressure, :math:`dP/dt_{\mathrm {max}}` -
are easier to fit.

As an example we use the very simple ``niederer`` stress model.
The model is based on activation times and constructed with exponential functions,
it also accounts for length-dependent development of active tension.
Our implementation of the ``niederer`` stress model is based on the PhD work of Andrew Crozier
and has also been used in publications [#niederer_length]_ [#crozier_relative]_.

The shape of the active stress transient is given by

.. math::
   :nowrap:

   \begin{equation}
   T_{\rm a} = T_{\rm peak}
            \phi
            \tanh^2 \left( \frac{t_s}{\tau_{\rm c}} \right)
            \tanh^2 \left( \frac{t_\text{dur} - t_{\rm s}}{\tau_{\rm r}} \right)
            \text{for } 0< t_s < t_\text{dur} \nonumber
   \end{equation}

with

.. math::
   :nowrap:

   \begin{eqnarray}
       \phi         &= \tanh (\text{ld} (\lambda - \lambda_{\rm 0})) \nonumber \\
       \tau_{\rm c} &= \tau_{\rm c0} + \text{ld}_\text{up} (1 - \phi) \nonumber \\
       t_{\rm s}    &= t - t_\text{act} - t_\text{emd} \nonumber
   \end{eqnarray}


with the following parameters:

.. _tab-TanhStress-params:

.. table:: Parameters of the Niederer tanh stress model


   +----------------------------+----------------------------------------------------------------+
   | Variable                   |  Description                                                   |
   +============================+================================================================+
   | :math:`t_{\rm s}`          | onset of contraction                                           |
   +----------------------------+----------------------------------------------------------------+
   | :math:`\phi`               | non-linear length-dependent function in which :math:`\lambda`  |
   |                            | is the fiber stretch                                           |
   +----------------------------+----------------------------------------------------------------+
   | :math:`\lambda_{\rm 0}`    | fiber stretch below which no tension is generated anymore.     |
   +----------------------------+----------------------------------------------------------------+
   | :math:`t_{\mathrm {emd}}`  | electro-mechanical delay                                       |
   +----------------------------+----------------------------------------------------------------+
   | :math:`T_{\mathrm {peak}}` | peak isometric tension                                         |
   +----------------------------+----------------------------------------------------------------+
   | :math:`t_{\mathrm dur}`    | duration of tension generation                                 |
   +----------------------------+----------------------------------------------------------------+
   | :math:`\tau_{\mathrm {c}}` | time constant of contraction                                   |
   +----------------------------+----------------------------------------------------------------+
   | :math:`\tau_{\mathrm {c0}}`| baseline time constant of contraction :math:`ld_{\mathrm {up}}`|
   +----------------------------+----------------------------------------------------------------+
   | :math:`ld_{\mathrm {up}}`  | length-dependence contraction time constant                    |
   +----------------------------+----------------------------------------------------------------+
   | :math:`\tau_{\mathrm {r}}` | time constant of relaxation                                    |
   +----------------------------+----------------------------------------------------------------+
   | :math:`ld`                 | degree of length dependence                                    |
   +----------------------------+----------------------------------------------------------------+
   | :math:`ld_{\mathrm {on}}`  | flag to turn on/off length-dependence of active stress         |
   |                            | generation local activation time                               |
   +----------------------------+----------------------------------------------------------------+
   | :math:`t_{\mathrm {act}}`  | which is automatically derived from the action potential,      |
   |                            | estimated as the instant of crossing the :math:`V_{\rm m}`     |
   |                            | threshold :math:`V_{\mathrm{m,thresh}}`                        |
   +----------------------------+----------------------------------------------------------------+
   | :math:`V_{\rm m,thresh}`   | transmembrane voltage threshold used for detecting             |
   |                            | :math:`t_{\mathrm {act}}`                                      |
   +----------------------------+----------------------------------------------------------------+


The effect of varying the time constants governing contraction :math:`\tau_{\rm c}`
and relaxation, :math:`\tau_{\rm r}`, is shown below. Duration of the active
stress transient was set to 550 ms in this case.

.. figure:: /images/01_06_tanhActiveStress.png
   :scale: 50%
   :align: center


.. _Length dependent tension in the failing heart and the efficacy of cardiac
   resynchronization therapy: http://doi.org/10.1093/cvr/cvq318
.. _The relative role of patient physiology and device optimisation in cardiac
   resynchronisation therapy: http://doi.org/10.1016/j.yjmcc.2015.10.026


.. _exp-em-sc-01:

**Experiment exp01**
^^^^^^^^^^^^^^^^^^^^

We start with coupling the Niederer tanh stress model with the tentusscherpanfilov human myocyte model,

.. code-block:: bash

   ./run.py --EP tentusscherpanfilov --stress stress_niederer --duration 2000 --bcl 500 --ID exp01 --visualize


.. _exp-em-sc-02:

**Experiment exp02**
^^^^^^^^^^^^^^^^^^^^

As the tentusscherpanfilov is not at a limit cycle, we pace the model for 20 seconds

.. code-block:: bash

   ./run.py --EP tentusscherpanfilov --stress stress_niederer --duration 20000 --bcl 500 --ID exp02 --visualize

In exp02 the states of myocyte and myofilament model are stored at the very end of
the experiment at :math:`t=20000 ms` in the file
``exp01_tentusscherpanfilov_stress_niederer_bcl_500_ms_dur_20000_ms.sv``.


.. _exp-em-sc-03:

**Experiment exp03**
^^^^^^^^^^^^^^^^^^^^

Using the state of the myocyte stored in the file
``exp01_tentusscherpanfilov_stress_niederer_bcl_500_ms_dur_20000_ms.sv`` in exp02
we confirm now that your trajectories are close to a limit cycle for the
given ``bcl`` and ``stretch`` we run

.. code-block:: bash

   ./run.py --EP tentusscherpanfilov --stress tanh --duration 1000 --bcl 500 --ID exp03 \
            --stretch 1.0 --init exp02_tentusscherpanfilov_tanh_bcl_500_ms_dur_20000_ms.sv \
            --visualize

Observe that there is no significant difference between the traces for AP1 and AP2.


.. _exp-em-sc-04:

**Experiment exp04**
^^^^^^^^^^^^^^^^^^^^

To ensure that length-dependence of active tension is working as expected
re-run the experiment at a shorter cell length by setting the fiber stretch :math:`\lambda=0.75`
and compare with the traces stored in exp03 in the file *exp03_traces.h5*.

.. code-block:: bash

   ./run.py --EP tentusscherpanfilov --stress tanh --duration 1000 --bcl 500 \
            --ID exp04 --stretch 0.75 --init exp02_tentusscherpanfilov_tanh_bcl_500_ms_dur_20000_ms.sv \
            --ref exp03 --visualize


.. _exp-em-sc-05:

**Experiment exp05**
^^^^^^^^^^^^^^^^^^^^

Re-run experiment exp04, but at a stretched cell length by setting the
fiber stretch :math:`\lambda=1.25` and compare again with the traces stored
in exp03 in the file *exp03_traces.h5*
at the reference cell stretch :math:`\lambda=1.0`.

.. code-block:: bash

   ./run.py --EP tentusscherpanfilov --stress tanh --duration 1000 --bcl 500 \
            --ID exp05 --stretch 1.25 --init exp02_tentusscherpanfilov_tanh_bcl_500_ms_dur_20000_ms.sv \
            --ref exp03 --visualize



.. _LandStress-model:

Weakly coupled Calcium-driven active stress model
-------------------------------------------------

As an example of a weakly coupled Calcium-driven active stress model due to Land et al for
rat and rabbit myocytes [#land_rat]_ and for human myocytes [#land_human]_.
Parameters used in this tutorial were derived from the study of Tondel et al [#tondel_land]_.

.. _tab-LandStress-params:

.. table:: Parameters of the Land stress model

   +-------------------------+---------------------------------------------------------------------+
   | Variable                | Description                                                         |
   +=========================+=====================================================================+
   | :math:`T_{\rm {ref}}`   | Reference tension [kPa]                                             |
   +-------------------------+---------------------------------------------------------------------+
   | :math:`Ca_{\rm {50ref}}`| Calcium sensitivity at resting sarcomere length                     |
   +-------------------------+---------------------------------------------------------------------+
   | :math:`TRPN_{\rm {50}}` | Troponin C sensitivity                                              |
   +-------------------------+---------------------------------------------------------------------+
   | :math:`n_{\rm {TRPN}}`  | Hill coefficient for cooperative binding of Calcium to Troponin C   |
   +-------------------------+---------------------------------------------------------------------+
   | :math:`k_{\rm {TRPN}}`  | Unbinding rate of Calcium from Troponin C                           |
   +-------------------------+---------------------------------------------------------------------+
   | :math:`n_{\rm {xb}}`    | Hill coefficient for cooperative crossbridge action                 |
   +-------------------------+---------------------------------------------------------------------+
   | :math:`k_{\rm {xb}}`    | scaling factor for the rate of crossbridge binding                  |
   |                         | [:math:`\mathrm{ms}^{-1}`]                                          |
   +-------------------------+---------------------------------------------------------------------+
   | :math:`\beta_{\rm {0}}` | magnitude of filament overlap effects                               |
   +-------------------------+---------------------------------------------------------------------+
   | :math:`\beta_{\rm {1}}` | magnitude of length-dependent activation effects                    |
   +-------------------------+---------------------------------------------------------------------+


.. _exp-em-sc-06:

**Experiment exp06**
^^^^^^^^^^^^^^^^^^^^

As before with the Tanh stress model, we pace the model for 20 seconds at a resting
length of :math:`\lambda=1.` to arrive at a limit cycle.

.. code-block:: bash

   ./run.py --EP tentusscherpanfilov --stress land --duration 20000 --bcl 500 --ID exp06 --visualize

Observe that, unlike on the experiments with the Tanh stress model, tension transients
vary over time as tension depends on Calcium.
In exp06 the states of myocyte and myofilament model are stored at the very end of the
experiment at :math:`t=20000~ms` in the file ``exp06_tentusscherpanfilov_land_bcl_500_ms_dur_20000_ms.sv``.


.. _exp-em-sc-07:

**Experiment exp07**
^^^^^^^^^^^^^^^^^^^^

Using the state of the myocyte stored in the
file ``exp06_tentusscherpanfilov_land_bcl_500_ms_dur_20000_ms.sv`` in exp06
we confirm now that your trajectories are close to a limit cycle for the
given ``bcl`` and ``stretch`` we run

.. code-block:: bash

   ./run.py --EP tentusscherpanfilov --stress land --duration 1000 --bcl 500 --ID exp07 \
            --stretch 1.0 --init exp06_tentusscherpanfilov_land_bcl_500_ms_dur_20000_ms.sv \
            --visualize

Observe that there is no significant difference between the traces for AP1 and AP2.


.. _exp-em-sc-08:

**Experiment exp08**
^^^^^^^^^^^^^^^^^^^^

To verify that the Land model in combination with the tentusscherpanfilov human ventricular myocyte models
produces reasonable tensions over the physiological stretch range from :math:`\lambda=0.75`
up to  :math:`\lambda=1.25` we run two cycles at with a shortened myocyte :math:`\lambda=0.75`.


.. code-block:: bash

   ./run.py --EP tentusscherpanfilov --stress land --duration 1000 --bcl 500 \
            --ID exp08 --stretch 0.75 --init exp06_tentusscherpanfilov_land_bcl_500_ms_dur_20000_ms.sv \
            --ref exp07 --visualize


.. _exp-em-sc-09:

**Experiment exp09**
^^^^^^^^^^^^^^^^^^^^

Re-run experiment exp08, but at a stretched cell length by setting the
fiber stretch :math:`\lambda=1.25`
and compare again with the traces stored in exp07 in the file *exp07_traces.h5*
at the reference cell stretch :math:`\lambda=1.0`.

.. code-block:: bash

   ./run.py --EP tentusscherpanfilov --stress land --duration 1000 --bcl 500 \
            --ID exp09 --stretch 1.25 --init exp06_tentusscherpanfilov_land_bcl_500_ms_dur_20000_ms.sv \
            --ref exp07 --visualize


.. _exp-em-sc-10:

**Experiment exp10**
^^^^^^^^^^^^^^^^^^^^

Force development depends on both, length :math:`\lambda` and the velocity of
length change :math:`\dot{\lambda}`.
Increasing velocity :math:`\dot{\lambda}` lowers the force produced by a myocyte
which produces the maximum force under isometric conditions, that is :math:`\dot{\lambda} = 0`.
To illustrate these two effects indpendently
we re-run exp09 with a dynamic sawtooth loading protocol shown in :numref:`fig-sawtooth-loading-ld`,
but we keep the velocity at zero.

.. _fig-sawtooth-loading-ld:

.. figure:: /images/01_06_sawtooth_loading_ld.png

   Mechanical loading protocol applied to stretch the cell. Compared to the reference
   stretch experiment with :math:`\lambda=1.0` a sawtooth stretch function of
   time :math:`\lambda(t)` is applied.
   Velocity dependence is suppressed here by keeping :math:`\dot{\lambda}(t)=0.`.
   Blue traces show the loading protocol applied in exp09.

.. note:: This is artificial as we are altering :math:`\lambda(t)` and
   :math:`\dot{\lambda}(t)` independently.

.. code-block:: bash

   ./run.py --EP tentusscherpanfilov --stress land --duration 1000 --bcl 500 \
            --ID exp10 --init exp06_tentusscherpanfilov_land_bcl_500_ms_dur_20000_ms.sv \
            --fs lambda_sawtooth_200ms.dat \
            --ref exp07 --visualize


.. _exp-em-sc-11:

**Experiment exp11**
^^^^^^^^^^^^^^^^^^^^

In this experiment do not suppress velocity dependence anymore.
The mechanical loading protocol applied is the same as in exp10,
but with :math:`\dot{\lambda}(t) \neq 0`. The loading protocol is illustrated in
:numref:`fig-sawtooth-loading-ld-vd`.

.. _fig-sawtooth-loading-ld-vd:

.. figure:: /images/01_06_sawtooth_loading_ld_vd.png
   :align: center

   Mechanical loading protocol applied to stretch the cell.
   In this experimental protocol velocity-dependence of force generation is considered,
   that is, delLambda(:math:`\dot{\lambda}(t)`) is indeed the derivative with respect to
   time of Lambda (:math:`\lambda(t)`).
   Blue traces show the loading protocol applied in exp10.


.. code-block:: bash

   ./run.py --EP tentusscherpanfilov --stress land --duration 1000 --bcl 500 \
            --ID exp11 --init exp06_tentusscherpanfilov_land_bcl_500_ms_dur_20000_ms.sv \
            --fs lambda_sawtooth_200ms.dat --fsDot dlambda_sawtooth_200ms.dat \
            --ref exp10 --visualize


.. _Augustin-model:

Strongly coupled Calcium-driven active stress model
---------------------------------------------------

In this set of experiments we couple the recent EP model of the human ventricular myocyte
due to Grandi et al [#grandi_human_ventricle]_ (Grandi-Pasqualini-Bers GPB)
with the active tension model due to Land et al [#land_rat]_ (Land-Niederer LN).
Details on the implementation and parameterization of the coupling have been reported in
detail in Augustin et al [#augustin_hrmech]_.

Briefly, to allow for strong coupling, i.e., to account for length effects on the
cytosolic calcium transient, modifications were implemented to both the GPB and the LN model.
The equation governing the binding of calcium to the low affinity regulatory sites on Troponin
in the myofilament model (see Eq.~(114) in the Appendix of [#grandi_human_ventricle]_)

.. math::
   :nowrap:
   :label: eq-Ca-Shannon

   \begin{align}
     \frac{\mathrm{d}[\mathrm{TnC}_{\mathrm{L}}]}{\mathrm{d} t}
     = k_{\mathrm{on}_{\mathrm{TnC}_{\mathrm{L}}}}[\mathrm{Ca}_{\mathrm{i}}^{2+}]
       \left(\hat{B}_{\mathrm{TnC}_{\mathrm{L}}} -[\mathrm{TnC}_{\mathrm{L}}] \right)
     - k_{\mathrm{off}_{\mathrm{TnC}_{\mathrm{L}}}} [\mathrm{TnC}_{\mathrm{L}}] \nonumber
   \end{align}

is replaced by the analogous Eq.~(1) of the LN active stress model [#land_rat]_, given as

.. _eq-LandTnC:

.. math::
   :nowrap:
   :label: eq-LandTnC

   \begin{align}
     \frac{\mathrm{d}\,\mathrm{TRPN}}{\mathrm{d} t}
     = k_{\mathrm{TRPN}} \left(\frac{[\mathrm{Ca}_{\mathrm{i}}^{2+}]}
       {[\mathrm{Ca}_{\mathrm{i}}^{2+}]_{\mathrm{T50}}(\lambda)} \right)^{n_\mathrm{TRPN}}
       \hspace{-2ex}\left(1 - \mathrm{TRPN}\right)
                         - k_{\mathrm{TRPN}}\, \mathrm{TRPN}. \nonumber
   \end{align}

In the combined GPB+LN model :eq:`eq-Ca-Shannon` is replaced by :eq:`eq-LandTnC`,
scaled by the total buffer concentration :math:`\hat{B}_{\mathrm{TnC}_{\mathrm{L}}}`
assumed to be 70 :math:`\mu M/L` in the GPB model.
This scaling is necessary to correctly relate fractional occupancy,
used by the LN myofilament model, with the concentration of Troponin bound calcium,
used by the GPB model. Parameters of the GPB model were left unaltered whereas
parameters of the LN model were adapted to give human myocardium tension transients
when coupled to the GPB calcium transient [#tondel_land]_.

In single cell experiments, both the unaltered GPB model and the combined GPB+LN model
were paced at a cycle length of 500 ms for a duration of 60 seconds.
In the combined GPB+LN model the stretch ratio was kept constant at :math:`\lambda=1.0`
over the entire protocol.
In both models, all state variable transients were plotted over the entire pacing experiment
to ensure that the system had settled close to a stable limit cycle.
The effect of the altered Troponin buffering equation in the GPB+LN model
relative to the GPB model is illustrated in :numref:`fig-GPB-LN-modified`.

.. _fig-GPB-LN-modified:

.. figure:: /images/01_06_gpb_versus_modified.png
   :align: center
   :width: 75%

   Comparison of :math:`V_{\mathrm{m}}`, :math:`\mathrm{TnC}_{\mathrm{L}}`
   and :math:`[\mathrm{Ca}_{\mathrm{i}}^{2+}]` single cell traces
   between the GPB model (blue) and the GPB+LN model at steady state with :math:`\lambda=1` (red).

The effect of strong coupling in the GPB+LN model under fixed stretch conditions in the range
:math:`\lambda\in[1.0,1.2]`, with the same pacing protocol, is illustrated in
:numref:`fig-GPB-length-dep`.

.. _fig-GPB-length-dep:

.. figure:: /images/01_06_gpb_strong_length_dep_Cai.png
   :align: center
   :width: 75%

   Steady state traces of :math:`V_{\mathrm{m}}`,
   isometric tension, :math:`\mathrm{TnC}_{\mathrm{L}}`
   and :math:`[\mathrm{Ca}_{\mathrm{i}}^{2+}]` in a single cell pacing experiment
   under varying stretch ratios.


.. _exp-em-sc-12:

**Experiment exp12 (pace to limit cycle)**
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

As before, we pace the model for 20 seconds at a resting length of :math:`\lambda=1.`
to arrive at a limit cycle.

.. code-block:: bash

   ./run.py --EP gpb_land --duration 20000 --bcl 1000 --ID exp12  \
            --no-filter --visualize

.. _exp-em-sc-13:

**Experiment exp13 (confirm limit cycle)**
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Using the state of the myocyte stored in the file
``exp12_gpb_land_bcl_1000_ms_dur_20000_ms.sv`` in exp12
we confirm now that your trajectories are close to a limit cycle for the
given ``bcl`` and ``stretch`` we run

.. code-block:: bash

   ./run.py --EP augustin --duration 2000 --bcl 1000 --ID exp13 --stretch 1.0 \
            --init exp12_augustin_strongly_coupled_bcl_1000_ms_dur_20000_ms.sv \
            --visualize

Observe that there is no significant difference between the traces for AP1 and AP2.


.. _exp-em-sc-14:

**Experiment exp14 (return to rest)**
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

As this model is strongly coupled, mechanical loading influences EP in the myocyte.
To observe MEF effects we leave the limit cycle and stop pacing for 2 seconds.
That is, in this experiment we keep the myocyte quiescent,
we do not deliver an electrical stimulus current, nor a mechanical stretch protocol
and observe the return of trajectories to a resting state.

.. code-block:: bash

   ./run.py --experiment passive --EP augustin --duration 1000 --bcl 1000 \
            --ID exp14 --stretch 1.0 \
            --init exp12_augustin_strongly_coupled_bcl_1000_ms_dur_20000_ms.sv \
            --visualize

.. _exp-em-sc-15:

**Experiment exp15 (stretch resting myocyte, velocity dependence off)**
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

To observe the effect of cellular stretch upon EP we apply now mechanical loading protocols
to the resting myocyte.
As a reference case, we apply a step change in stretch of 20%
and observe only length-dependent effects, that is, velocity-dependence is turned off.
This is achieved with the loading protocol shown in :numref:`fig-gpb-land-mef-ld-protocol`.

.. _fig-gpb-land-mef-ld-protocol:

.. figure:: /images/01_06_gpb-land-mef-ld-protocol.png
   :align: center

   Mechanical loading protocol applied to stretch the cell.
   Velocity dependence is turned off in this case by setting
   :math:`\dot{\lambda}=0.0` (delLambda).

.. code-block:: bash

   ./run.py --experiment passive --EP augustin --duration 1000 --bcl 1000 \
            --ID exp15 --init exp14_augustin_strongly_coupled_bcl_1000_ms_dur_1000_ms.sv \
            --fs lambda_step_tr_time_100ms.dat --visualize

.. _exp-em-sc-16:

**Experiment exp16 (stretch resting myocyte, velocity dependence on, slower transition)**
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

We repeat experiment exp15 now, but with velocity dependence enabled.
First, we use a moderately fast step change by going from :math:`\lambda=1.0`
to :math:`\lambda=1.2` in 100 ms and compare results with exp15
where velocity dependence was disabled. See :numref:`fig-gpb-land-mef-ld-vd-slow-protocol`.

.. _fig-gpb-land-mef-ld-vd-slow-protocol:

.. figure:: /images/01_06_gpb-land-mef-ld-vd-slow-protocol.png
   :align: center

   Comparison of stretch protocols between exp15 where velocity dependence was disabled
   (:math:`\dot{\lambda}=0.0`) and with velocity-dependence enabled using a transition
   rate of the step change of 20% over 100 ms.

.. code-block:: bash

   ./run.py --experiment passive --EP augustin --duration 1000 --bcl 1000 \
            --ID exp16 --init exp14_augustin_strongly_coupled_bcl_1000_ms_dur_1000_ms.sv \
            --fs lambda_step_tr_time_100ms.dat --fsDot dlambda_step_tr_time_100ms.dat \
            --ref exp15 --visualize


.. _exp-em-sc-17:

**Experiment exp17 (stretch resting myocyte, velocity dependence on, faster transition)**
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

We repeat experiment exp16 now, but with velocity dependence enabled.
We use a faster step change by going from :math:`\lambda=1.0`
to :math:`\lambda=1.2` in only 10 msecs. See :numref:`fig-gpb-land-mef-ld-vd-fast-protocol`.

.. _fig-gpb-land-mef-ld-vd-fast-protocol:

.. figure:: /images/01_06_gpb-land-mef-ld-vd-fast-protocol.png
   :align: center

   Comparison of stretch protocols between slow transition (100 ms) and fast
   transition (10 ms) step changes.

.. code-block:: bash

   ./run.py --experiment passive --EP augustin --duration 1000 --bcl 1000 \
            --ID exp17 --init exp14_augustin_strongly_coupled_bcl_1000_ms_dur_1000_ms.sv \
            --fs lambda_step_tr_time_10ms.dat --fsDot dlambda_step_tr_time_10ms.dat \
            --ref exp16 --visualize

.. _ToRORd-LandHumanStress-model:

Strongly coupled electro-mechanical human ventricular myocyte model with passive component
---------------------------------------------------

In this set last set of experiments we couple the recent EP model of the
human ventricular myocyte due to Tomek et al [#tomek_torord]_ (Tomek-Rodriguez-O'Hara-Rudy ToR-ORd)
with the active tension model due to Land et al [#land_human]_ (Land-Niederer LN).
Details on the implementation and parameterization of the coupling have been reported in Margara et al [#margara]_.

.. _exp-em-sc-18:

**Experiment exp18 (pace to limit cycle)**
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

As before, we pace the model for 20 seconds to arrive at a limit cycle.

.. code-block:: bash

   ./run.py --EP torord_landhumanstresswithpassive --duration 20000 \
            --bcl 1000 --ID exp18 --no-filter --visualize

.. _exp-em-sc-19:

**Experiment exp19 (confirm limit cycle)**
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Using the state of the myocyte stored in the file
``exp18_torord_landhumanstresswithpassive_strongly_coupled_bcl_1000_ms_dur_20000_ms.sv`` in exp18
we confirm now that your trajectories are close to a limit cycle for the
given ``bcl`` and ``stretch`` we run

.. code-block:: bash

   ./run.py --EP torord_landhumanstresswithpassive --duration 2000 \
            --bcl 1000 --ID exp19 \
            --init exp18_torord_landhumanstresswithpassive_strongly_coupled_bcl_1000_ms_dur_20000_ms.sv \
            --visualize

Observe that there is no significant difference between the traces for AP1 and AP2.


.. _exp-em-sc-20:

**Experiment exp20 (return to rest)**
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Again, as this model is strongly coupled, mechanical loading influences EP in the myocyte.
To observe MEF effects we leave the limit cycle and stop pacing for 2 seconds.
That is, in this experiment we keep the myocyte quiescent,
we do not deliver an electrical stimulus current, nor a mechanical stretch protocol
and observe the return of trajectories to a resting state.

.. code-block:: bash

   ./run.py --experiment passive --EP torord_landhumanstresswithpassive \
            --duration 1000 --bcl 4000 --ID exp20 --visualize \
            --init exp18_torord_landhumanstresswithpassive_strongly_coupled_bcl_1000_ms_dur_20000_ms.sv



Literature
==========

.. [#niederer_length] Niederer SA, Plank G, Chinchapatnam P, Ginks M, Lamata P, Rhode KS, Rinaldi CA, Razavi R, Smith NP.
   **Length-dependent tension in the failing heart and the efficacy of
   cardiac resynchronization therapy**,
   Cardiovasc Res 89(2):336-43, 2011.
   [`PubMed <https://www.ncbi.nlm.nih.gov/pubmed/20952413>`__]
   [`Full Text <http://doi.org/10.1093/cvr/cvq318>`__]

.. [#crozier_relative] Crozier A, Blazevic B, Lamata P, Plank G, Ginks M, Duckett S, Sohal M, Shetty A, Rinaldi CA, Razavi R, Smith NP, Niederer SA.
   **The relative role of patient physiology and device optimisation in
   cardiac resynchronisation therapy: A computational modelling study**,
   [`PubMed <https://www.ncbi.nlm.nih.gov/pubmed/26546827>`__]
   [`Full Text <http://doi.org/10.1016/j.yjmcc.2015.10.026>`__]

.. [#land_human] Land S, Park-Holohan SJ, Smith NP, Dos Remedios CG, Kentish JC, Niederer SA.
   **A model of cardiac contraction based on novel measurements of tension
   development in human cardiomyocytes**,
   J Mol Cell Cardiol. 106:68-83, 2017.
   [`PubMed <https://www.ncbi.nlm.nih.gov/pubmed/28392437>`__]

.. [#land_rat] Land S, Niederer SA, Aronsen JM, Espe EK, Zhang L, Louch WE, Sjaastad I, Sejersted OM, Smith NP
   **An analysis of deformation-dependent electromechanical coupling in the mouse heart**,
   J Physiol 590(18):4553-69, 2012.
   [`PubMed <https://www.ncbi.nlm.nih.gov/pubmed/22615436>`__]

.. [#tondel_land] Tondel K, Land S, Niederer SA, Smith NP.
   **Quantifying inter-species differences in contractile function through biophysical modelling**,
   J Physiol 593(5):1083-111, 2015.
   [`PubMed <https://www.ncbi.nlm.nih.gov/pubmed/25480801>`__]
   [`Full Text <https://doi.org/10.1113/jphysiol.2014.279232>`__]

.. [#grandi_human_ventricle] Grandi E, Pasqualini FS, Bers DM.
   **A novel computational model of the human ventricular action potential and Ca transient**,
   J Mol Cell Cardiol 48, 112-121, 2010.
   [`PubMed <https://www.ncbi.nlm.nih.gov/pubmed/21921263>`__]
   [`Full Text <http://circres.ahajournals.org/content/109/9/1055.long>`__]

.. [#tomek_torord] Tomek J, Bueno-Orovio A, Rodriguez B.
   **ToR-ORd-dynCl: an update of the ToR-ORd model of human ventricular cardiomyocyte with dynamic intracellular chloride**,
   bioRxiv, 2020.
   [`Full Text <hhttps://www.biorxiv.org/content/10.1101/2020.06.01.127043v1.abstract>`__]

.. [#margara] Margara F, Wang ZJ, Levrero-Florencio F, Santiago A, Vasquez M, Bueno-Orovio A, Rodriguez B.
   **In-silico human electro-mechanical ventricular modelling and simulation for drug-induced pro-arrhythmia and inotropic risk assessment**,
   Prog Biophys Mol Biol 159:58-74, 2021.
   [`PubMed <https://pubmed.ncbi.nlm.nih.gov/32710902>`__]
   [`Full Text <https://www.sciencedirect.com/science/article/pii/S007961072030064X?via%3Dihub>`__]

.. [#augustin_hrmech] Augustin CM, Neic A, Liebmann M, Prassl AJ, Niederer SA, Haase G, Plank G.
   **Anatomically accurate high resolution modeling of human whole heart electromechanics:
   A strongly scalable algebraic multigrid solver method for nonlinear deformation**,
   J Comput Phys. 305:622-646, 2016.
   [`PubMed <https://www.ncbi.nlm.nih.gov/pubmed/26819483>`__ ]
   [`Full Text <http://www.sciencedirect.com/science/article/pii/S0021999115007226>`__]

"""

import os
import sys
import glob
import numpy as np

from carputils import settings
from carputils import tools
from carputils import model

EXAMPLE_DESCRIPTIVE_NAME = 'Electromechanical Single Cell Stretcher'
EXAMPLE_AUTHOR = 'Gernot Plank <gernot.plank@medunigraz.at> and Christoph Augustin <christoph.augustin@medunigraz.at>'

# strong coupling indicators:
STRONG_CIS = ("_landhuman", "_land", "_landhumanstress", "_landhumanstresswithpassive", "_rice")

# ----------------------------------------------------------------------------
def arg_parser():
    """ parse command line arguments """

    parser = tools.standard_parser()
    group = parser.add_argument_group('experiment specific options')

    group.add_argument('--experiment', default='active',
                       choices=['active', 'passive'],
                       help='pick experiment type')
    group.add_argument('--EP',
                       type=str, default='tentusscherpanfilov',
                       choices=list(model.ionic.keys()),
                       help='pick EP model')
    group.add_argument('--stress', default='stress_land17',
                       choices=list(model.activetension.keys()),
                       help='pick model for active stress')
    group.add_argument('--imp-params',
                       default="", type=str,
                       help='specify parameters of the EP model')
    group.add_argument('--stress-params',
                       default="", type=str,
                       help='specify parameters of the active stress model')
    group.add_argument('--stretch', type=float, default=1.0,
                       help='prescribe constant stretch over each cycle')
    group.add_argument('--init', default='',
                       help='pick state variable initialization file')
    group.add_argument('--duration', type=int, default=2000,
                       help='pick duration of experiment in ms')
    group.add_argument('--bcl', type=int, default=500,
                       help='pick basic cycle length in ms)')
    group.add_argument('--fs', default='',
                       help='provide trace file to prescribe fiber stretch')
    group.add_argument('--fsDot', default='',
                       help='provide trace file to prescribe fiber stretch rate')
    group.add_argument('--ca_i', default='',
                       help='provide calcium file')
    group.add_argument('--load-module', type = str,
                       help = 'IMP library to link (with .so ending)')
    group.add_argument('--ref', default='',
                       help='folder holding .bin files of a reference solution to be plotted in comparison')
    group.add_argument('--no-filter', action='store_true',
                       help='Usually we filter and show only pre-selected traces. ' \
                            'If this argument is set, all traces are shown')

    return parser

# ----------------------------------------------------------------------------
def job_id(args):
    """
    Generate name of top level output directory.
    """
    if args.EP.endswith(STRONG_CIS):
        args.stress = 'strongly_coupled'  # for output purposes
    tpl = f'exp_{args.EP}_{args.stress}_stretch_{args.stretch}'
    return tpl


# ----------------------------------------------------------------------------
def setup_ep_model(args):
    """ setup standard or strongly coupled EP model """
    ep_model = model.ionic.get(args.EP)
    ep_opts = [f'--imp={ep_model.MODELID}']

    # check if strongly coupled and add stress plugin if not
    if not (args.EP.endswith(STRONG_CIS) or args.stress == 'none'):
        at_model = model.activetension.get(args.stress)
        ep_opts += [f'--plug-in={at_model.PLUGIN}']
    return ep_opts

# ----------------------------------------------------------------------------
def prescribe_stretch(args):
    """ prescribe single cell stretch protocol """
    ext_load = []
    load_sv = load_sv_file = ''

    if args.fs != '':
        if not os.path.isfile(args.fs):
            print(f'Fiber stretch file {args.fs} not found!')
            sys.exit(-1)

        load_sv += 'Lambda'
        load_sv_file += f'{args.fs}'
        if args.fsDot != '':
            if not os.path.isfile(args.fsDot):
                print(f'Fiber stretch rate file {args.fsDot} not found!')
                sys.exit(-1)

            load_sv += ':delLambda'
            load_sv_file += f':{args.fsDot}'

    if args.ca_i != '':
        if not os.path.isfile(args.ca_i):
            print(f'Calcium file {args.ca_i} not found!')
            sys.exit(-1)

        load_sv += ':Ca_i'
        load_sv_file += f'{args.ca_i}'

    if args.fs != '' or args.ca_i != '':
        ext_load += ['--clamp-SVs', load_sv,
                    '--SV-clamp-files', load_sv_file]
    if args.fs == '':
        # no fiber stretch prescribed, keep cell isometric
        constant_stretch = float(args.stretch)

        constant_strain = constant_stretch - 1.
        ext_load += ['--strain', constant_strain,
                    '--strain-dur', float(args.duration)+1,
                    '--strain-rate', 0.,
                    '--strain-time', -2.0]
    return ext_load

# ----------------------------------------------------------------------------
def update_landhuman_stress_par():
    """
    update Land human stress model parameters
    not mentioned parameters will have the default value
    """
    param = {'perm50':      0.35,
             'TRPN_n':      2.0,
             'koff':        0.1,
             'dr':          0.25,
             'wfrac':       0.5,
             'TOT_A':      25.0,
             'ktm_unblock': 1.0,
             'beta_1':     -2.4,
             'beta_0':      2.3,
             'gamma':       0.0085,
             'gamma_wu':    0.615,
             'phi':         2.23,
             'nperm':       5,
             'ca50':        0.805,
             'Tref':      120.0,
             'nu':          7,
             'mu':          3}
    return param

# ----------------------------------------------------------------------------
def update_land_stress_par():
    """
    update Land stress model parameters
    not mentioned parameters will have the default value
    """
    param = {'T_ref': 117.1,
             'Ca_50ref': 0.52,
             'TRPN_50': 0.37,
             'n_TRPN': 1.54,
             'k_TRPN': 0.08,
             'n_xb': 3.38,
             'k_xb': 4.9e-3}
    return param

# ----------------------------------------------------------------------------
def update_tanh_stress_par():
    """
    update tanh stress model parameters
    not mentioned parameters will have the default value
    """
    param = {'Tpeak': 100.0,
             'tau_c0': 40.0,
             'tau_r': 110.0,
             't_dur': 400.0,
             'ldOn': 1}
    return param

# ----------------------------------------------------------------------------
def update_torord_landhumanstress_par():
    """
    update TorORd strongly coupled to LandHuman stress parameters
    not mentioned parameters will have the default value
    """

    param = {'celltype': 1,      #   EPI setting from Margara et al. 2022
             'Tref': 120,        #   Tref    = 120
             'TRPN_n': 2.0,      #   TRPN_n  = 2.0
             'ca50': 0.805,      #   ca50    = 0.805
             'koff': 0.1,        #   koff    = 0.1
             'beta_0': 2.3,      #   beta0   = 2.3
             'ktm_unblock': 0.021,
             'nperm': 2.036}

    return param

# ----------------------------------------------------------------------------
def setup_params(args):
    """ set EP and active tension parameters """
    ep_pars = []
    # check if strongly coupled
    if args.stress == 'none':
        if args.imp_params != "":
            ep_pars += ['--imp-par', args.imp_params]
    elif args.EP.endswith(STRONG_CIS):
        args.stress = 'strongly_coupled'  # for output purposes
        ta_params = {}
        if args.stress_params != "":
            ta_params = args.stress_params
        else:
            if args.EP == 'augustin':
                ta_params = {'Tref': 120, 'TRPN_n': 0.5, 'koff': 0.1,
                             'lengthDep': 1}
            elif args.EP.startswith('torord_landhumanstress'):
                ta_params = update_torord_landhumanstress_par()
            ta_params = model.convert_param_dict(ta_params)
        if args.imp_params != "":
            ta_params += args.imp_params
        if ta_params:
            ep_pars = ['--imp-par', ta_params]
    else:
        # overrule default parametrization? (all other params are default)
        if args.stress_params != "":
            ta_params = args.stress_params
        else:
            if args.stress.lower() == 'stress_land17':
                ta_params = update_landhuman_stress_par()
            elif args.stress.lower() == 'stress_land12':
                ta_params = update_land_stress_par()
            elif args.stress.lower() in ['stress_niederer']:
                ta_params = update_niederer_stress_par()
            else:  # use default values for other stress models
                ta_params = {}
            ta_params = model.convert_param_dict(ta_params)

        # add an active stress plug-in
        ep_pars = ['--plug-par', ta_params]

        if args.imp_params != "":
            ep_pars += ['--imp-par', args.imp_params]

    return ep_pars

# # ----------------------------------------------------------------------------
# def launch_limpet_gui(args, job):
#     """ prepare launching of visualization """

#     # build hdf5 file
#     traces_header = f'{job.ID}/{args.EP}_{args.stress}_header.txt'
#     trace_h5 = f'{job.ID}_traces.h5'

#     whitelist = create_whitelist(args)

#     h5cmd = [settings.execs.sv2h5b, traces_header, trace_h5] + whitelist

#     job.bash(h5cmd)


#     # limpetGUI command
#     vcmd = [settings.execs.limpetgui,
#             f'{job.ID}_traces.h5']

#     # do we have a reference trace to compare against?
#     if args.refH5 != '':
#         vcmd += [args.refH5]

#     job.bash(vcmd)

# ----------------------------------------------------------------------------
def create_whitelist(args):
    """ create filter for limpetgui """
    whitelist = []
    if not args.no_filter:
        wlfilter = f'filter_{args.EP}_{args.stress}.txt'
        if os.path.isfile(wlfilter):
            toks = np.loadtxt(wlfilter, dtype=str)
            toklist = toks.tolist()
        else:
            toklist = filters_ep()
            # EP filters
            if 'tentusscherpanfilov' in args.EP.lower():
                toklist += filters_tentusscherpanfilov()
            elif 'torord' in args.EP.lower():
                toklist += filters_torord()
            # stress filters
            toklist += filters_active_stress()
            ep_stress = args.EP.lower() + args.stress.lower()
            if 'withpassive' in ep_stress:
                toklist += filters_withpassive()
            elif 'stress_land17' in ep_stress:
                toklist += filters_landhumanstress()
            elif 'stress_land12' in ep_stress:
                toklist += filters_landstress()

        if toklist != []:
            whitelist = ['--whitelist'] + toklist

    return whitelist

def filters_ep():
    """ filters for general EP model """
    return["t", "Vm", "Iion", "Cai"]

def filters_active_stress():
    """ filters for general EP model """
    return["Tension", "delLambda"]

def filters_tentusscherpanfilov():
    """ filters for tentusscherpanfilov EP model """
    return["CaSR", "CaSS"]

def filters_torord():
    """ filters for ToRORd EP model """
    return["cass", "CaMKt", "nca_i", 'cai', 'cli', 'nai']

def filters_withpassive():
    """ filters for land human stress model """
    return["stretch", "TRPN", "XS", "XW"]

def filters_landhumanstress():
    """ filters for land human stress model """
    return["Lambda", "TRPN", "XS", "XW"]

def filters_landstress():
    """ filters for land human stress model """
    return["Lambda", "Q_1", "Q_2", "TRPN", "xb"]

def findRow(theEntry, theList, column):
    for i, item in enumerate(theList):
        if item[column] == theEntry:
            return i
    print("%s not found in list!"%theEntry)

def visualize(args, job):
    import matplotlib.pyplot as plt

    path = job.ID
    whitelist = create_whitelist(args)
    variables = whitelist[1:-1]
    ep_model = model.ionic.get(args.EP)
    ep_modelid = ep_model.MODELID
    stress_model = model.activetension.get(args.stress)
    stress_modelid = stress_model.PLUGIN

    txt_file = f'{job.ID}/{args.EP}_{args.stress}_header.txt'

    dat_files = glob.glob(os.path.join('*.dat'))
    sv_names = open(txt_file).read().splitlines()
    for i_sv, e_sv in enumerate(sv_names):
        e_sv = e_sv.split()
        e_sv.append("")
        e_sv[4] = e_sv[0].replace(f'{args.EP}_{args.stress}_{ep_modelid}.', "")
        e_sv[4] = e_sv[4].replace(f'{args.EP}_{args.stress}_{stress_modelid}.', "")
        e_sv[4] = e_sv[4].replace(f'{args.EP}_{args.stress}.', "")
        e_sv[4] = e_sv[4].replace(".bin", "")
        sv_names[i_sv] = e_sv  # modify original list

    plotData=[]
    refData=[]
    for variable in variables:
        i_sv = findRow(variable, sv_names, 4)
        with open(f'{job.ID}/{sv_names[i_sv][0]}', "rb") as f:
            if sv_names[i_sv][2] == "8":   # double
                plotData.append(np.fromfile(f, dtype=np.float64, count=int(sv_names[i_sv][3]), sep=''))
            elif sv_names[i_sv][2] == "4":   # float
                plotData.append(np.fromfile(f, dtype=np.float32, count=int(sv_names[i_sv][3]), sep=''))
        if args.ref:
            with open(f'{args.ref}/{sv_names[i_sv][0]}', "rb") as f:
                if sv_names[i_sv][2] == "8":   # double
                    refData.append(np.fromfile(f, dtype=np.float64, count=int(sv_names[i_sv][3]), sep=''))
                elif sv_names[i_sv][2] == "4":   # float
                    refData.append(np.fromfile(f, dtype=np.float32, count=int(sv_names[i_sv][3]), sep=''))

    fig, axes = plt.subplots(1, len(plotData)-1, sharex=True, sharey=False)
    for i, ax in enumerate(axes.flatten()):
        ax.plot(plotData[0], plotData[i+1])
        if args.ref:
            ax.plot(plotData[0], refData[i+1], '--')
        ax.set_title(variables[i+1])
        ax.set_xlabel('Time (ms)')
    plt.show()

# ----------------------------------------------------------------------------
@tools.carpexample(arg_parser, job_id, clean_pattern=r'^(\d{4}-\d{2}-\d{2})|(mesh)|exp*')
def run(args, job):
    """ run bench """

    # run bench with available ionic models
    cmd = [settings.execs.BENCH,
           '--duration', args.duration
          ]

    if args.load_module:
        if os.path.exists(args.load_module):
            module = os.path.abspath(args.load_module)
            cmd += ['--load-module', module]
            args.stress = os.path.splitext(os.path.basename(module))[0]
        else:
            print(f'Could not find module "{args.load_module}" anywhere! Aborting...')
            return

    # setup EP-active stress combination
    cmd += setup_ep_model(args)

    # parametrize EP-active stress combination
    cmd += setup_params(args)

    # apply stimulus current or observe only response to imposed stretch?
    if args.experiment == 'passive':
        stim_curr = 0.0
    else:
        stim_curr = 30.

    cmd += ['--stim-curr', stim_curr,
            '--stim-start', 0.0,
            '--numstim', int(float(args.duration)/float(args.bcl)+1),
            '--bcl', args.bcl]

    # prescribe external loading protocols
    cmd += prescribe_stretch(args)

    # numerical settings
    cmd += ['--dt', 1e-2]

    # IO and state management
    exp_id = ''
    if args.ID:
        exp_id = f'{args.ID}_'
    init_file = f'{exp_id}{args.EP}_{args.stress}_bcl_{args.bcl}_ms_dur_{args.duration}_ms'
    if args.suffix:
        init_file += f'_{args.suffix}'
    init_file += '.sv'
    cmd += ['--dt-out', 0.1,
            '--save-ini-file', init_file,
            '--save-ini-time', args.duration]

    # use steady state initialization vectors
    if args.init != '':
        if not os.path.isfile(args.init):
            print(f'State variable initialization file {args.init} not found!')
            sys.exit(-1)

        cmd += ["--read-ini-file", args.init]

    if args.visualize and not settings.platform.BATCH:
        cmd += ['--validate']

    # Output options
    cmd += [f'--fout={os.path.join(job.ID, args.EP)}_{args.stress}',
            '--bin']

    job.mpi(cmd, f'Testing {args.EP}-{args.stress}')

    # Do visualization
    if args.visualize and not settings.platform.BATCH:

        # detailed visualization of all relevant traces
        #launch_limpet_gui(args, job)
        visualize(args, job)

# ----------------------------------------------------------------------------
if __name__ == '__main__':
    run()
