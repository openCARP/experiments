__title__ = 'Import CellML'
__description__ = 'Here you learn how to import CellML data into openCARP and what problems might occur during this process'
__q2a_tags__ = 'easyml,cellml'