__title__ = 'Voltage clamp'
__description__ = 'This example explains the basic usage of bench for performing voltage clamp experiments.'
__image__ = '/images/01_03_voltage_clamp.png'
__q2a_tags__ = 'limpet,experiments,examples,parameters,bench,single_cell'