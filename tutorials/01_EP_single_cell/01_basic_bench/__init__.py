__title__ = 'Basic single cell EP'
__description__ = 'This example introduces the basic steps of running EP simulations in an isolated myocytes'
__image__ = '/images/01_01_MyocyteEP_Setup.png'
__q2a_tags__ = 'limpet,experiments,examples,parameters,bench,single_cell'
