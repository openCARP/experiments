#!/usr/bin/env python
import myokit
from myokit import formats
import argparse

parser = argparse.ArgumentParser(description='Convert a .mmt model to CellML.')
parser.add_argument('model', help='model.mmt is converted to model.cellml')
args = parser.parse_args()

# Load a model, protocol and embedded script
m, p, x = myokit.load('%s.mmt'%args.model)

i = formats.exporter('cellml')

i.model(model=m, path='%s.cellml'%args.model, version='2.0')

