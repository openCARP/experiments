#!/usr/bin/env python

"""
======================
Converting EasyML to CellML
======================

Intro
======

CellML is a markup language for describing, amongst other things, ionic models. Many models are published and available in the
`CellML Model Repository <https://models.cellml.org/electrophysiology>`_. However, this format can only be processed by a machine. openCARP uses its own markup language called :ref:`EasyML <EasyML>`.
This tutorial describes how to convert your EasyML .model file to CellML for
sharing it with others or publishing it in the CelML Model Repository.
In this process, we employ `Myokit <http://myokit.org>`_'s .mmt format as an intermediate step.

Prerequisites
=============

This tutorial assumes that you installed openCARP from source as you likely
had to compile your new .model file anyway. It should also work with other
forms of installation but you may need to adapt some paths.

You need to have myokit installed and in your ``PYTHONPATH``. Often
this can be done by simply running ``pip3 install myokit``. See the `Myokit webpage <http://myokit.org>`_ for more detailed information.

Converting .model to .mmt
=========================

In a first step, the EasyML .model file is converted to Myokit's .mmt format.
For this, a python script is shipped with openCARP. Conversion of a CellML file
is performed using the ``cellml_converter.py`` script::

    usage: EasyML2mmt.py [-h] [--verbose] [--init_component INIT_COMPONENT]
                     [--istim ISTIM] [--pace_name PACE_NAME] [--tend TEND]
                     [--bcl BCL] [--plugin] [--author AUTHOR] [--desc DESC]
                     eml

    convert a EasyML model file to myokit mmt format

    In the EasyML file, comments with meta data provide the component name,
    which is not part of the EasyML specification. A component called cname is
    identified by one of the 2 forms:

    # COMPONENT cname
    // COMPONENT cname

    All variables and equations belong to the last component declared.
    By default, an initial component cell is in effect, but this particular name
    can be changed.

    N.B.: groups within groups are not supported
    N.B.: hyperbolic trig functions are not supported
    N.B.: try to avoid using variables named just d or inf

    positional arguments:
      eml                   EasyML file

    optional arguments:
      -h, --help            show this help message and exit
      --verbose             verbose output
      --init_component INIT_COMPONENT
                            initial component [cell]
      --istim ISTIM         name of stimulus current [I_stim]
      --pace_name PACE_NAME
                            name of pacing variable [pace]
      --tend TEND           duration [1100.0]
      --bcl BCL             pacing cycle length [500.0]
      --plugin              IMP is a plugin


Set the ``--plugin`` flag when your EasyML file describes a plugin (i.e. it adds
at value to ``Iion``) rather than a model (i.e. it defines ``Iion``).
When conversion to CellML is your goal, you can stay with the default values
for the other optional parameters as they mostly control the simulation protocol
in Myokit. The output of this step will be an .mmt file.
Watch out for warnings as some EasyML expressions are not supported in Myokit.

Copy the .model file to the local directory, for example:
    ``cp /path/to/openCARP/physics/limpet/models/DrouhardRoberge.model .``

Example call:
    ``python3 /path/to/openCARP/physics/limpet/src/python/EasyML2mmt.py DrouhardRoberge.model``

Converting .mmt to .cellml
==========================

We use Myokit to generate the final CellML file. script::

    usage: convert.py [-h] model

    Convert a .mmt model to CellML.

    positional arguments:
      model       model.mmt is converted to model.cellml

    optional arguments:
      -h, --help  show this help message and exit

The output of this step is the .cellml file that can then be uploaded to the
`CellML Model Repository <https://models.cellml.org/electrophysiology>`_ for
example.

Example call:
    ``python3 convert.py DrouhardRoberge``


"""


EXAMPLE_DESCRIPTIVE_NAME = 'Converting EasyML to CellML'
EXAMPLE_AUTHOR = 'Axel Loewe <axel.loewe@kit.edu>, Jorge Sánchez <jorge.arciniegas@kit.edu>'
GUIinclude = False

from carputils import tools

def parser():
    """ Setup command line parser.
    """
    parser = tools.standard_parser(False)
    return parser

