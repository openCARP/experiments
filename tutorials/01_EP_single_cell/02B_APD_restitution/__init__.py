__title__ = 'APD restitution'
__description__ = 'Action potenital duration (APD) restitution example in single cell. As pacing frequency is increased, APD shortens to maintain a one to one stimulus to responses'
__image__ = '/images/01_02_APD_res_curves.png'
__q2a_tags__ = 'limpet,experiments,examples,parameters,bench,single_cell'