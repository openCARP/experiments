# %% [markdown]
# # Tutorial ...01_EP_single_cell.02B_APD_restitution.run

# %%
import ipywidgets as widgets


def generate_widgets():
    
    generated_widgets = dict()
    
    # widget_np = widgets.IntText(
    #         value='1',
    #         description='np',
    #         disabled=False
    #     )
    # label_np = widgets.Label('number of processes (max=None)')
    # generated_widgets['np'] =widgets.HBox([widget_np, label_np])
    
    # widget_np_job = widgets.Text(placeholder='Type something',
    #         description='np-job',
    #         disabled=False
    #     )
    # label_np_job = widgets.Label('number of processes per job (for multiprocessing)')
    # generated_widgets['np-job'] =widgets.HBox([widget_np_job, label_np_job])
    
    # widget_tpp = widgets.Text(placeholder='Type something',
    #         description='tpp',
    #         disabled=False
    #     )
    # label_tpp = widgets.Label('threads per process')
    # generated_widgets['tpp'] =widgets.HBox([widget_tpp, label_tpp])
    
    # widget_runtime = widgets.Text(placeholder='Type something',
    #         description='runtime',
    #         disabled=False
    #     )
    # label_runtime = widgets.Label('max job runtime')
    # generated_widgets['runtime'] =widgets.HBox([widget_runtime, label_runtime])
    
    # widget_build = widgets.Dropdown(options = ('CPU',), value = 'CPU', description = 'build', disabled=False,)
    # label_build = widgets.Label('openCARP build to use')
    # generated_widgets['build'] =widgets.HBox([widget_build, label_build])
    
    # widget_flv = widgets.Dropdown(options = ('petsc', 'ginkgo', 'petsc-gamg-agg', 'petsc-pipelined', 'boomeramg', 'boomeramg-pipelined', 'parasails', 'pt', 'direct'), value = 'petsc', description = 'flavor', disabled=False,)
    # label_flv = widgets.Label('openCARP flavor')
    # generated_widgets['flavor'] =widgets.HBox([widget_flv, label_flv])
    
    # widget_platform = widgets.Dropdown(options = ['desktop', 'medbionode', 'archer', 'archer24c', 'archer2', 'archer2_e756', 'archer_intel', 'archer_camel', 'bwunicluster', 'curie', 'horeka', 'marconi', 'marconi_slurm', 'marconi_debug', 'medtronic', 'mephisto', 'smuc_f', 'smuc_t', 'smuc_i', 'vsc2', 'vsc3', 'vsc4', 'vsc5', 'wopr'], value = 'desktop', description = 'platform', disabled=False,)
    # label_platform = widgets.Label('pick a hardware profile from available platforms')
    # generated_widgets['platform'] =widgets.HBox([widget_platform, label_platform])
    
    # widget_queue = widgets.Text(placeholder='Type something',
    #         description='queue',
    #         disabled=False
    #     )
    # label_queue = widgets.Label('select a queue to submit job to (batch systems only)')
    # generated_widgets['queue'] =widgets.HBox([widget_queue, label_queue])
    
    # widget_vectorized_fe = widgets.Text(placeholder='Type something',
    #         description='vectorized-fe',
    #         disabled=False
    #     )
    # label_vectorized_fe = widgets.Label('vectorized FE assembly (default: on with FEMLIB_CUDA, off otherwise)')
    # generated_widgets['vectorized-fe'] =widgets.HBox([widget_vectorized_fe, label_vectorized_fe])
    
    # widget_dry = widgets.Text(placeholder='Type something',
    #         description='dry-run',
    #         disabled=False
    #     )
    # label_dry = widgets.Label('show command line without running the test')
    # generated_widgets['dry-run'] =widgets.HBox([widget_dry, label_dry])
    
    # widget_checkpoint = widgets.Text(placeholder='Type something',
    #         description='checkpoint',
    #         disabled=False
    #     )
    # label_checkpoint = widgets.Label('Set checkpoint interval.')
    # generated_widgets['checkpoint'] =widgets.HBox([widget_checkpoint, label_checkpoint])
    
    # widget_restore = widgets.Text(placeholder='Type something',
    #         description='restore',
    #         disabled=False
    #     )
    # label_restore = widgets.Label('Restart from given checkpoint file')
    # generated_widgets['restore'] =widgets.HBox([widget_restore, label_restore])
    
    # widget_generate_parfile = widgets.Text(placeholder='Type something',
    #         description='generate-parfile',
    #         disabled=False
    #     )
    # label_generate_parfile = widgets.Label('Generate parameter file with all chosen options')
    # generated_widgets['generate-parfile'] =widgets.HBox([widget_generate_parfile, label_generate_parfile])
    
    # widget_polling_param = widgets.Text(placeholder='Type something',
    #         description='polling-param',
    #         disabled=False
    #     )
    # label_polling_param = widgets.Label('Polling parameter')
    # generated_widgets['polling-param'] =widgets.HBox([widget_polling_param, label_polling_param])
    
    # widget_polling_range = widgets.Text(placeholder='Type something',
    #         description='polling-range',
    #         disabled=False
    #     )
    # label_polling_range = widgets.Label('Define polling parameter range')
    # generated_widgets['polling-range'] =widgets.HBox([widget_polling_range, label_polling_range])
    
    # widget_polling_file = widgets.Text(placeholder='Type something',
    #         description='polling-file',
    #         disabled=False
    #     )
    # label_polling_file = widgets.Label('File including polling data for parameter sweeps')
    # generated_widgets['polling-file'] =widgets.HBox([widget_polling_file, label_polling_file])
    
    # widget_sampling_type = widgets.Dropdown(options = ['linear', 'geometric', 'lhs'], value = 'linear', description = 'sampling-type', disabled=False,)
    # label_sampling_type = widgets.Label('Sampling type for parameter sweeps. Choose between "linear", "geometric" and latin hypercube ("lhs") sampling')
    # generated_widgets['sampling-type'] =widgets.HBox([widget_sampling_type, label_sampling_type])
    
    # widget_stress_models = widgets.Text(value="",placeholder='Type something',
    #         description='stress-models',
    #         disabled=False
    #     )
    # label_stress_models = widgets.Label('print stress-models and exit')
    # generated_widgets['stress-models'] =widgets.HBox([widget_stress_models, label_stress_models])
    
    # widget_material_models = widgets.Text(value="",placeholder='Type something',
    #         description='material-models',
    #         disabled=False
    #     )
    # label_material_models = widgets.Label('print material-models and exit')
    # generated_widgets['material-models'] =widgets.HBox([widget_material_models, label_material_models])
    
    # widget_ionic_models = widgets.Text(value="",placeholder='Type something',
    #         description='ionic-models',
    #         disabled=False
    #     )
    # label_ionic_models = widgets.Label('print ionic-models and exit')
    # generated_widgets['ionic-models'] =widgets.HBox([widget_ionic_models, label_ionic_models])
    
    widget_CARP_opts = widgets.Text(placeholder='Type something',
            description='CARP-opts',
            disabled=False
        )
    label_CARP_opts = widgets.Label('arbitrary openCARP options to append to command')
    generated_widgets['CARP-opts'] =widgets.HBox([widget_CARP_opts, label_CARP_opts])
    
    # widget_postprocess = widgets.Dropdown(options = ('phie', 'optic', 'activation', 'axial', 'filament', 'efield', 'mechanics'), description = 'postprocess', disabled=False,)
    # label_postprocess = widgets.Label('postprocessing mode(s) to execute')
    # generated_widgets['postprocess'] =widgets.HBox([widget_postprocess, label_postprocess])
    
    # widget_bundle = widgets.Text(placeholder='Type something',
    #         description='bundle',
    #         disabled=False
    #     )
    # label_bundle = widgets.Label('create a self-contained shareable bundle of the experiment')
    # generated_widgets['bundle'] =widgets.HBox([widget_bundle, label_bundle])
    
    # widget_push_bundle = widgets.Text(placeholder='Type something',
    #         description='push-bundle',
    #         disabled=False
    #     )
    # label_push_bundle = widgets.Label('Uploads the bundle on a git repository. Requires the --bundle option.                     The link to the repository can be given as an argument to this option or                     in METADATA.yml, in the `bundle_repository` field.')
    # generated_widgets['push-bundle'] =widgets.HBox([widget_push_bundle, label_push_bundle])
    
    # widget_release_bundle = widgets.Text(placeholder='Type something',
    #         description='release-bundle',
    #         disabled=False
    #     )
    # label_release_bundle = widgets.Label('Create the email that has to be sent to release the experiment on opencarp.org. Requires --push-bundle.')
    # generated_widgets['release-bundle'] =widgets.HBox([widget_release_bundle, label_release_bundle])
    
    # widget_bundle_output = widgets.Text(placeholder='Type something',
    #         description='bundle-output',
    #         disabled=False
    #     )
    # label_bundle_output = widgets.Label('Add the output of the simulation to the bundle')
    # generated_widgets['bundle-output'] =widgets.HBox([widget_bundle_output, label_bundle_output])
    
    # widget_gdb = widgets.Text(placeholder='Type something',
    #         description='gdb',
    #         disabled=False
    #     )
    # label_gdb = widgets.Label('start (optionally specified) processes in gdb')
    # generated_widgets['gdb'] =widgets.HBox([widget_gdb, label_gdb])
    
    # widget_lldb = widgets.Text(placeholder='Type something',
    #         description='lldb',
    #         disabled=False
    #     )
    # label_lldb = widgets.Label('start (optionally specified) processes in lldb')
    # generated_widgets['lldb'] =widgets.HBox([widget_lldb, label_lldb])
    
    # widget_ddd = widgets.Text(placeholder='Type something',
    #         description='ddd',
    #         disabled=False
    #     )
    # label_ddd = widgets.Label('start (optionally specified) processes in ddd')
    # generated_widgets['ddd'] =widgets.HBox([widget_ddd, label_ddd])
    
    # widget_ddt = widgets.Text(placeholder='Type something',
    #         description='ddt',
    #         disabled=False
    #     )
    # label_ddt = widgets.Label('start in Allinea ddt debugger')
    # generated_widgets['ddt'] =widgets.HBox([widget_ddt, label_ddt])
    
    # widget_valgrind = widgets.Text(placeholder='Type something',
    #         description='valgrind',
    #         disabled=False
    #     )
    # label_valgrind = widgets.Label('start in valgrind mode, use in conjunction with --gdb for interactive mode')
    # generated_widgets['valgrind'] =widgets.HBox([widget_valgrind, label_valgrind])
    
    # widget_valgrind_options = widgets.Text(value="",placeholder='Type something',
    #         description='valgrind-options',
    #         disabled=False
    #     )
    # label_valgrind_options = widgets.Label('specify valgrind CLI options, without preceding `--`')
    # generated_widgets['valgrind-options'] =widgets.HBox([widget_valgrind_options, label_valgrind_options])
    
    # widget_map = widgets.Text(placeholder='Type something',
    #         description='map',
    #         disabled=False
    #     )
    # label_map = widgets.Label('start using Allinea map profiler')
    # generated_widgets['map'] =widgets.HBox([widget_map, label_map])
    
    # widget_scalasca = widgets.Text(placeholder='Type something',
    #         description='scalasca',
    #         disabled=False
    #     )
    # label_scalasca = widgets.Label('start in scalasca profiling mode')
    # generated_widgets['scalasca'] =widgets.HBox([widget_scalasca, label_scalasca])
    
    widget_ID = widgets.Text(placeholder='Type something',
            value = 'results',
            description='ID',
            disabled=False
        )
    label_ID = widgets.Label('manually specify the job ID (output directory)')
    generated_widgets['ID'] =widgets.HBox([widget_ID, label_ID])
    
    # widget_suffix = widgets.Text(placeholder='Type something',
    #         description='suffix',
    #         disabled=False
    #     )
    # label_suffix = widgets.Label('add a suffix to the job ID (output directory)')
    # generated_widgets['suffix'] =widgets.HBox([widget_suffix, label_suffix])
    
    widget_overwrite_behaviour = widgets.Dropdown(options = ('prompt', 'error', 'delete', 'overwrite', 'append'), value = 'delete', description = 'overwrite-behaviour', disabled=False,)
    label_overwrite_behaviour = widgets.Label('behaviour when output directory already exists')
    generated_widgets['overwrite-behaviour'] =widgets.HBox([widget_overwrite_behaviour, label_overwrite_behaviour])
    
    # widget_silent = widgets.Text(placeholder='Type something',
    #         description='silent',
    #         disabled=False
    #     )
    # label_silent = widgets.Label('toggle silent output')
    # generated_widgets['silent'] =widgets.HBox([widget_silent, label_silent])
    
    # widget_visualize = widgets.Text(placeholder='Type something',
    #         description='visualize',
    #         disabled=False
    #     )
    # label_visualize = widgets.Label('toggle test results visualisation')
    # generated_widgets['visualize'] =widgets.HBox([widget_visualize, label_visualize])
    
    # widget_mech_element = widgets.Dropdown(options = ('P1-P0', 'P1-P1-DB', 'MINI'), value = 'P1-P0', description = 'mech-element', disabled=False,)
    # label_mech_element = widgets.Label('CARP default mechanics finite element')
    # generated_widgets['mech-element'] =widgets.HBox([widget_mech_element, label_mech_element])
    
    # widget_mech_with_inertia = widgets.Text(placeholder='Type something',
    #         description='mech-with-inertia',
    #         disabled=False
    #     )
    # label_mech_with_inertia = widgets.Label('toggle mechanics generalized alpha integrator')
    # generated_widgets['mech-with-inertia'] =widgets.HBox([widget_mech_with_inertia, label_mech_with_inertia])
    
    # widget_webGUI = widgets.Text(placeholder='Type something',
    #         description='webGUI',
    #         disabled=False
    #     )
    # label_webGUI = widgets.Label('run the example for visualization in the web GUI')
    # generated_widgets['webGUI'] =widgets.HBox([widget_webGUI, label_webGUI])
    
    # widget_Protocol = widgets.Dropdown(options = ['S1S2', 'Dynamic'], value = 'S1S2', description = 'Protocol', disabled=False,)
    # label_Protocol = widgets.Label('Pacing protocol for restitution curve')
    # generated_widgets['Protocol'] =widgets.HBox([widget_Protocol, label_Protocol])
    
    widget_prebeats = widgets.IntText(
            value=20,
            description='prebeats',
            disabled=False
        )
    label_prebeats = widgets.Label('Number of pre-pacing beats at chosen pacing cycle length PCL')
    generated_widgets['prebeats'] =widgets.HBox([widget_prebeats, label_prebeats])
    
    widget_initial = widgets.Text(placeholder='Type something',
            description='initial',
            disabled=False
        )
    label_initial = widgets.Label('Initialize with a stabilized limit cycle state vector precomputed for the chosen PCL')
    generated_widgets['initial'] =widgets.HBox([widget_initial, label_initial])
    
    widget_nbeats = widgets.IntText(
            value=5,
            description='nbeats',
            disabled=False
        )
    label_nbeats = widgets.Label('Number of beats for S1 pacing at CI1')
    generated_widgets['nbeats'] =widgets.HBox([widget_nbeats, label_nbeats])
    
    widget_BCL = widgets.IntText(
            value=600,
            description='BCL',
            disabled=False
        )
    label_BCL = widgets.Label('Reference basic cycle length')
    generated_widgets['BCL'] =widgets.HBox([widget_BCL, label_BCL])
    
    widget_CI0 = widgets.IntText(
            value=50,
            description='CI0',
            disabled=False
        )
    label_CI0 = widgets.Label('Shortest coupling interval (default: 50 ms)')
    generated_widgets['CI0'] =widgets.HBox([widget_CI0, label_CI0])
    
    widget_CI1 = widgets.Text(placeholder='Type something',
            description='CI1',
            disabled=False
        )
    label_CI1 = widgets.Label('Longest coupling interval (default: PCL)')
    generated_widgets['CI1'] =widgets.HBox([widget_CI1, label_CI1])
    
    widget_CIinc = widgets.IntText(
            value=25,
            description='Any:',
            disabled=False
        )
    label_CIinc = widgets.Label('Decrement for coupling interval (default: 25 ms)')
    generated_widgets['CIinc'] =widgets.HBox([widget_CIinc, label_CIinc])
    
    all_models = ['AlievPanfilov',
                  'AslanidiSleiman',
                  'Augustin',
                  'Bondarenko',
                  'Campos',
                  'Courtemanche',
                  'DiFrancescoNoble',
                  'DrouhardRoberge',
                  'Fox',
                  'Grandi',
                  'GrandiPanditVoigt',
                  'HodgkinHuxley',
                  'Inada',
                  'IribeKohl',
                  'Kurata',
                  'Loewe',
                  'LuoRudy91',
                  'LuoRudy94',
                  'MacCannell',
                  'MahajanShiferaw',
                  'Maleckar',
                  'MitchellSchaeffer',
                  'Nygren',
                  'OHara',
                  'Pathmanathan',
                  'Plonsey',
                  'Ramirez',
                  'Shannon',
                  'Skibsbye',
                  'Steward',
                  'Tomek',
                  'WangSobie',
                  'tenTusscherPanfilov']
    widget_imp = widgets.Dropdown(options = all_models, value = 'tenTusscherPanfilov', description = 'imp', disabled=False,)
    label_imp = widgets.Label('Ionic model')
    generated_widgets['imp'] =widgets.HBox([widget_imp, label_imp])
    
    widget_params = widgets.Text(placeholder='Type something',
            description='params',
            disabled=False
        )
    label_params = widgets.Label('Ionic model parameters')
    generated_widgets['params'] =widgets.HBox([widget_params, label_params])
    
    return generated_widgets


# %%
ionic_argument = "imp"
ionic_param = "params"

