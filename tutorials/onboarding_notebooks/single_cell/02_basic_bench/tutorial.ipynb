{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "873f9da2-1182-4e33-9b69-e098279a30f6",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "source": [
    "---\n",
    "title: Single Cell Tutorial\n",
    "subtitle: Basic functionality II\n",
    "license: CC-BY-4.0\n",
    "subject: Tutorial\n",
    "venue: openCARP onboarding\n",
    "authors:\n",
    "  - name: Gunnar Seemann\n",
    "    email: gunnar.seemann@kit.edu\n",
    "    corresponding: true\n",
    "    orcid: 0000-0001-7111-7992\n",
    "    affiliations:\n",
    "      - openCARP project\n",
    "      - CARPe-diem project\n",
    "date: 2024/03/06\n",
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "27a01664-d776-457f-b787-aadf45fd8967",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "source": [
    "## Introduction\n",
    "\n",
    ":::{admonition} New to openCARP JupyterLab?\n",
    ":class: note\n",
    "If you have not started at the [first tutorial](../01_basic_bench/tutorial.ipynb), please do so and continue one after the other.\n",
    ":::\n",
    "\n",
    ":::{admonition} Is there something happening that you can't handle while executing cells?\n",
    ":class: note\n",
    "You will get help in the [first tutorial](../01_basic_bench/tutorial.ipynb).\n",
    ":::\n",
    "\n",
    "## Learning Goals\n",
    "This second tutorial should familiarize you with futher basic functionality of the ionic cell model program ```bench```. You will learn to\n",
    "- change simulation duration, integration (time step), and allow several stimulations\n",
    "- adjust what data will be written to the output file\n",
    "- adapt stimulation current amplitude and duration\n",
    "- define a stimulation time sequence\n",
    "- adjust the basic cycle length (BCL), the rate of stimulation\n",
    "\n",
    "## Importing python libraries and visualize command line options\n",
    "🛠 Please execute the following cell in this notebook. It will also show you the command line options. 🛠\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e68baff6-6ce9-464f-b0da-e6ba7a273ea2",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "outputs": [],
   "source": [
    "import ipywidgets as widgets\n",
    "import subprocess\n",
    "import pandas as pd\n",
    "from random import randint\n",
    "import matplotlib.pyplot as plt\n",
    "from jupyterquiz import display_quiz\n",
    "\n",
    "!bench --help"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "53a8530c-b30b-49f0-82cf-3bcfbc4f6b15",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "source": [
    "## Simulation duration, time step, and multiple stimulations\n",
    "\n",
    "Now, you are learning several parameters for ```bench``` in each section. In this part we start with the length of the simulation (duration), how to stimulate more than once, and what effect you will see from changing the temporal discretization (time step, dt). Please first try to find the corresponding command line options in the ```bench --help``` output above. If you have troble finding them, the execution of the next cell will give you support.\n",
    "\n",
    "🛠 OK, please execute bench using the Courtemanche model with a duration of 5s, a time step of 2 ms (and also 3 ms) and 4 stimulations. You have to scroll quite a bit to see the figure... 🛠"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "84b4b6aa-6443-4d6b-b910-0b46cc8b4625",
   "metadata": {
    "editable": true,
    "scrolled": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "outputs": [],
   "source": [
    "display_quiz(\"help/q01.json\", shuffle_answers=False, max_width=1000) # leave this line unchanged\n",
    "\n",
    "\n",
    "\n",
    "data = pd.read_csv(\"Trace_0.dat\", header=None, sep=\"\\t\")\n",
    "plt.xlabel('time (ms)')\n",
    "plt.ylabel('Vm (mV)')\n",
    "plt.plot(data[0], data[16])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3ae4658f-9e8c-4b4b-aa30-0fefd1d9656b",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "source": [
    ":::{admonition} This is the result that you should see\n",
    ":class: note dropdown\n",
    "```{image} help/Image1.png\n",
    ":width: 400px\n",
    "```\n",
    ":::\n",
    "\n",
    "The standard time step is 0.01. You could run the last simulation with different dt and analyze the output at the end of the simulation. Most interesting here is \"main loop time\" and more specifically \"total ode time\". You save a lot of time with a larger time step but the accuracy will go down. Especially the sodium channel and calcium handling are sensitve to larger time steps.\n",
    "\n",
    "## Setting output file parameters\n",
    "\n",
    "Here, we want to show you how to adjust the output (file and on screen) regarding the starting time of outputting and the sampling (Find the options in the ```bench --help``` output above). \n",
    "\n",
    "🛠 Please execute bench using the Courtemanche model with a duration of 4.5s and 5 stimulations. Please add the time where the output should start to 3.9 s and the time step for the output to 25 ms. 🛠"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "109653ab-cfbf-4309-aaeb-caa738f632a8",
   "metadata": {
    "editable": true,
    "scrolled": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "outputs": [],
   "source": [
    "display_quiz(\"help/q02.json\", shuffle_answers=False, max_width=1000) # leave this line unchanged\n",
    "\n",
    "\n",
    "\n",
    "data = pd.read_csv(\"Trace_0.dat\", header=None, sep=\"\\t\")\n",
    "plt.xlabel('time (ms)')\n",
    "plt.ylabel('Vm (mV)')\n",
    "plt.plot(data[0], data[16])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "76f0791b-5a5b-43b0-8116-e45b8421e165",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "source": [
    ":::{admonition} This is the result that you should see\n",
    ":class: note dropdown\n",
    "```{image} help/Image2.png\n",
    ":width: 400px\n",
    "```\n",
    ":::\n",
    "\n",
    "You now see that you have a lot less entries in the output cell of jupyter. This is because you start later writing the data and you have lower temporal information that is also seen in the coarser discretization of the graph. You miss the overshoot of the action potential, because you just don't have the sample point at this time.\n",
    "\n",
    "## Stimulation amplitude and duration\n",
    "\n",
    "The threshold of cell activation was in all simulations so far reached. Now, you will adjust stimulus duration and current amplitude so that you can find this threshold. The combination of both are important. If you have a large amplitude with a short duration,  or a long duration with a low amplitude, you will not be able to activate the myocyte (Find the options in the ```bench --help``` output above).\n",
    "\n",
    "🛠 Please execute bench using the Courtemanche model with a simulation duration of 400 ms, with a stimulus duration of 0.5 ms and a current amplitude of 30 pA/pF. You should see sub-threashold activation only. Please increase both parameters (first the one than the other) in order to find the threashold for each. 🛠"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "eb92bae2-1ae7-434c-82eb-9d9b8549fbe1",
   "metadata": {
    "editable": true,
    "scrolled": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "outputs": [],
   "source": [
    "display_quiz(\"help/q03.json\", shuffle_answers=False, max_width=1000) # leave this line unchanged\n",
    "\n",
    "\n",
    "data = pd.read_csv(\"Trace_0.dat\", header=None, sep=\"\\t\")\n",
    "plt.xlabel('time (ms)')\n",
    "plt.ylabel('Vm (mV)')\n",
    "plt.plot(data[0], data[16])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "624a4d0a-e8bb-4c8c-989a-f98c60c72934",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "source": [
    ":::{admonition} This is the result that you should see\n",
    ":class: note dropdown\n",
    "```{image} help/Image3.png\n",
    ":width: 400px\n",
    "```\n",
    ":::\n",
    "\n",
    "## Irregularly timed stimuli\n",
    "\n",
    "Let us now place the timing of stimuli irregularly (Find the options in the ```bench --help``` output above).\n",
    "\n",
    "🛠 Please execute bench using the Courtemanche model with a duration of 2.2s and stimulations at 0 ms, 600 ms, 1100 ms, 1500 ms, and 1800 ms. 🛠"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e0c74e47-ab15-429f-b297-658019fadc48",
   "metadata": {
    "editable": true,
    "scrolled": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "outputs": [],
   "source": [
    "display_quiz(\"help/q04.json\", shuffle_answers=False, max_width=1000) # leave this line unchanged\n",
    "\n",
    "\n",
    "\n",
    "data = pd.read_csv(\"Trace_0.dat\", header=None, sep=\"\\t\")\n",
    "plt.xlabel('time (ms)')\n",
    "plt.ylabel('Vm (mV)')\n",
    "plt.plot(data[0], data[16])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d0430960-f864-48e0-9eda-79705b87ac07",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "source": [
    ":::{admonition} This is the result that you should see\n",
    ":class: note dropdown\n",
    "```{image} help/Image4.png\n",
    ":width: 400px\n",
    "```\n",
    ":::\n",
    "\n",
    "You can change the timing (especially make it shorter) to identify when cells can't get activated anymore because of the refractory period (sodium channels are not yet back in their inactivated state).\n",
    "\n",
    "## Basic Cycle Length\n",
    "\n",
    "Often, it is useful to stimulate a cell with a constant rate (basic cycle length - BCL) for a longer time so that the cell can adjust to the different rate (Normally, initial states for the cells are adjusted to a BCL of 1000 ms). In this case it is not useful to write all stimulation times in a sequence like above (Find the options in the ```bench --help``` output above).\n",
    "\n",
    "🛠 Please execute bench using the Courtemanche model with a duration of 5.2s, a BCL of 600 ms, 20 stimulations, and the time where the output should start to 4 s. You have used most of the command line options above. 🛠"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "27cd744a-079b-4f6b-8526-aabb61b73383",
   "metadata": {
    "editable": true,
    "scrolled": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "outputs": [],
   "source": [
    "display_quiz(\"help/q05.json\", shuffle_answers=False, max_width=1000) # leave this line unchanged\n",
    "\n",
    "\n",
    "\n",
    "data = pd.read_csv(\"Trace_0.dat\", header=None, sep=\"\\t\")\n",
    "plt.xlabel('time (ms)')\n",
    "plt.ylabel('Vm (mV)')\n",
    "plt.plot(data[0], data[16])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fc1eab8d-22bd-4005-9b6e-bc0686e441a7",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "source": [
    ":::{admonition} This is the result that you should see\n",
    ":class: note dropdown\n",
    "```{image} help/Image5.png\n",
    ":width: 400px\n",
    "```\n",
    ":::\n",
    "\n",
    "🛠 Now we want to add another graph with a BCL of 400 but all other options the same. Please remind that you should add ```--trace-no=1``` so that the result is written into a different file. 🛠"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "301975f7-122b-4d86-bfe5-8eb082c5e4f8",
   "metadata": {
    "editable": true,
    "scrolled": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "outputs": [],
   "source": [
    "display_quiz(\"help/q06.json\", shuffle_answers=False, max_width=1000) # leave this line unchanged\n",
    "\n",
    "\n",
    "\n",
    "data2 = pd.read_csv(\"Trace_1.dat\", header=None, sep=\"\\t\")\n",
    "plt.xlabel('time (ms)')\n",
    "plt.ylabel('Vm (mV)')\n",
    "plt.plot(data[0],data[16], label='first simulation')\n",
    "plt.plot(data[0],data2[16], label='second simulation')\n",
    "plt.legend(loc='upper right')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a813229a-706b-4411-9ade-15ae7aec35ad",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "source": [
    ":::{admonition} This is the result that you should see\n",
    ":class: note dropdown\n",
    "```{image} help/Image_result.png\n",
    ":width: 400px\n",
    "```\n",
    ":::\n",
    "\n",
    "The action potential duration (APD) of the faster paced cell is shorter. This is caused mainly due to the fact that repolarizing ion channels are not yet completely closed again and thus repolarize the cell from the beginning of the action potential.\n",
    "\n",
    "If you want to play around with the parameters, go back, change parameters, execute the corresponding cells, and execute the visualization cells again.\n",
    "\n",
    "## What's next\n",
    "\n",
    "In the [next tutorial](../03_advanced_bench/tutorial.ipynb), you will learn more about how to save the state of a cell model at the end of a simulation and restart ```bench``` with that state. Additonally, you will learn how to include a plugin (e.g. a further ionic channel or a tension model) and how to adjust it's parameters."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3183ff2d-737a-4ab2-9b77-9e8f5103f051",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "source": [
    "::::{grid} 1 2 2 3\n",
    ":gutter: 1 1 1 2\n",
    "\n",
    ":::{grid-item-card}\n",
    ":header: ⬅️ Basic Single Cell Tutorial I\n",
    ":link: ../01_basic_bench/tutorial.ipynb\n",
    ":link-type: ref\n",
    "Available commands and models, running standard model and varying parameters\n",
    ":::\n",
    "\n",
    ":::{grid-item-card}\n",
    ":header: 🏡 Overview Page\n",
    ":link: ../../index.ipynb\n",
    ":link-type: ref\n",
    "Go back to the overview page of the tutorials\n",
    ":::\n",
    "\n",
    ":::{grid-item-card}\n",
    ":header: Advanced Single Cell Tutorial I ➡️\n",
    ":link: ../03_advanced_bench/tutorial.ipynb\n",
    ":link-type: ref\n",
    "Loading/saving states, loading plugins, and adapting plugin parameters\n",
    ":::\n",
    "\n",
    "::::\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
