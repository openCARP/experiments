#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the 
# Apache License, Version 2.0 (the "License"); 
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#

#!/usr/bin/env python

"""
Runs regression tests on the Niederer N-version benchmark
"""

import os

from carputils import tools
from carputils import mesh
from carputils import testing

EXAMPLE_DESCRIPTIVE_NAME = 'N-version benchmark of cardiac tissue electrophysiology'
EXAMPLE_AUTHOR = 'Tobias Gerach <tobias.gerach@kit.edu>'
EXAMPLE_DIR = os.path.dirname(__file__)

def parser():
    parser = tools.standard_parser()
    benchmark = parser.add_argument_group('N-version benchmark specific options')
    benchmark.add_argument('--tend',
                        type=float, default=60.0,
                        help='duration of the simulation in ms')
    benchmark.add_argument('--dx',
                        type=float, default=500.0,
                        help='resolution of the mesh in um to be used for '
                            +'benchmark')
    benchmark.add_argument('--dt',
                        type=float, default=20.0,
                        help = 'temporal resolution in us')
    benchmark.add_argument('--massLumping',
                        type=int, default=0,
                        choices=[0, 1],
                        help='toggle mass massLumping on (1) or use full mass matrix (0)')
    benchmark.add_argument('--solver',
                        type=int, default=1,
                        choices=[0, 1, 2],
                        help='toggle mass massLumping on (1) or use full mass matrix (0)')
    
    return parser

def jobID(args):
    """
    Generate name of top level output directory.
    """
    return 'benchmark_{}us_{}um_{}ms_lumping{}_np{}_solver{}'.format(args.dt, args.dx, args.tend, args.massLumping, args.np, args.solver)

@tools.carpexample(parser, jobID)
def run(args, job):

    # Generate mesh
    # Block according to the benchmark problem. P1 corner set to origin
    geom = mesh.Block(size=(20, 7, 3), resolution=args.dx/1000, centre=(10, 3.5, 1.5))
    # Set fibre angle to 0, sheet angle to 0
    geom.set_fibres(0, 0, 90, 90)

    # Generate and return base name
    meshname = mesh.generate(geom)

    # Add all the non-general arguments
    cmd = tools.carp_cmd(os.path.join(EXAMPLE_DIR, 'nversion.par'))
    cmd += ['-imp_region[0].im_sv_init', os.path.join(EXAMPLE_DIR, 'singlecell.sv')]
    cmd += [
        '-simID',           job.ID,
        '-meshname',        meshname,
        '-tend',            args.tend,
        '-dt',              args.dt,
        '-mass_lumping',    args.massLumping,
        '-parab_solve',     args.solver
    ]

    # define physics regions
    # ------------------------------------------------------------------------------
    IntraTags = [1]
    ExtraTags = [1]

    cmd += tools.gen_physics_opts(ExtraTags=ExtraTags, IntraTags=IntraTags)

    # Run simulation
    job.mpi(cmd)

# Define some tests
desc = ('Run Niederer et al. benchmark and compare against reference solution of activation times ')
test_fullMass = testing.Test('fullMass', run, description=desc + 'using full mass matrix',
                             tags=[testing.tag.FAST, testing.tag.SERIAL])
test_fullMass.add_filecmp_check('init_acts_vm_act-thresh.dat', testing.max_error, 1e-6)

test_lumpedMass = testing.Test('lumpedMass', 
                                run, ['--tend', '150', '--massLumping', '1'],
                                description=desc + 'using lumped mass matrix',
                                tags=[testing.tag.FAST, testing.tag.SERIAL])
test_lumpedMass.add_filecmp_check('init_acts_vm_act-thresh.dat', testing.max_error, 1e-6)

test_explicit = testing.Test('explicit', 
                             run, ['--solver', '0'],
                             description=desc + 'using explicit time integration',
                             tags=[testing.tag.FAST, testing.tag.SERIAL])
test_explicit.add_filecmp_check('init_acts_vm_act-thresh.dat', testing.max_error, 1e-6)

test_o2dt = testing.Test('O2dT', 
                        run, ['--solver', '2'],
                        description=desc + 'using 2nd order time integration',
                        tags=[testing.tag.FAST, testing.tag.SERIAL])
test_o2dt.add_filecmp_check('init_acts_vm_act-thresh.dat', testing.max_error, 1e-6)

__tests__ = [test_fullMass, test_lumpedMass, test_explicit, test_o2dt]

if __name__ == '__main__':
    run()    