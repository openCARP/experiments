# openCARP Developer Tests

This git repository contains developer tests for openCARP.
Running these tests requires the *carputils* Python package;
please see the documentation [there](http://git.opencarp.org/openCARP/carputils) for installation and usage instructions.

Instructions on how to run these tests using [autotester](https://autotester.readthedocs.io/en/latest/) are detailed 
[here](https://git.opencarp.org/openCARP/experiments/-/blob/master/TESTS.md).


