#!/usr/bin/env python

"""
This example tests if our LIMPET models (except for 'IION_SRC' and
'UCLA_RAB_SCR') have received 'additional features' since their implementation.

Usage
=====

Following optional arguments with their default values are available:

.. code-block:: bash

    ./run.py --model=MBRDR
    ./run.py --list
"""

from __future__ import print_function
import os
import numpy as np
from datetime import date

from carputils import settings
from carputils import bench
from carputils import testing
from carputils import tools
from carputils import divertoutput

EXAMPLE_DESCRIPTIVE_NAME = 'LIMPET Plugins'
EXAMPLE_AUTHOR = ('Anton Prassl <anton.prassl@medunigraz.at> and '
                  'Andrew Crozier <andrew.crozier@medunigraz.at>')
EXAMPLE_DIR = os.path.dirname(__file__)

AXIS_LABEL = {'Tension': 'Tension (kPa)'}

def parser():
    parser = tools.standard_parser()
    group  = parser.add_argument_group('experiment specific options')

    group.add_argument('--model',
                        default='LuoRudy91',
                        help='select IMP model')
    group.add_argument('--plugin',
                        default='Stress_Land17',
                        help='select LIMPET plugin')
    group.add_argument('--param', '-p',
                        nargs=2, action='append',
                        help='assign model parameter values')
    group.add_argument('--list',
                        action='store_true',
                        help='list available LIMPET plugins')
    group.add_argument('--list-params',
                        action='store_true',
                        help='list available parameters for this plugin')
    return parser

def jobID(args):
    """
    Generate name of top level output directory.
    """
    today = date.today()
    tpl = '{}_bench_{}-{}_{}_np{}'
    return tpl.format(today.isoformat(), args.model, args.plugin, args.flv, args.np)


def read_sv_header(filename):
    """
    Reads header provided by LIMPET/bench and extracts information about
    the binary files to read and create the HDF5 repository.

    Args:
        filename: the name of the header file to read.

    Returns:
        list of state vectors
    """

    fh      = open(filename, 'rt')
    line    = fh.readline() # don't care about endianess
    sv_list = []
    for line in fh:
      fields = line.strip().split()
      sv     = fields[0].split('.')
      while sv[-1] == 'bin' or sv[-1] == 'dummy':
          del sv[-1]
      sv_list.append(sv[-1])
    fh.close()

    return sv_list

# statevector files export to hdf5
def bin2h5(args, job):
    try:
        # limpetgui visualization needs a time vector which is only then created
        # when the SV variables are "whitelisted".
        sv_lst = read_sv_header('{}_header.txt'.format(os.path.join(job.ID, args.plugin)))
        sv_lst_str = ' '.join(str(sv) for sv in sv_lst)

        # create hdf5 file including time vector
        cmd = [settings.execs.SV2H5B,
              '{}_header.txt'.format(os.path.join(job.ID, args.plugin)),
              '{}.h5'.format(        os.path.join(job.ID, args.plugin)),
              '--whitelist', sv_lst_str]
        job.carp(cmd, 'Converting to state vector files to HDF5...')
    except:
        if not args.silent:
            print('Attempt to HDF5 conversion failed.')
    return

@tools.carpexample(parser, jobID)
def run(args, job):

    if args.list:
        print('Plugins:')
        for plugin in bench.plugins().keys():
            print('    {}'.format(plugin))
        return

    if args.list_params:
        cmd = [settings.execs.BENCH,
               '--imp={}'.format(args.plugin),
               '--imp-info']
        divertoutput.call(cmd)
        return

    # run bench with available ionic models
    cmd  = [settings.execs.BENCH,
            '--imp={}'.format(args.model),
            '--plug-in={}'.format(args.plugin)]

    if args.param is not None:
        pstr = ','.join(['{}={}'.format(p,v) for p,v in args.param])
        cmd += ['--plug-par={}'.format(pstr)]

    # Output options
    cmd += ['--fout={}'.format(os.path.join(job.ID, args.plugin)),
            '--bin',
            '--no-trace',
            '--validate',
            '--duration', 1000]
    job.mpi(cmd, 'bench: Plugin {}'.format(args.plugin))

    # export statevector files to hdf5
    bin2h5(args, job)

    # Do visualization
    if args.visualize and not settings.platform.BATCH:

        # Determine data field
        fields = bench.plugins()[args.plugin]
        for fieldname in ['Tension', 'Iion']:
            if fieldname in fields:
                break
        else:
            # Priority matches did not work, select first
            fieldname = fields[0]

        # Load data
        dt   = np.dtype('<d') # little-endian
        tpl  = os.path.join(job.ID, '{}.{{}}.bin'.format(args.plugin))
        t    = np.fromfile(tpl.format('t'))
        data = np.fromfile(tpl.format(fieldname))

        # Plot data
        from matplotlib import pyplot
        pyplot.plot(t, data)
        pyplot.xlabel('Time (ms)')
        pyplot.ylabel(AXIS_LABEL.get(fieldname, fieldname))
        pyplot.show()


# Define some tests

# Get list of ionic models
settings.cli = tools.standard_parser().parse_args(['--silent'])
plugins = bench.plugins()

__tests__ = []

for name, fields in plugins.items():

    desc = ('Compute {} trace(s) for {} plugin and compare '
            'against reference.'.format(' ,'.join(fields), name))

    test = testing.Test(name, run,
                        ['--plugin', name],
                        description=desc,
                        tags=[testing.tag.FAST, testing.tag.SERIAL])

    # Compare all traces
    for field in fields:
        fname = '{}.{}.bin'.format(name, field)
        test.add_filecmp_check(fname, testing.max_error, 0.001)

    __tests__.append(test)

if __name__ == '__main__':
    run()
