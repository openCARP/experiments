#!/usr/bin/env python

"""
This example tests if our LIMPET models (except for 'IION_SRC' and
'UCLA_RAB_SCR') have received 'additional features' since their implementation.

Usage
=====

Following optional arguments with their default values are available:

.. code-block:: bash

    ./run.py --model=MBRDR
    ./run.py --list
"""
import os
EXAMPLE_DESCRIPTIVE_NAME = 'Isolated Ionic Model'
EXAMPLE_AUTHOR = 'Anton Prassl <anton.prassl@medunigraz.at>'
EXAMPLE_DIR = os.path.dirname(__file__)

import numpy as np
from datetime import date

from carputils import settings
from carputils import bench
from carputils import testing
from carputils import tools


MODEL_EXCLUDE_LIST = ['IION_SRC', 'Campos']

def parser():
    parser = tools.standard_parser()
    parser.add_argument('--model',
                        default='DrouhardRoberge',
                        help='select IMP model')
    parser.add_argument('--list',
                        action='store_true',
                        help='list available LIMPET models')
    return parser

def jobID(args):
    """
    Generate name of top level output directory.
    """
    today = date.today()
    tpl = '{}_bench_{}_np{}'
    return tpl.format(today.isoformat(), args.model, args.np)

def read_sv_header(filename):
    """
    Reads header provided by LIMPET/bench and extracts information about
    the binary files to read and create the HDF5 repository.

    Args:
        filename: the name of the header file to read.

    Returns:
        list of state vectors
    """

    fh      = open(filename, 'rt')
    line    = fh.readline() # don't care about endianess
    sv_list = []
    for line in fh:
      fields = line.strip().split()
      sv     = fields[0].split('.')
      while sv[-1] == 'bin' or sv[-1] == 'dummy':
          del sv[-1]
      sv_list.append(sv[-1])
    fh.close()

    return sv_list


# statevector files export to hdf5
def bin2h5(args, job):
    try:
        # limpetgui visualization needs a time vector which is only then created
        # when the SV variables are "whitelisted".
        sv_lst = read_sv_header('{}_header.txt'.format(os.path.join(job.ID, args.model)))
        sv_lst_str = ' '.join(str(sv) for sv in sv_lst)

        # create hdf5 file including time vector
        cmd = [settings.execs.SV2H5B,
              '{}_header.txt'.format(os.path.join(job.ID, args.model)),
              '{}.h5'.format(        os.path.join(job.ID, args.model)),
              '--whitelist', sv_lst_str]
        job.carp(cmd, 'Converting to state vector files to HDF5...')
    except:
        if not args.silent:
            print('Attempt to HDF5 conversion failed.')
    return


@tools.carpexample(parser, jobID)
def run(args, job):

    if args.list:
        print('Ionic models:')
        for imp in bench.imps():
            print('    {}'.format(imp))
        return

    # run bench with available ionic models
    cmd  = [settings.execs.BENCH,
            '--imp={}'.format(args.model)]

    # Output options
    cmd += ['--fout={}'.format(os.path.join(job.ID, args.model)),
            '--bin',
            '--no-trace',
            '--validate',
            '--duration', 1000]
    job.mpi(cmd, 'Testing {}'.format(args.model))

    # export statevector files to hdf5
    bin2h5(args, job)

    # Do visualization
    if args.visualize and not settings.platform.BATCH:

        # Load data
        dt = np.dtype('<d') # little-endian
        tpl = os.path.join(job.ID, '{}.{{}}.bin'.format(args.model))
        t  = np.fromfile(tpl.format('t'),  dtype=dt)
        Vm = np.fromfile(tpl.format('Vm'), dtype=dt)

        # Plot data
        from matplotlib import pyplot
        pyplot.plot(t, Vm)
        pyplot.xlabel('Time (ms)')
        pyplot.ylabel('Transmembrane voltage (mV)')
        pyplot.show()


# Define some tests

# Get list of ionic models
settings.cli = tools.standard_parser().parse_args(['--silent'])
imps = bench.imps()

__tests__ = []

for imp in imps:

    if imp in MODEL_EXCLUDE_LIST:
        continue

    desc = ('Compute :math:`V_m` trace for {} model and compare '
            'against reference.'.format(imp))

    test = testing.Test(imp, run,
                        ['--model', imp],
                        description=desc,
                        tags=[testing.tag.FAST, testing.tag.SERIAL])

    test.add_filecmp_check('{}.Vm.bin'.format(imp), testing.max_error, 0.001)

    __tests__.append(test)

if __name__ == '__main__':
    run()
