#!/usr/bin/env python

"""

"""
import os
EXAMPLE_DESCRIPTIVE_NAME = 'Total Current Check'
EXAMPLE_AUTHOR = 'Gernot Plank <gernot.plank@medunigraz.at>, ' \
                 'Anton J Prassl <anton.prassl@medunigraz.at>'
EXAMPLE_DIR = os.path.dirname(__file__)

from datetime import date
from carputils.carpio import igb, txt
from carputils import settings
from carputils import testing
from carputils import tools
from carputils import mesh

import matplotlib.pyplot as plt
import numpy as np

def parser():
    parser = tools.standard_parser()
    group  = parser.add_argument_group('experiment specific options')

    group.add_argument('--strength',
                       type = float, default = 1000,
                       help = 'Choose stimulus strength (uA)')
    group.add_argument('--tend',
                       type = float, default = 50,
                       help = 'Duration of experiment')
    return parser


def define_geom(args):
    # ---DEFINE AND GENERATE MESH----------------------------------------------
    # size of wedge in mm
    resolution = 0.1
    wedge_x = 10
    wedge_y = 1
    wedge_z = resolution

    geom = mesh.Block(size=(wedge_x, wedge_y, wedge_z), resolution=resolution)

    # set fibre angle to 0, sheet angle to 0
    geom.set_fibres(0, 0, 90, 90)

    # add regions
    tag = 10
    reg = mesh.BoxRegion((-wedge_x / 2. + 1 * resolution, -wedge_y / 2 + resolution, -resolution),
                         (wedge_x / 2. - 1 * resolution, wedge_y / 2 - resolution, +resolution),
                         tag=tag, bath=1)
    geom.add_region(reg)

    # put the corner of the tissue block at the origin
    geom.corner_at_origin()

    # generate and return basename
    meshname = mesh.generate(geom)

    # query for element labels
    _, etags, _ = txt.read(meshname + '.elem')
    etags = np.unique(etags)

    return meshname, etags, geom


def jobID(args):
    """
    Generate name of top level output directory.
    """
    today = date.today()
    return '{}_strength_{}uA_{}_np{}'.format(today.isoformat(), args.strength,
                                       args.flv, args.np)

@tools.carpexample(parser, jobID)
def run(args, job):

    # define geometry
    meshname, etags, geom = define_geom(args)

    # extracellular and intracellular tags
    ExtraTags = etags.tolist().copy()
    IntraTags = [ ExtraTags[0] ]       # make it a list

    # define electrode locations
    lstim = mesh.block_bc_opencarp(geom, 'stim', 0, 'x', lower=True, verbose=not args.silent)
    rstim = mesh.block_bc_opencarp(geom, 'stim', 1, 'x', lower=False, verbose=not args.silent)
    del geom

    # openCARP command arguments
    cmd  = tools.carp_cmd()
    cmd += ['-meshname',    meshname,
            '-simID',       job.ID,
            '-tend',        args.tend,
            '-dt',         50,
            '-bidomain',    1,
            '-spacedt',     0.1,
            '-timedt',      0.1,
            '-parab_solve', 1]

    # add physics (register extracellular/intracellular domains)
    cmd += tools.gen_physics_opts(ExtraTags=ExtraTags, IntraTags=IntraTags)

    # further specify electrical setup
    cmd += ['-num_stim',                        2]

    lstim += ['-stim[0].name',                  'LEFT']
    lstim += ['-stim[0].crct.type',             1] # extracellular current
    lstim += ['-stim[0].pulse.strength',        1000] # uA -> 1mA
    lstim += ['-stim[0].ptcl.duration',         1]
    lstim += ['-stim[0].ptcl.start',            0]
    lstim += ['-stim[0].ptcl.npls',             1]
    lstim += ['-stim[0].crct.total_current',    1] # treat strength as total current
                                               # and not current density

    rstim += ['-stim[1].name',                  'RIGHT']
#    rstim += ['-stim[1].crct.type',      1] # extracellular current
    rstim += ['-stim[1].crct.type',             3]  # extracellular ground
#    rstim += ['-stim[1].balance',       0] # balance with electrode 0
#    rstim += ['-stim[1].total_current', 1] # treat strength as total current
                                               # and not current density

    # finally add stimuli
    cmd += lstim + rstim

    # ionic setup
    cmd += ['-num_imp_regions', 1]
    cmd += ['-imp_region[0].im', 'PASSIVE']

    if args.visualize:
        cmd += ['-gridout_i', 3,
                '-gridout_e', 3]
    
    # Run simulations
    job.carp(cmd, 'Total Current Test')

    # Do visualization
    if args.visualize and not settings.platform.BATCH:

        geom = os.path.join(job.ID, os.path.basename(meshname)+'_i')
        data = os.path.join(job.ID, 'vm.igb')
        view = os.path.join(EXAMPLE_DIR, 'view.mshz')
        #job.meshalyzer(geom, data, view)

        geom = os.path.join(job.ID, os.path.basename(meshname)+'_e')
        data = os.path.join(job.ID, 'phie.igb')
        view = os.path.join(EXAMPLE_DIR, 'view.mshz')
        job.meshalyzer(geom, data, view)

        # plot data along first row of nodes on x-axis
        pts, npts = txt.read(geom + '.pts')
        num_x_pts = len(np.unique(pts[:,0]))
        igbdata, igbheader, t = igb.read(data)

        trc = igbdata[0:num_x_pts,-1]
        plt.plot(np.linspace(0, num_x_pts, num_x_pts, endpoint=False), trc)
        plt.grid(True)
        plt.show()

        return

# Define some tests
#__tests__ = []

## test extracellular current stimulation
#test = testing.Test('total_I_serial', run,
#                    tags=[testing.tag.FAST, testing.tag.SERIAL])
#test.add_filecmp_check('phie.igb.gz', testing.max_error, 0.001)
#test.add_filecmp_check('vm.igb.gz',   testing.max_error, 0.001)
#__tests__.append(test)

## test extracellular current stimulation
#test = testing.Test('total_I_parallel', run, ['--np', 4],
#                    tags=[testing.tag.FAST, testing.tag.PARALLEL])
#test.add_filecmp_check('phie.igb.gz', testing.max_error, 0.001)
#test.add_filecmp_check('vm.igb.gz',   testing.max_error, 0.001)
#__tests__.append(test)


if __name__ == '__main__':
    run()
