#!/usr/bin/env python

"""
Compute activation times from bidomain simulation.
"""
import os

EXAMPLE_DESCRIPTIVE_NAME = 'Activation Time Computation'
EXAMPLE_AUTHOR = 'Gernot Plank <gernot.plank@medunigraz.at>'
EXAMPLE_DIR = os.path.dirname(__file__)

import numpy as np
from datetime import date

from carputils import settings
from carputils import tools
from carputils import mesh
from carputils import testing

class LATDetector(object):

    MEASURANDS = ['Vm', 'Phie']
    METHODS = ['thresh', 'deriv']
    EVENTS = ['activation', 'repolarisation']

    SETTINGS = {
        'Phie': {
            'activation': {
                'thresh': {
                    'threshold': 1., # use Phie > 1. mV
                    'mode':      0   # rising slope
                },
                'deriv': {
                    'threshold': -25., # dPhie/dt < -25 mV/ms
                    'mode':      1     # use negative derivative
                }
            }
        },
        'Vm': {
            'activation': {
                'thresh': {
                    'threshold': -20.,
                    'mode':      0
                },
                'deriv': {
                    'threshold': 50., # mV/ms
                    'mode':      0    # use +dV/dt > 0
                }
            },
            'repolarisation': {
                'thresh': {
                    'threshold': -65.,
                    'mode':      1     # Falling slope
                },
                'deriv': {
                    'threshold': -20., # mV/ms
                    'mode':      0     # use -dV/dt
                }
            }
        }
    }

    def __init__(self, measurand, method, event, all_lats=False):

        assert measurand in self.MEASURANDS
        assert method in self.METHODS
        assert event in self.EVENTS

        self._measurand = measurand
        self._method    = method
        self._event     = event
        self._trigger_all = all_lats

    def bidomain(self):
        if self._measurand == 'Vm':
            return 0
        elif self._measurand == 'Phie':
            return 2 # use pseudo bidomain to get Phie data

    def _settings(self):

        try:
            measurand = self.SETTINGS[self._measurand]
        except KeyError:
            raise Exception('Invalid measurand {}'.format(self._measurand))

        try:
            event = measurand[self._event]
        except KeyError:
            tpl = 'Event {} not supported with measurand {}'
            raise Exception(tpl.format(self._event, self._measurand))

        try:
            method = event[self._method]
        except KeyError:
            tpl = 'Method {} not supported with event {} and measurand {}'
            raise Exception(tpl.format(self._method, self._event,
                                       self._measurand))

        return method

    def threshold(self):
        return self._settings()['threshold']

    def mode(self):
        return self._settings()['mode']

    def tend(self):
        if self._event == 'activation':
            return 30.
        elif self._event == 'repolarisation':
            return 400. # Give more time to allow repol everywhere

    def slug(self):
        tpl = '{}_{}_{}_{}_{:.2f}mV'
        return tpl.format(self._event,
                          'all' if self._trigger_all else 'first',
                          self._method,
                          'pos' if self.mode() else 'neg',
                          self.threshold())

    def opts(self):
        opts = ['-num_LATs', 1,
                '-lats[0].measurand', 1 if self._measurand == 'Phie' else 0,
                '-lats[0].method',    2 if self._method == 'deriv' else 1,
                '-lats[0].all',       int(self._trigger_all),
                '-lats[0].threshold', self.threshold(),
                '-lats[0].mode',      self.mode(),
                '-lats[0].ID',        self._event]
        return opts

    def act_filename(self):
        fname = ''
        if not self._trigger_all:
            fname += 'init_acts_' + self._event
        else:
            fname += 'activation'
        fname += '-' + self._method + '.dat'
        return fname

    def output(self, job, meshfilebase, outfilebase):
        # make sure outfilebase does not have an extension
        if ('.dat' or '.igb') in outfilebase:
            outfilebase,_ = os.path.splitext(outfilebase)

        # create dummy lats file with correct length
        # *_i.pts is needed
        with open(meshfilebase + '.pts') as fp:
            num_nodes = int(fp.readline())
        data_sorted = np.ones(num_nodes) * -1

        # read lats data
        lats_inds = np.loadtxt(os.path.join(job.ID, self.act_filename()), usecols=[0], dtype=int)
        lats      = np.loadtxt(os.path.join(job.ID, self.act_filename()), usecols=[1])

        # ---------------------------------------------------------------------
        # resort activations, keeping all activation instances
        ind       = np.lexsort((lats, lats_inds))
        lats      = lats[ind]
        lats_inds = lats_inds[ind]
        del ind

        # create dummy array
        bin_cnt         = np.bincount(lats_inds)
        bin_sorted      = np.ones((num_nodes,bin_cnt.max())) * -1
        max_activations = bin_cnt.max()

        # find out
        for num_activations in np.arange(max_activations)+1:
            active_nodes = np.asarray(np.where(bin_cnt == num_activations))
            active_nodes = active_nodes[0]
            # determine which lats_inds are currently active - boolean mask
            inds_mask    = np.in1d(lats_inds, active_nodes)
            lats_active  = lats[inds_mask]

            # reshape lats
            lats_active  = lats_active.reshape(active_nodes.size,num_activations)
            for i in np.arange(num_activations):
                bin_sorted[active_nodes,i] = lats_active[:,i]

        # export sorted LATs
        # as dat file
        for i in np.arange(num_activations):
            np.savetxt('{}_{}.dat'.format(outfilebase, i), bin_sorted[:,i],
                       fmt='%f', delimiter=' ', newline='\n')

        # as igb file
        # sorry, not yet implemented
        return

def parser():
    parser = tools.standard_parser()

    parser.add_argument('--measurand', 
                        default='Vm', choices=LATDetector.MEASURANDS,
                        help='pick measurand from which to compute LATs')
    parser.add_argument('--method', 
                        default='thresh', choices=LATDetector.METHODS,
                        help='pick LAT determination method: threshold '
                            +'crossing or max/min derivative')
    parser.add_argument('--all',
                        action='store_true',
                        help='detect all LATs instead of just first')
    parser.add_argument('--event', 
                        default='activation', choices=LATDetector.EVENTS,
                        help='choose event')
    return parser


def jobID(args):
    """
    Generate name of top level output directory.
    """
    today = date.today()
    lat = LATDetector(args.measurand, args.method, args.event, args.all)
    return '{}_lats_{}_{}_np{}'.format(today.isoformat(), lat.slug(), args.flv,
                                       args.np)

@tools.carpexample(parser, jobID)
def run(args, job):

    # Generate mesh
    # Block which is thin in z direction
    geom = mesh.Block(size=(5, 5, 0.1))
    # Set fibre angle to 0, sheet angle to 0
    geom.set_fibres(0, 0, 90, 90)

    # Generate and return base name
    meshname = mesh.generate(geom)

    # define lat settings
    #lats, latDef = define_lats(args,0)
    lat = LATDetector(args.measurand, args.method, args.event, args.all)

    # Add all the non-general arguments
    cmd = tools.carp_cmd(os.path.join(os.path.dirname(__file__), 'lats.par'))

    cmd += ['-simID',    job.ID,
            '-meshname', meshname,
            '-tend',     lat.tend(),
            '-bidomain', lat.bidomain()]
    cmd += lat.opts()

    if args.all:
        cmd += ['-imp_region[0].im_param', 'APDshorten*16',
                '-stim[0].ptcl.npls',           2, 
                '-stim[0].ptcl.bcl',            80,
                '-tend',                        98,
                '-gridout_i',                   3]

    if args.visualize:
        vis_cmd = ['-gridout_i', 3,
                   '-gridout_e', 3]
        cmd += vis_cmd

    # Run simulation 
    job.carp(cmd)


    # Need to do some postprocessing if --all was selected
    if args.all:
        # save sorted lats as dat files (one per activation)
        geom = os.path.join(job.ID, os.path.basename(meshname)+'_i')
        data = os.path.join(job.ID, 'all_lats')
        lat.output(job, geom, data)

    # Do visualization
    if args.visualize and not settings.platform.BATCH:
        geom = os.path.join(job.ID, os.path.basename(meshname)+'_i')
        data = os.path.join(job.ID, lat.act_filename())

        if args.all:
            # expecting muliple activations with names *_0.dat, *_1.dat,...
            # choosing the first
            data = os.path.join(job.ID, 'all_lats_0')

        view = os.path.join(EXAMPLE_DIR, 'lats.mshz')
        
        job.meshalyzer(geom, data, view)


# test extracellular voltage stimulation
__tests__ = []
test = testing.Test('thresh', run, ['--measurand', 'Vm',
                                    '--method', 'thresh',
                                    '--event', 'activation',
                                    '--np', '4'],
                    tags=[testing.tag.SHORT, testing.tag.PARALLEL])
test.add_filecmp_check('init_acts_activation-thresh.dat', testing.max_error, 0.001)
__tests__.append(test)


test = testing.Test('thresh_all_serial', run, ['--measurand', 'Vm',
                                               '--method', 'thresh',
                                               '--event', 'activation',
                                               '--all',
                                               '--np', '1'],
                    tags=[testing.tag.MEDIUM, testing.tag.SERIAL])
test.add_filecmp_check('all_lats_0.dat', testing.max_error, 0.001)
test.add_filecmp_check('all_lats_1.dat', testing.max_error, 0.001)
__tests__.append(test)


test = testing.Test('thresh_all_parallel', run, ['--measurand', 'Vm',
                                                 '--method', 'thresh',
                                                 '--event', 'activation',
                                                 '--all',
                                                 '--np', '4'],
                    tags=[testing.tag.MEDIUM, testing.tag.PARALLEL])
test.disable_reference_generation()
test.add_filecmp_check('all_lats_0.dat', testing.max_error, 0.001)
test.add_filecmp_check('all_lats_1.dat', testing.max_error, 0.001)
__tests__.append(test)


test = testing.Test('deriv', run, ['--measurand', 'Vm',
                                   '--method', 'deriv',
                                   '--event', 'activation',
                                   '--np', '4'],
                    tags=[testing.tag.SHORT, testing.tag.PARALLEL])
test.add_filecmp_check('init_acts_activation-deriv.dat', testing.max_error, 0.001)
__tests__.append(test)


if __name__ == '__main__':
    run()
