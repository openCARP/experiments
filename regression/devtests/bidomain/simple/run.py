#!/usr/bin/env python

"""
This is a simple bidomain example that is primarily a demonstration of the
carputils example and testing framework. In that vein, this documentation
illustrates some of the features available when writing test documentation.

Please see the :ref:`examples` and :ref:`tests` sections of the carputils
documentation for more information, in particular the subsection on
:ref:`example_documentation`.

Problem Setup
=============

This example defines a small cuboid on the domain:

.. math::

    -0.5  \leq x \leq 0.5

    -0.25 \leq y \leq 0.25

    -0.25 \leq z \leq 0.25

And applies an electrical stimulus on the :math:`x = -0.5` face. This is
illustrated below:

.. image:: /images/bidomain_simple_setup.png

Usage
=====

This example specifies just one optional argument, ``--tend``. Use it to
specify how long to simulate for after stimulus, in milliseconds:

.. code-block:: bash

    ./run.py --tend 100

As with other examples, add the ``--visualize`` option to automatically load
the results in meshalyzer:

.. code-block:: bash

    ./run.py --tend 100 --visualize
"""
import os

EXAMPLE_DESCRIPTIVE_NAME = 'Simple Bidomain Example'
EXAMPLE_AUTHOR = 'Andrew Crozier <andrew.crozier@medunigraz.at>'
EXAMPLE_DIR = os.path.dirname(__file__)

from datetime import date

from carputils import settings
from carputils import tools
from carputils import mesh
from carputils import testing

def parser():
    parser = tools.standard_parser()
    parser.add_argument('--tend',
                        type=float, default=20.,
                        help='Duration of simulation (ms)')
    return parser

def jobID(args):
    """
    Generate name of top level output directory.
    """
    today = date.today()
    return '{}_simple_{}_{}_np{}'.format(today.isoformat(), args.tend,
                                         args.flv, args.np)

@tools.carpexample(parser, jobID)
def run(args, job):

    # Generate mesh
    # Units are mm
    # Block which is thin in z direction
    geom = mesh.Block(size=(1, 0.5, 0.5))
    # Set fibre angle to 0, sheet angle to 0
    geom.set_fibres(0, 0, 90, 90)
    # Generate and return base name
    meshname = mesh.generate(geom)

    # Define the geometry of the stimulus at one end of the block
    # Units are um
    stim = mesh.block_bc_opencarp(geom, 'stim', 0, 'x', False)

    # Get basic command line, including solver options
    cmd = tools.carp_cmd(os.path.join(EXAMPLE_DIR, 'simple.par'))
    cmd += tools.gen_physics_opts(ExtraTags=[0, 1], IntraTags=[1])

    cmd += ['-simID',    job.ID,
            '-meshname', meshname,
            '-tend',     args.tend]
    cmd += stim

    if args.visualize:
        cmd += ['-gridout_i', 3]
        cmd += ['-gridout_e', 3]

    # Run simulation
    job.carp(cmd)

    # Do visualization
    if args.visualize and not settings.platform.BATCH:

        # Prepare file paths
        geom = os.path.join(job.ID, os.path.basename(meshname)+'_i')
        data = os.path.join(job.ID, 'vm.igb.gz')
        view = os.path.join(EXAMPLE_DIR, 'simple.mshz')

        # Call meshalyzer
        job.meshalyzer(geom, data, view)

# Define some tests
desc = ('Generate short activation sequence on small cuboid and compare '
        ':math:`V_m` against reference.')
test_default = testing.Test('default', run, description=desc,
                            tags=[testing.tag.FAST, testing.tag.SERIAL])
test_default.add_filecmp_check('vm.igb.gz', testing.max_error, 0.001)

desc = ('Run longer activation sequence on small cuboid and compare '
        ':math:`V_m` against reference.')
test_long = testing.Test('long', run, ['--tend', '300.'],
                         description=desc,
                         tags=[testing.tag.MEDIUM, testing.tag.SERIAL])
test_long.add_filecmp_check('vm.igb.gz', testing.max_error, 0.001)

__tests__ = [test_default, test_long]

if __name__ == '__main__':
    run()
