#!/usr/bin/env python3

"""
This tutorial introduces the concepts of region-based and gradient-based heterogeneities for
assigning spatially varying properties such as tissue conductivities and ion channel conductances.

Conceptual overview
===================

#) **Region-wise assignment of electrophysiological properties**

   In any particular finite element mesh, each element is part of a region defined by its tag
   (see :ref:`defining regions <region-definition>`). When cell- or tissue-scale properties are assigned 
   on a region-wise basis, the property in question is assigned homogeneously to all nodes and 
   elements that belong to that region. 

   One very important thing to note is that finite element nodes along the boundary between
   two different element regions will be assigned by carpentry to being in one region or the 
   other. The system responsible for deciding which boundary nodes belong to which regions is
   predictible but not necessarily intuitive. When using region-based tagging, it is important
   to remain mindful of the consequences of this process. Detailed information is available in 
   the tutorial on :ref:`Cellular Dynamics Heterogeneity <cellular-dynamics-heterogeneity>`.

#) **Gradient-wise assignment of electrophysiological properties**

   As an alternative to the above-described approach, sometimes it is useful or relevant to 
   assign properties on a node-wise basis. For example, the value of an ion channel conductance
   at various points across the myocardial wall might vary linearly between two values as a 
   function of distance from the epicardium to the endocardium. This approach utilizes the
   carpentry adjustments interface (see :ref:`adjustments <adjustments>`) and is well-suited 
   for defining smooth variations in electrophysiological properties. Tagging is a poor option
   for this type of problem, since it might require a large number of tags OR different tag-wise
   division for gradients in different properties (e.g., a transmural gradient in one conductance
   and an apicobasal gradient in another).

Problem Setup
=============

This example will run two simulations using a 2D sheet model (1 cm x 1 cm) in which GKr and GKs 
values are smaller at the bottom of the sheet (y = 0) and larger at the top of the sheet (y = 1 cm).
In the first simulation, the domain will be subdivided into four regions, each one with a distinct
scalar multiplier for GKr and GKs: 

.. figure:: /images/05A_Regions_vs_Gradients_Fig1_RegionWiseGKr+GKs.png
    :scale: 60%
    :align: center

    Region-wise assignment of properties. Tissue is subdivided into four regions each of which is assigned
    different properties.

In the second simulation, the domain will consist of one unified region but the GKr and GKs values
will be scaled continuously (via linear gradient) from the bottom to the top:

.. figure:: /images/05A_Regions_vs_Gradients_Fig2_GradientWiseGKr+GKs.png
    :scale: 60%
    :align: center
  
    Gradient-wise assignment of properties. There is only one tissue region and GKr/GKs values vary
    continuously from low (bottom) to high (top).

Usage
=============

The following optional arguments are available (default values are indicated):

.. code-block:: bash

    ./run.py --min_scf     = 0.1      # lower bound scaling factor (min = 0.000, max = 0.999)
             --max_scf     = 2.0      # upper bound scaling factor (min = 1.000, max = 10.00)

If the program is run with the ``--visualize`` option, meshalyzer will automatically load a map 
showing the difference in action potential durations between the two simulations:

.. code-block:: bash

    ./run.py --visualize

Interpreting Results
====================

With the default parameters, the program will perform the two simulations as described above.
When the vm.igb files are visualized, it will be apparent that the depolarisation patterns are
approximately the same but the repolarisation sequences are markedly different due to the 
variable representations of GKr/GKs heterogeneity. The side-by-side animation shown here is 
for the vm sequences produced by the test using the default parameters: 

.. _fig-region-vs-gradient-Vm-comparison:

.. figure:: /images/05A_Regions_vs_Gradients_Fig3_VmComparison.gif
    :scale: 100%
    :align: center

    Comparision of membrane voltage over time (:math:`V_m(t)`) for Region-wise (left) and
    Gradient-wise (right) variability in GKr/GKs.

The program will also produce APD.dat files in the relevant simulation subdirectories, which
can also be visualized in meshalyzer to appreciate the exact distributions of APD:

.. _fig-region-vs-gradient-APD-comparison:

.. figure:: /images/05A_Regions_vs_Gradients_Fig4_APDComparison.png
    :scale: 100%
    :align: center

    Comparision of action potential duration for Region-wise (left) and
    Gradient-wise (right) variability in GKr/GKs.

Notably, although the effect on repolarisation is definitely more pronounced than that on 
depolarisation, the resulting differences in APD are subtle, as shown by the maps that can
be seen using the --visualize function:

.. _fig-Region-vs-Gradient-DeltaAPD:

.. figure:: /images/05A_Regions_vs_Gradients_Fig5_DeltaAPD.png
    :scale: 100%
    :align: center

    Difference in action potential duration (APD) between region-based and gradient based 
    repolarisation heterogeneity

"""
import os, sys

EXAMPLE_DESCRIPTIVE_NAME = 'Region- vs. Gradient-Based Heterogeneities'
EXAMPLE_AUTHOR = 'Patrick Boyle <pmjboyle@gmail.com>'
EXAMPLE_DIR = os.path.dirname(__file__)

import numpy as np
from datetime import date

from carputils.carpio import txt
from carputils import settings
from carputils import tools
from carputils import mesh
from carputils import testing

isPy2 = True
if sys.version_info.major > 2:
    isPy2  = False
    xrange = range


def setupMesh(args):
    # user input
    UM2MM      = 1./1000
    slabsize   = args.slabsize   * UM2MM                    # in mm
    resolution = args.resolution * UM2MM                    # in mm
    centre     = np.asarray([slabsize/2., slabsize/2., 0.]) # in mm

    # generate mesh (units in mm) and return basename
    # lower left of the mesh will be the origin
    geom = mesh.Block(size=(slabsize, slabsize, 0.),
                      centre=centre,
                      etype='tri',
                      resolution=resolution)

    # incorporate tag regions (divide block into four regions along y-axis)
    # ^ y
    # |
    # o -> x
    #
    # ------------ (y_r3)
    # |  tag 3   |
    # ------------ (y_r2)
    # |  tag 2   |
    # ------------ (y_r1)
    # |  tag 1   |
    # ------------ (y_r0)
    # |  tag 0   |
    # ------------ (0)

    y_r0 = 1.*slabsize/4. # in mm
    y_r1 = 2.*slabsize/4. # in mm
    y_r2 = 3.*slabsize/4. # in mm
    y_r3 = 4.*slabsize/4. # in mm

    # define blocks relative to the mesh centre (mesher wants it this way):
    ll_box_r0 = np.asarray([0.,   0., 0.])-centre; ur_box_r0 = np.asarray([slabsize, y_r0, 0.])-centre # in mm
    ll_box_r1 = np.asarray([0., y_r0, 0.])-centre; ur_box_r1 = np.asarray([slabsize, y_r1, 0.])-centre # in mm
    ll_box_r2 = np.asarray([0., y_r1, 0.])-centre; ur_box_r2 = np.asarray([slabsize, y_r2, 0.])-centre # in mm
    ll_box_r3 = np.asarray([0., y_r2, 0.])-centre; ur_box_r3 = np.asarray([slabsize, y_r3, 0.])-centre # in mm

    box_r0 = mesh.BoxRegion(ll_box_r0, ur_box_r0, 0) # in mm
    box_r1 = mesh.BoxRegion(ll_box_r1, ur_box_r1, 1) # in mm
    box_r2 = mesh.BoxRegion(ll_box_r2, ur_box_r2, 2) # in mm
    box_r3 = mesh.BoxRegion(ll_box_r3, ur_box_r3, 3) # in mm

    geom.add_region(box_r0) # mesher expects boxes relative to the center!!!
    geom.add_region(box_r1)
    geom.add_region(box_r2)
    geom.add_region(box_r3)

    meshname = mesh.generate(geom)

    return meshname, geom


def parser():
    parser = tools.standard_parser()
    group  = parser.add_argument_group('experiment specific options')

    group.add_argument('--min_scf', default=0.1, type=float,
                       help='Minimum scaling factor used in lower extremity of tissue block')
    group.add_argument('--max_scf', default=2.0, type=float,
                        help='Maximum scaling factor used in upper extremity of tissue block')

    group.add_argument('--duration', type=float, default=300,
                        help='Duration of simulation (ms)')
    group.add_argument('--slabsize', default=10000., type=float,
                        help='Side length of 2D slab (triangles) in microns.')
    group.add_argument('--resolution', default=100., type=float,
                       help='Edge length of triangular slab elements in microns.')
    return parser

def jobID(args):
    """
    Generate name of top level output directory.
    """
    today = date.today()
    return '{}'.format(today.isoformat())


@tools.carpexample(parser, jobID)
def run(args, job):

    # user input
    f_min_scf  = args.min_scf
    f_max_scf  = args.max_scf

    # baseline GKr/GKs values in the UCLA_RAB model
    GKr_base   = 0.012500
    GKs_base   = 0.173558


    # generate mesh and return basename
    meshname, geom = setupMesh(args)


    # setup GKr/GKs region-wise im_param arguments
    reg0_cmd = 'GKr*{},factorGKs*{}'.format(f_min_scf, f_min_scf)
    reg1_cmd = 'GKr*{},factorGKs*{}'.format(f_min_scf+(1./3.)*(f_max_scf-f_min_scf),
                                      f_min_scf+(1./3.)*(f_max_scf-f_min_scf))
    reg2_cmd = 'GKr*{},factorGKs*{}'.format(f_min_scf+(2./3.)*(f_max_scf-f_min_scf),
                                      f_min_scf+(2./3.)*(f_max_scf-f_min_scf))
    reg3_cmd = 'GKr*{},factorGKs*{}'.format(f_max_scf, f_max_scf)


    # setup command line and run simulation #1 (Region-wise APD variability)
    simID1 = os.path.join(job.ID, 'RegionWise_{0:4.2f}-{1:4.2f}'.format(f_min_scf,f_max_scf))
    cmd    = tools.carp_cmd(os.path.join(EXAMPLE_DIR, 'Region-Wise-APD-Variability.par'))
    cmd   += ['-simID',                  simID1,
              '-tend',                   args.duration,
              '-imp_region[0].im_param', reg0_cmd,
              '-imp_region[1].im_param', reg1_cmd,
              '-imp_region[2].im_param', reg2_cmd,
              '-imp_region[3].im_param', reg3_cmd]

    # add stimulus location (left side)
    # stimID = 0
    # cmd += mesh.block_bc_opencarp(geom, 'stim', stimID, 'x', True)

    lstim  = ['-stim[0].elec.p0[0]',    0,
              '-stim[0].elec.p1[0]',    args.resolution*5.,
              '-stim[0].elec.p0[1]',    0,
              '-stim[0].elec.p1[1]',    args.slabsize,
              '-stim[0].elec.p0[2]',    -args.resolution/2.,
              '-stim[0].elec.p1[2]',    0]

    # add stumulus
    cmd += lstim  

    # add meshname
    cmd   += ['-meshname', meshname]

    # run simulation #1
    job.carp(cmd)

    # -------------------------------------------------------------------------

    # Setup .adj and .dat files for gradient by reading mesh .pts file and extracting y values
    adjf_GKr_fn = os.path.join(job.ID, 'GradientWise_{0:4.2f}-{1:4.2f}_GKr'.format(f_min_scf,f_max_scf))
    adjf_GKs_fn = os.path.join(job.ID, 'GradientWise_{0:4.2f}-{1:4.2f}_GKs'.format(f_min_scf,f_max_scf))

    # Read in .pts file and output .adj and .dat files
    points, npts = txt.read(meshname+'.pts')
    if not args.dry:
        adjf_GKr = open(adjf_GKr_fn + '.adj','w')
        adjf_GKs = open(adjf_GKs_fn + '.adj','w')
        datf_GKr = open(adjf_GKr_fn + '.dat','w')
        datf_GKs = open(adjf_GKs_fn + '.dat','w')

        for i in range(npts):
            if not i:
                # Write .adj file headers
                adjf_GKr.write('{}\nintra\n'.format(npts))
                adjf_GKs.write('{}\nintra\n'.format(npts))

            # y values will vary from 0 to 'slabsize' from bottom to top of sheet
            scf = f_min_scf + ((f_max_scf - f_min_scf) * points[i,1]/args.slabsize)

            adjf_GKr.write('{} {}\n'.format(i, scf*GKr_base))
            adjf_GKs.write('{} {}\n'.format(i, scf*GKs_base))
            datf_GKr.write('{}\n'.format(scf*GKr_base))
            datf_GKs.write('{}\n'.format(scf*GKs_base))

        adjf_GKr.close()
        adjf_GKs.close()
        datf_GKr.close()
        datf_GKs.close()

    # Setup command line and run simulation #2 (Gradient-wise APD variability)
    simID2 = os.path.join(job.ID, 'GradientWise_{0:4.2f}-{1:4.2f}'.format(f_min_scf,f_max_scf))
    cmd    = tools.carp_cmd(os.path.join(EXAMPLE_DIR, 'Gradient-Wise-APD-Variability.par'))
    cmd   += ['-simID',              simID2,
              '-tend',               args.duration,
              '-adjustment[0].file', adjf_GKr_fn + '.adj',
              '-adjustment[1].file', adjf_GKs_fn + '.adj']

    # add stumulus
    cmd += lstim 

    # add meshname
    cmd   += ['-meshname', meshname]
    job.carp(cmd)

    if args.dry:
        return
    
    # Calculate and output difference in APD between Sim1 and Sim2 for each node in the grid
    depolf1 = txt.read(os.path.join(simID1, 'init_acts_depol-thresh.dat'))
    repolf1 = txt.read(os.path.join(simID1, 'init_acts_repol-thresh.dat'))
    APD1    = repolf1 - depolf1
    txt.write(os.path.join(simID1, 'APD.dat'), APD1)

    depolf2 = txt.read(os.path.join(simID2, 'init_acts_depol-thresh.dat'))
    repolf2 = txt.read(os.path.join(simID2, 'init_acts_repol-thresh.dat'))
    APD2    = repolf2 - depolf2
    txt.write(os.path.join(simID2, 'APD.dat'), APD2)

    dAPD    = APD2 - APD1
    dAPD_fn = os.path.join(job.ID, 'DeltaAPD_{0:4.2f}-{1:4.2f}.dat'.format(f_min_scf,f_max_scf))
    txt.write(dAPD_fn, dAPD)

    # Do visualization
    if args.visualize and not settings.platform.BATCH:
        geom = meshname
        data = dAPD_fn
        view = os.path.join(EXAMPLE_DIR, 'DeltaAPD.mshz')
        job.meshalyzer(geom, data, view)

# Define some tests
__tests__ = []
f_min_scf = 0.1
f_max_scf = 2.0
simID1 = 'RegionWise_{0:4.2f}-{1:4.2f}'.format(f_min_scf,f_max_scf)
simID2 = 'GradientWise_{0:4.2f}-{1:4.2f}'.format(f_min_scf,f_max_scf)

desc = ('Testing region-based and gradient based incorporation of '
        'heterogeneities in a thin 2D sheet.')

test = testing.Test('serial', run, ['--np', 1],
                    description = desc,
                    refdir = 'serial',
                    tags = [testing.tag.MEDIUM, testing.tag.SERIAL])
test.add_filecmp_check(os.path.join(simID1, 'init_acts_depol-thresh.dat'), testing.max_error, 0.001)
test.add_filecmp_check(os.path.join(simID1, 'init_acts_repol-thresh.dat'), testing.max_error, 0.001)
test.add_filecmp_check(os.path.join(simID2, 'init_acts_depol-thresh.dat'), testing.max_error, 0.001)
test.add_filecmp_check(os.path.join(simID2, 'init_acts_repol-thresh.dat'), testing.max_error, 0.001)
test.add_filecmp_check('DeltaAPD_{0:4.2f}-{1:4.2f}.dat'.format(f_min_scf,f_max_scf), testing.max_error, 0.001)
__tests__.append(test)

desc = ('Same setting as in single core mode above, but now using 8 threads. '
        'Compare against single core results.')
test = testing.Test('parallel', run, ['--np', 4],
                    description = desc,
                    refdir = 'serial',
                    tags = [testing.tag.MEDIUM, testing.tag.PARALLEL])
test.add_filecmp_check(os.path.join(simID1, 'init_acts_depol-thresh.dat'), testing.max_error, 0.001)
test.add_filecmp_check(os.path.join(simID1, 'init_acts_repol-thresh.dat'), testing.max_error, 0.001)
test.add_filecmp_check(os.path.join(simID2, 'init_acts_depol-thresh.dat'), testing.max_error, 0.001)
test.add_filecmp_check(os.path.join(simID2, 'init_acts_repol-thresh.dat'), testing.max_error, 0.001)
test.add_filecmp_check('DeltaAPD_{0:4.2f}-{1:4.2f}.dat'.format(f_min_scf,f_max_scf), testing.max_error, 0.001)
test.disable_reference_generation()
__tests__.append(test)


if __name__ == '__main__':
    run()
