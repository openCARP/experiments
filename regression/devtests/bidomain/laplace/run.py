#!/usr/bin/env python3

"""
Demonstration of solving Laplace's equation in CARP.
"""
import os

EXAMPLE_DESCRIPTIVE_NAME = 'Laplace\'s Equation'
EXAMPLE_AUTHOR = 'Andrew Crozier <andrew.crozier@medunigraz.at>'
EXAMPLE_DIR = os.path.dirname(__file__)

from datetime import date

from carputils import settings
from carputils import tools
from carputils import mesh
from carputils import testing

def parser():
    parser = tools.standard_parser()
    parser.add_argument('--resolution',
                        type=float, default=0.1,
                        help='Resolution to discretise mesh (mm) (default: '
                            +'0.5)')
    return parser

def jobID(args):
    """
    Generate name of top level output directory.
    """
    today = date.today()
    return '{}_laplace_res{}_{}_np{}'.format(today.isoformat(), args.resolution,
                                             args.flv, args.np)

@tools.carpexample(parser, jobID)
def run(args, job):

    # Generate mesh
    # Units are mm
    # Block which is thin in z direction
    geom = mesh.Block(size=(10, 10, args.resolution), resolution=args.resolution)
    # Generate and return base name
    meshname = mesh.generate(geom)

    # Set up conductivity
    cond = ['-num_gregions',    1,
            '-gregion[0].g_il', 1,
            '-gregion[0].g_it', 1,
            '-gregion[0].g_el', 1,
            '-gregion[0].g_et', 1]

    # Define the geometry of the stimulus at one end of the block
    stimuli = ['-num_stim', 2]

    # Add ground
    stimuli += mesh.block_bc_opencarp(geom, 'stim', 0, 'x', True)
    stimuli += ['-stim[0].crct.type',       3]

    # Add voltage
    stimuli += mesh.block_bc_opencarp(geom, 'stim', 1, 'y', True)

    stimuli += ['-stim[1].crct.type',       2,
                '-stim[1].ptcl.start',      0,
                '-stim[1].ptcl.duration',   1,
                '-stim[1].pulse.strength',  1]

    # Get basic command line, including solver options
    cmd = tools.carp_cmd()

    cmd += ['-simID',      job.ID,
            '-meshname',   meshname,
            '-experiment', 2, # Laplacian solve
            '-bidomain',   1] # This option must be set or the code segfaults. 
                              # The laplacian solve takes place on the extracellular grid.
    cmd += stimuli
    cmd += cond

    if args.visualize:
        cmd += ['-gridout_i', 3]
        cmd += ['-gridout_e', 3]

    # Run simulation 
    job.carp(cmd)

    # Do visualization
    if args.visualize and not settings.platform.BATCH:

        # Prepare file paths
        geom = os.path.join(job.ID, os.path.basename(meshname)+'_i')
        data = os.path.join(job.ID, 'phie.igb.gz')
        view = os.path.join(EXAMPLE_DIR, 'laplace.mshz')
        
        # Call meshalyzer
        job.meshalyzer(geom, data, view)

# Define some tests
# ---SERIAL---------------------------------------------------------------------
desc = ('Solve Laplace\'s equation on thin slab and compare :math:`\phi_e` '
        'against reference.')
test_default = testing.Test('default', run, description=desc,
                            tags=[testing.tag.FAST, testing.tag.SERIAL])
test_default.add_filecmp_check('phie.igb.gz', testing.max_error, 0.001)
# ---PARALLEL-------------------------------------------------------------------
test_parallel = testing.Test('parallel', run, ['--np', '2'], description=desc,
                              refdir='default', 
                              tags=[testing.tag.FAST, testing.tag.PARALLEL])
test_parallel.add_filecmp_check('phie.igb.gz', testing.max_error, 0.001)
test_parallel.disable_reference_generation()


__tests__ = [test_default,test_parallel]

if __name__ == '__main__':
    run()
