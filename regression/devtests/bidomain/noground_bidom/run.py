#!/usr/bin/env python3

"""
Description TBD.
"""
import os

EXAMPLE_DESCRIPTIVE_NAME = 'Bidomain without a ground-type stimulus'
EXAMPLE_AUTHOR = 'Aurel Neic <aurel.neic@medunigraz.at>'
EXAMPLE_DIR = os.path.dirname(__file__)

from datetime import date

from carputils import settings
from carputils import tools
from carputils import mesh
from carputils import testing

def parser():
    parser = tools.standard_parser()
    parser.add_argument('--ground',
                        choices=['free', 'enforced','floating', 'free'],
                        default='free',
                        help='choose if ground is enforced (Dirichlet), '
                             'floating (single node Dirichlet with mean '
                             'average) or free (Neumann problem)')
    parser.add_argument('--resolution',
                        type=float, default=0.1,
                        help='resolution of the block (mm)')
    parser.add_argument('--sizeX',
                        type=float, default=6,
                        help='length of the tissue block (mm)')
    parser.add_argument('--sizeY',
                        type=float, default=1,
                        help='length of the tissue block (mm)')
    parser.add_argument('--sizeZ',
                        type=float, default=1,
                        help='length of the tissue block (mm)')
    parser.add_argument('--tend',
                        type=float, default=8,
                        help='simulated time (ms)')
    parser.add_argument('--parab-solve',
                        type=int, default=1,
                        help='Time integration scheme: explicit (0), Crank-Nicolson (1), second order implicit (2)')
    parser.add_argument('--dt',
                        type=float, default=10,
                        help='Time integration step (us)')
    parser.add_argument('--imp',
                        default='DrouhardRoberge',
                        help='cell model to be used. Run bench --list-imp for choices.')
    return parser

def jobID(args):
    """
    Generate name of top level output directory.
    """
    today = date.today()
    return '{}_{}_{}_np{}'.format(today.isoformat(), args.flv, args.ground,
                                  args.np)

@tools.carpexample(parser, jobID)
def run(args, job):

    # Generate mesh
    # strand extended in x-direction
    geom = mesh.Block(size=(args.sizeX, args.sizeY, args.sizeZ), etype='tetra', resolution=args.resolution)
    geom.set_bath(thickness=(0.2,0.2,0.2), both_sides=True)
    # Set fibre angle to 0, sheet angle to 0
    geom.set_fibres(0, 0, 90, 90)
    stim = mesh.block_bc_opencarp(geom, 'stim', 0, 'x')

    # Generate and return base name
    meshname = mesh.generate(geom)

    # Add all the non-general arguments
    cmd = tools.carp_cmd(os.path.join(EXAMPLE_DIR, 'noground-bidom.par'))
    cmd += tools.gen_physics_opts(ExtraTags=[0, 1], IntraTags=[1])

    cmd += ['-meshname', meshname,
            '-simID',    job.ID,
            '-parab_solve',   args.parab_solve,
            '-tend', args.tend,
            '-dt', args.dt
           ]

    cmd += stim

    cmd += ['-num_imp_regions', 1,
            '-imp_region[0].im', args.imp]


    if args.ground == 'floating':
        cmd += [ '-floating_ground', '1' ]

    if args.ground == 'enforced':
        gnd = [ '-stim[1].crct.type', '3',
                '-stim[1].elec.vtx_file', os.path.join(EXAMPLE_DIR, 'gnd')]
        cmd += [ '-num_stim', '2'] + gnd

    if args.visualize:
        vis_cmd = ['-gridout_i',   3]
        cmd += vis_cmd

    # Execute simulation
    job.carp(cmd)

    # Do visualization
    if args.visualize and not settings.platform.BATCH:

        geom = os.path.join(job.ID, os.path.basename(meshname)+'_i')
        data = os.path.join(job.ID, 'phie_i.igb.gz')
        view = os.path.join(EXAMPLE_DIR, 'view_phie.mshz')

        job.meshalyzer(geom, data, view)

# test settings
test_grnd_free = testing.Test('bidomain-ground-free', run, ['--ground','free', '--np','8' ],
                             tags=[testing.tag.MEDIUM,
                                   testing.tag.BIDOMAIN,
                                   testing.tag.PARALLEL])
test_grnd_free.add_filecmp_check('phie_i.igb.gz', testing.max_error, 0.001)

test_grnd_enforced = testing.Test('bidomain-ground-enforced', run, ['--ground','enforced', '--np','8' ],
                             tags=[testing.tag.MEDIUM,
                                   testing.tag.BIDOMAIN,
                                   testing.tag.PARALLEL])
test_grnd_enforced.add_filecmp_check('phie_i.igb.gz', testing.max_error, 0.001)

test_grnd_floating = testing.Test('bidomain-ground-floating', run, ['--ground','floating', '--np','8' ],
                             tags=[testing.tag.MEDIUM,
                                   testing.tag.BIDOMAIN,
                                   testing.tag.PARALLEL])
test_grnd_floating.add_filecmp_check('phie_i.igb.gz', testing.max_error, 0.001)

__tests__ = [test_grnd_free, test_grnd_enforced, test_grnd_floating]

if __name__ == '__main__':
    run()
