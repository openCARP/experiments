#!/usr/bin/env python

"""
Description TBD.
"""
import os

EXAMPLE_DESCRIPTIVE_NAME = 'Prepace'
EXAMPLE_AUTHOR = 'Fernando Campos <fernando.campos@kcl.ac.uk>'
EXAMPLE_DIR = os.path.dirname(__file__)

from datetime import date

from carputils import settings
from carputils import tools
from carputils import mesh
from carputils import testing

def parser():
    parser = tools.standard_parser()
    parser.add_argument('--tend',
                        type=float, default=50.0,
                        help='Duration of simulation (ms)')
    return parser

def jobID(args):
    """
    Generate name of top level output directory.
    """
    today = date.today()
    return '{}_prepace_{}_{}_np{}'.format(today.isoformat(), args.tend,
                                          args.flv, args.np)

@tools.carpexample(parser, jobID)
def run(args, job):

    # Generate mesh
    # Units are mm
    # Block which is thin in z direction
    geom = mesh.Block(size=(10,10,0),resolution=0.1,etype='hexa')
    # Put the corner of the tissue block at the origin.
    geom.corner_at_origin()
    # Add regions ------> (0,0,0) is the center of the mesh, not the left lower corner!
    endo = mesh.BoxRegion((0-5,0-5,0),(0-5+3,0-5+10,0.1),tag=100)
    geom.add_region(endo)
    m = mesh.BoxRegion((0-5+3,0-5,0),(0-5+3+4,0-5+10,0.1),tag=200)
    geom.add_region(m)
    epi = mesh.BoxRegion((0-5+3+4,0-5,0),(0-5+3+4+3,0-5+10,0.1),tag=300)
    geom.add_region(epi)
    # Set fibre angle to 0, sheet angle to 0
    geom.set_fibres(0, 0, 90, 90)
    # Generate and return base name
    meshname = mesh.generate(geom)

    # IONIC SETUP
    imp_reg = ['-num_imp_regions', 3,
               '-imp_region[0].im', "DrouhardRoberge",
               '-imp_region[0].im_param', "APDshorten=8",
               '-imp_region[0].name', "Endocardium",
               '-imp_region[0].num_IDs', 1,
               '-imp_region[0].ID[0]', 100,
               '-imp_region[1].im', "DrouhardRoberge",
               '-imp_region[1].im_param', "APDshorten=2",
               '-imp_region[1].name', "M-Cell",
               '-imp_region[1].num_IDs', 1,
               '-imp_region[1].ID[0]', 200,
               '-imp_region[2].im', "DrouhardRoberge",
               '-imp_region[2].im_param', "APDshorten=4",
               '-imp_region[2].name', "Epicardium",
               '-imp_region[2].num_IDs', 1,
               '-imp_region[2].ID[0]', 300]
    
    # TISSUE SETUP
    g_reg = ['-num_gregions', 1,
             '-gregion[0].num_IDs', 3,
             '-gregion[0].ID[0]', 100,
             '-gregion[0].ID[1]', 200,
             '-gregion[0].ID[2]', 300]

    # STIMULATION PROTOCOL
    stim = ['-num_stim', 1,
            '-stim[0].crct.type', 0,
            '-stim[0].pulse.strength', 100.0,
            '-stim[0].ptcl.duration', 1.0,
            '-stim[0].ptcl.npls', 1,
            '-stim[0].elec.p0[0]', 0,
            '-stim[0].elec.p1[0]', 500,
            '-stim[0].elec.p0[1]', 0,
            '-stim[0].elec.p1[1]', 500,
            '-stim[0].elec.p0[2]', 0,
            '-stim[0].elec.p1[2]', 500]

    # NUMERICAL PARAMETERS
    num_par = ['-dt', 50,
               '-parab_solve', 1]

    # I/O
    IO_par = ['-spacedt', 0.1,
              '-timedt', 10.0,
              '-num_gvecs', 1,
              '-gvec[0].name', "Cai",
              '-gvec[0].units', "umol/L",
              '-gvec[0].ID[0]', "Cai"]

   
    # LATs
    lat = ['-num_LATs', 1,
           '-lats[0].ID', "LATs",
           '-lats[0].all', 0,
           '-lats[0].measurand', 0,
           '-lats[0].threshold', -10,
           '-lats[0].method', 1]


    # RUN ONCE TO GENERATE LATs
    
    # Get basic command line, including solver options
    cmd = tools.carp_cmd()

    cmd += imp_reg
    cmd += g_reg
    cmd += stim
    cmd += num_par
    cmd += IO_par
    cmd += lat
    lat_simID = os.path.join(job.ID, 'lats')
    
    cmd += ['-meshname', meshname,
            '-tend',     args.tend,
            '-simID',    lat_simID]

    # Run simulation 
    job.carp(cmd)
    
    # RUN PREPACE CODE TAKING LATs FROM THE PREVIOUS SIMULATION
    
    prepace = ['-prepacing_beats', 1,
               '-prepacing_bcl',   500,
               '-prepacing_lats',
                    os.path.join(lat_simID, 'init_acts_LATs-thresh.dat')]
    
    # Get basic command line, including solver options
    cmd = tools.carp_cmd()

    cmd += imp_reg
    cmd += g_reg
    cmd += stim
    cmd += prepace
    cmd += num_par
    cmd += IO_par
    cmd += lat
    
    prepace_simID = os.path.join(job.ID, 'prepace')

    cmd += ['-meshname', meshname,
            '-tend',     args.tend,
            '-simID',    prepace_simID]
    
    if args.visualize:
        cmd += ['-gridout_i', 2]

    # Run simulation
    job.carp(cmd)

    # Do visualization
    if args.visualize and not settings.platform.BATCH:

        # Prepare file paths
        geom = os.path.join(prepace_simID, os.path.basename(meshname)+'_i')
        data = os.path.join(prepace_simID, 'vm.igb.gz')
        
        # Call meshalyzer to show Vm
        job.meshalyzer(geom, data)

        # and again to show at least one state variable
        data = os.path.join(prepace_simID, 'Cai.igb')
        job.meshalyzer(geom, data)

# Define some tests
test_default = testing.Test('default', run,
                            tags=[testing.tag.SHORT, testing.tag.SERIAL])
test_default.add_filecmp_check('prepace/vm.igb.gz', testing.max_error, 0.001)

test_long = testing.Test('long', run, ['--tend', '100.'],
                         tags=[testing.tag.SHORT, testing.tag.SERIAL])
test_long.add_filecmp_check('prepace/vm.igb.gz', testing.max_error, 0.001)

__tests__ = [test_default, test_long]

if __name__ == '__main__':
    run()
