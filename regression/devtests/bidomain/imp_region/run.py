"""
The test included in this file aims to check a bug [iss332] triggered by
reading state-variable file for imp_regions that has no representation on the
mesh (i.e. all associated tags did not exist in the mesh).

[iss332] https://git.opencarp.org/openCARP/openCARP/-/issues/332

If the bug is present the simulation fails.
"""
import os

EXAMPLE_DESCRIPTIVE_NAME = 'Issue 332 Checker'
EXAMPLE_AUTHOR = ''
EXAMPLE_DIR = os.path.dirname(__file__)

from datetime import date
from carputils import tools
from carputils import testing

def parser():
    parser = tools.standard_parser()
    parser.add_argument('--tend',
                        type=float, default=20.,
                        help='Duration of simulation (ms)')
    return parser

def jobID(args):
    """
    Generate name of top level output directory.
    """
    today = date.today()
    return '{}_simple_{}_{}_np{}'.format(today.isoformat(), args.tend,
                                         args.flv, args.np)
@tools.carpexample(parser, jobID)
def run(args, job):
    cmd = tools.carp_cmd(os.path.join(EXAMPLE_DIR, 'parameters.par'))
    cmd += ['-simID',      job.ID]
    cmd += ['-meshname', os.path.join(EXAMPLE_DIR, 'patch_fib')]
    cmd += ['-tagreg[0].elemfile',os.path.join(EXAMPLE_DIR, 'fib_101.regele')]
    cmd += ['-tagreg[1].elemfile',os.path.join(EXAMPLE_DIR, 'fib_102.regele')]
    cmd += ['-tagreg[2].elemfile',os.path.join(EXAMPLE_DIR, 'fib_103.regele')]
    cmd += ['-tagreg[3].elemfile',os.path.join(EXAMPLE_DIR, 'fib_104.regele')]
    cmd += ['-imp_region[0].im_sv_init',os.path.join(EXAMPLE_DIR, 'init_values_stab_0_bcl_600.0.sv')]

    job.carp(cmd)


# test settings
desc = ('Check that the bug openCARP!332 is solved')
# test_default = testing.Test('default', run, description=desc,
#                             tags=[testing.tag.FAST, testing.tag.SERIAL])
# # test_default.add_filecmp_check('prepace_patch_fib_fib/vm_last_beat.igb', testing.max_error, 0.001)
# test_default.add_filecmp_check('vm.igb.gz', testing.max_error, 0.001)

# __tests__ = [test_default]

# test_default.add_filecmp_check('vm.igb.gz', testing.max_error, 0.001)
test_imp_region = testing.Test('prepace_patch_fib_fib', run, ['--np','8' ],
                             tags=[testing.tag.FAST,
                                   testing.tag.PARALLEL])

# Could be changed to an analytic test (or a newly implemented test directly considering the norm provided above)
# Would need some refactoring, though.
test_imp_region.add_filecmp_check('vm_last_beat.igb', testing.max_error, 0.001)

__tests__ = [test_imp_region]


if __name__ == '__main__':
    run()
