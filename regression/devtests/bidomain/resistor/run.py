#!/usr/bin/env python

"""
Validation of physical units used in extracellular voltage and current stimulation code

Problem Setup
=============
This test generates a simle slab geometry of length :math:`L=10 mm` with a square cross section
of edge length :math:`a = 0.1 mm`. A conductivity of :math:`1 S/m` is assigned. 
The total resistance between the two terminals of size :math:`a \times a` is then :math:`1 \mu S`.
To use the extracellular space of the bidoman model as an approximation of a resistor,
a passive ionic model (Plonsey) is used. As we aim to prevent current entering an intracellular path 
we minimize current flow into the intracellular space by increasing the membrane resistance
and decreasing the conductivity of the intracellular current path. 

Two tests are performed. In both tests, the right hand side terminal will be grounded. 
Into the  left hand side terminal we either inject current of a total strength of :math:`1 \mu A`
or we impose a voltage of :math:`V_{x=-L/2} = 1000 mV`.
As we model a resistor in both cases the total voltage drop across the resisor has to be :math:`1000 mV`
and the total current flowing over the resistor has to be :math:`1 \mu A`.
The setup is illustrated below:

.. image:: /images/resistor_setup.png
    

Usage
=====

This example specifies only one argument, ``--stimulus``,
which can be set to extra_V or extra_I.

.. code-block:: bash

    ./run.py --stimulus extra_I

"""
import os

EXAMPLE_DESCRIPTIVE_NAME = 'resistor'
EXAMPLE_AUTHOR = 'Gernot Plank <gernot.plank@medunigraz.at>'
EXAMPLE_DIR = os.path.dirname(__file__)

from datetime import date

from carputils import settings
from carputils import tools
from carputils import mesh
from carputils import testing

def parser():
    parser = tools.standard_parser()

    parser.add_argument('--stimulus', 
                        default='extra_V', 
                        choices=['extra_V',
                                 'extra_I'],
                        help='pick stimulus type (default is extra extra_V)')
    return parser

def jobID(args):
    """
    Generate name of top level output directory.
    """
    today = date.today()
    return '{}_stim_{}_{}_np{}'.format(today.isoformat(), args.stimulus,
                                       args.flv, args.np)

@tools.carpexample(parser, jobID)
def run(args, job):

    # Generate mesh
    # Block which is thin in z direction
    geom = mesh.Block(size=(10, 0.1, 0.1))
    # Set fibre angle to 0, sheet angle to 0
    geom.set_fibres(0, 0, 90, 90)
    # Generate and return base name
    meshname = mesh.generate(geom)

    # define electrode locations
    lstm = mesh.block_bc_opencarp(geom, 'stim', 0, 'x', lower=True, verbose=not args.silent)

    rstm = mesh.block_bc_opencarp(geom, 'stim', 1, 'x', lower=False, verbose=not args.silent)
    
    astm = []
  
    # make subtest specific settings
    duration = 100.
    if args.stimulus == 'extra_V':
        # left electrode imposes extracellular voltage
        lstm += [ '-stim[0].crct.type', 2,
                  '-stim[0].pulse.strength', 1e3,
                  '-stim[0].ptcl.duration', duration]

    elif args.stimulus == 'extra_I':
        # left electrode injects extracellular current
        # right electrode is grounded
        lstm += [ '-stim[0].crct.type', 1,
                  '-stim[0].pulse.strength', 2.215e6, 
                  '-stim[0].ptcl.duration', duration]

    else:
        raise Exception('Unknown stimulus type!')

    # Add all the non-general arguments
    cmd = tools.carp_cmd(os.path.join(EXAMPLE_DIR, 'resistor.par'))

    cmd += ['-meshname',  meshname,
            '-gridout_i', 3,
            '-tend',      duration,
            '-simID',     job.ID]  


    # add PDE solver options
    cmd += lstm + rstm + astm

    if args.visualize:
        vis_cmd = ['-gridout_i', 3,
                   '-gridout_e', 3]
        cmd += vis_cmd
    
    # Run simulations
    job.carp(cmd)

    # Do visualization
    if args.visualize and not settings.platform.BATCH:

        geom = os.path.join(job.ID, os.path.basename(meshname)+'_i')
        data = os.path.join(job.ID, 'phie.igb')
        view = os.path.join(EXAMPLE_DIR, 'resistor.mshz')
        
        job.gunzip(data)
        job.meshalyzer(geom, data, view)

# test extracellular voltage stimulation
test_resistor_V = testing.Test('extra_V', 
                            run, ['--stimulus', 'extra_V' ],
                            tags=[testing.tag.FAST, testing.tag.SERIAL])
test_resistor_V.add_filecmp_check('phie.igb.gz', testing.max_error, 0.001)
test_resistor_V.add_filecmp_check('vm.igb.gz', testing.max_error, 0.001)

# test extracellular current stimulation
test_resistor_I = testing.Test('extra_I', 
                            run, ['--stimulus', 'extra_I'],
                            tags=[testing.tag.FAST, testing.tag.SERIAL])
test_resistor_I.add_filecmp_check('phie.igb.gz', testing.max_error, 0.001)
test_resistor_I.add_filecmp_check('vm.igb.gz', testing.max_error, 0.001)

__tests__ = [test_resistor_V, 
             test_resistor_I ] 


if __name__ == '__main__':
    run()
