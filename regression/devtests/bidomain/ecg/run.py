#!/usr/bin/env python3

"""
Description: ECG computed based on a tissue wedge simulation.
The wedge contains transmural (Boukens et al. Cardiovasc Res 2015) as well as apico-basal heterogeneity.

.. image:: /images/ecg_setup.png
    :scale: 60%
"""
import os
EXAMPLE_DESCRIPTIVE_NAME = 'ECG simulation on a tissue wedge with heterogeneity'
EXAMPLE_AUTHOR = 'Fernando Campos <fernando.campos@kcl.ac.uk>'
EXAMPLE_DIR = os.path.dirname(__file__)
CALLER_DIR = os.getcwd()

from datetime import date
from carputils.carpio import txt
from carputils import mesh
from carputils import settings
from carputils import testing
from carputils import tools
import numpy as np

def parser():
    parser = tools.standard_parser()
    parser.add_argument('--tend',
                        type = float,
                        default = 100.0,
                        help = 'Duration of simulation (ms)')
    return parser

def jobID(args):
    """
    Generate name of top level output directory.
    """
    today = date.today()
    return '{}_ecg_{}_{}_np{}'.format(today.isoformat(), args.tend,
                                          args.flv, args.np)

@tools.carpexample(parser, jobID)
def run(args, job):

    # Generate mesh
    # Size of the wedge (mm)
    wedge_x = 20
    wedge_y = 50
    wedge_z =  0.2
    
    # Spatial (mm) and time (us) discretization
    dx = 0.2
    dt = 100
    geom = mesh.Block(size=(wedge_x,wedge_y,wedge_z),resolution=dx,etype='hexa')
    
    # Put the corner of the tissue block at the origin
    geom.corner_at_origin()
    
    # Transmural gradient
    endo = 6.5
    mid  = 7.0
    epi  = wedge_x - endo - mid
    
    # Apico-basal gradient
    apex    = 15
    central = 20
    base    = wedge_y - central - apex
    
    # Tags
    tag_apex_endo    = 300 + 25
    tag_apex_mid     = 300 + 50
    tag_apex_epi     = 300 + 75
    
    tag_central_endo = 200 + 25
    tag_central_mid  = 200 + 50
    tag_central_epi  = 200 + 75
    
    tag_base_endo    = 100 + 25
    tag_base_mid     = 100 + 50
    tag_base_epi     = 100 + 75
    
    # Add regions ------> (0,0,0) is the center of the mesh, not the left lower corner!
    endo_x0 = -wedge_x/2
    endo_x1 = endo_x0 + endo
    
    mid_x0 = endo_x1
    mid_x1 = mid_x0 + mid

    epi_x0 = mid_x1
    epi_x1 = epi_x0 + epi
    
    # Apex is the lower part of the wedge
    apex_y0 = -wedge_y/2
    apex_y1 = apex_y0 + apex
    
    central_y0 = apex_y1
    central_y1 = central_y0 + central
    
    base_y0 = central_y1
    base_y1 = base_y0 + base
    
    # Create mesh
    apex_endo = mesh.BoxRegion((endo_x0,apex_y0,-wedge_z),(endo_x1,apex_y1,wedge_z),tag_apex_endo)
    geom.add_region(apex_endo)
    apex_mid  = mesh.BoxRegion((mid_x0, apex_y0,-wedge_z),(mid_x1, apex_y1,wedge_z),tag_apex_mid)
    geom.add_region(apex_mid)
    apex_epi  = mesh.BoxRegion((epi_x0, apex_y0,-wedge_z),(epi_x1, apex_y1,wedge_z),tag_apex_epi)
    geom.add_region(apex_epi)
    
    central_endo = mesh.BoxRegion((endo_x0,central_y0,-wedge_z),(endo_x1,central_y1,wedge_z),tag_central_endo)
    geom.add_region(central_endo)
    central_mid  = mesh.BoxRegion((mid_x0, central_y0,-wedge_z),(mid_x1, central_y1,wedge_z),tag_central_mid)
    geom.add_region(central_mid)
    central_epi  = mesh.BoxRegion((epi_x0, central_y0,-wedge_z),(epi_x1, central_y1,wedge_z),tag_central_epi)
    geom.add_region(central_epi)

    base_endo = mesh.BoxRegion((endo_x0,base_y0,-wedge_z),(endo_x1,base_y1,wedge_z),tag_base_endo)
    geom.add_region(base_endo)
    base_mid  = mesh.BoxRegion((mid_x0, base_y0,-wedge_z),(mid_x1, base_y1,wedge_z),tag_base_mid)
    geom.add_region(base_mid)
    base_epi  = mesh.BoxRegion((epi_x0, base_y0,-wedge_z),(epi_x1, base_y1,wedge_z),tag_base_epi)
    geom.add_region(base_epi)
    
    # Set fibre angle to 0, sheet angle to 0
    geom.set_fibres(0, 0, 90, 90)
    
    # Generate and return base name
    meshname = mesh.generate(geom)

    # element labels for intracellular grid
    IntraTags = [tag_apex_endo, tag_apex_mid, tag_apex_epi,
                 tag_central_endo, tag_central_mid, tag_central_epi,
                 tag_base_endo, tag_base_mid, tag_base_epi]
    # element labels for extracellular grid
    ExtraTags = IntraTags

    # IONIC SETUP
    imp_reg = ['-num_imp_regions', 9,
               '-imp_region[0].im', "tenTusscherPanfilov",
               '-imp_region[0].im_param', "flags=ENDO,GKs*0.45",
               '-imp_region[0].name', "VE-BASE-ENDO",
               '-imp_region[0].num_IDs', 1,
               '-imp_region[0].ID[0]', tag_base_endo,
               '-imp_region[1].im', "tenTusscherPanfilov",
               '-imp_region[1].im_param', "flags=MCELL,GKs*2.48",
               '-imp_region[1].name', "VE-BASE-MCELL",
               '-imp_region[1].num_IDs', 1,
               '-imp_region[1].ID[0]', tag_base_mid,
               '-imp_region[2].im', "tenTusscherPanfilov",
               '-imp_region[2].im_param', "flags=EPI,GKs*0.86",
               '-imp_region[2].name', "VE-BASE-EPI",
               '-imp_region[2].num_IDs', 1,
               '-imp_region[2].ID[0]', tag_base_epi,
               '-imp_region[3].im', "tenTusscherPanfilov",
               '-imp_region[3].im_param', "flags=ENDO,GKs*0.56",
               '-imp_region[3].name', "VE-CENTRAL-ENDO",
               '-imp_region[3].num_IDs', 1,
               '-imp_region[3].ID[0]', tag_central_endo,
               '-imp_region[4].im', "tenTusscherPanfilov",
               '-imp_region[4].im_param', "flags=MCELL,GKs*3.1",
               '-imp_region[4].name', "VE-CENTRAL-MCELL",
               '-imp_region[4].num_IDs', 1,
               '-imp_region[4].ID[0]', tag_central_mid,
               '-imp_region[5].im', "tenTusscherPanfilov",
               '-imp_region[5].im_param', "flags=EPI,GKs*1.1",
               '-imp_region[5].name', "VE-CENTRAL-EPI",
               '-imp_region[5].num_IDs', 1,
               '-imp_region[5].ID[0]', tag_central_epi,
               '-imp_region[6].im', "tenTusscherPanfilov",
               '-imp_region[6].im_param', "flags=ENDO,GKs*0.67",
               '-imp_region[6].name', "VE-APEX-ENDO",
               '-imp_region[6].num_IDs', 1,
               '-imp_region[6].ID[0]', tag_apex_endo,
               '-imp_region[7].im', "tenTusscherPanfilov",
               '-imp_region[7].im_param', "flags=MCELL,GKs*3.72",
               '-imp_region[7].name', "VE-APEX-MCELL",
               '-imp_region[7].num_IDs', 1,
               '-imp_region[7].ID[0]', tag_apex_mid,
               '-imp_region[8].im', "tenTusscherPanfilov",
               '-imp_region[8].im_param', "flags=EPI,GKs*1.29",
               '-imp_region[8].name', "VE-APEX-EPI",
               '-imp_region[8].num_IDs', 1,
               '-imp_region[8].ID[0]', tag_apex_epi]
    
    # TISSUE SETUP
    g_reg = ['-bidomain',0,
             '-bidm_eqv_mono',1,
             '-num_gregions', 3,
             '-gregion[0].num_IDs', 3,
             '-gregion[0].ID[0]', tag_base_endo,
             '-gregion[0].ID[1]', tag_central_endo,
             '-gregion[0].ID[2]', tag_apex_endo,
             '-gregion[0].g_il', 0.174,
             '-gregion[0].g_it', 0.174,
             '-gregion[0].g_in', 0.174,
             '-gregion[0].g_el', 0.625,
             '-gregion[0].g_et', 0.625,
             '-gregion[0].g_en', 0.625,
             '-gregion[1].num_IDs', 3,
             '-gregion[1].ID[0]', tag_base_mid,
             '-gregion[1].ID[1]', tag_central_mid,
             '-gregion[1].ID[2]', tag_apex_mid,
             '-gregion[1].g_il', 0.174,
             '-gregion[1].g_it', 0.174,
             '-gregion[1].g_in', 0.174,
             '-gregion[1].g_el', 0.625,
             '-gregion[1].g_et', 0.625,
             '-gregion[1].g_en', 0.625,
             '-gregion[2].num_IDs', 3,
             '-gregion[2].ID[0]', tag_base_epi,
             '-gregion[2].ID[1]', tag_central_epi,
             '-gregion[2].ID[2]', tag_apex_epi,
             '-gregion[2].g_il', 0.174,
             '-gregion[2].g_it', 0.174,
             '-gregion[2].g_in', 0.174,
             '-gregion[2].g_el', 0.625,
             '-gregion[2].g_et', 0.625,
             '-gregion[2].g_en', 0.625]

    # STIMULATION PROTOCOL
    stim_xd = (wedge_x/10)*1000
    stim_yd = (wedge_y/4)*1000
    stim_zd = 1000
    
    stim = ['-num_stim', 1,
            '-stim[0].crct.type', 0,
            '-stim[0].pulse.strength', 100.0,
            '-stim[0].ptcl.duration', 1.0,
            '-stim[0].ptcl.npls', 1,
            '-stim[0].elec.p0[0]', 0,
            '-stim[0].elec.p1[0]', stim_xd,
            '-stim[0].elec.p0[1]', (wedge_y/2)*1000 - stim_yd/2,
            '-stim[0].elec.p1[1]', (wedge_y/2)*1000 + stim_yd/2,
            '-stim[0].elec.p0[2]', (wedge_z/2)*1000 - stim_zd/2,
            '-stim[0].elec.p1[2]', (wedge_z/2)*1000 + stim_zd/2]

    # NUMERICAL PARAMETERS
    num_par = ['-dt', dt,
               '-parab_solve', 1]

    # I/O
    IO_par = ['-spacedt', 0.1,
              '-timedt', 10.0]

    # LATs
    lat = ['-num_LATs', 1,
           '-lats[0].ID', "LATs",
           '-lats[0].all', 0,
           '-lats[0].measurand', 0,
           '-lats[0].threshold', -10,
           '-lats[0].method', 1]
    
    # ECG
    ecg = ['-phie_rec_ptf', os.path.join(EXAMPLE_DIR, 'ecg')]


    # Get basic command line, including solver options
    cmd  = tools.carp_cmd()

    cmd  += imp_reg
    cmd  += g_reg
    cmd  += stim
    cmd  += num_par
    cmd  += IO_par
    cmd  += lat
    cmd  += ecg
    cmd  += tools.gen_physics_opts(ExtraTags=ExtraTags, IntraTags=IntraTags)
    simID = job.ID
    
    cmd += ['-meshname', meshname,
            '-tend',     args.tend,
            '-simID',    simID]

    if args.visualize:
        cmd += ['-gridout_i', 2]

    # Run simulation 
    job.carp(cmd)

    # export auxilliary data
    aux = os.path.join(CALLER_DIR, simID, 'ecg.pts_t')

    # actually we still need to create the files in a postprocessing step
    # Note: produces the same results as the former '-dump_ecg_leads' flag!
    recv_ptsFile = os.path.join(EXAMPLE_DIR, 'ecg.pts')
    recv_igbFile = os.path.join(CALLER_DIR, simID, 'phie_recovery.igb')
    txt.write(aux, recv_igbFile, ptsf=recv_ptsFile)
    
    # Do visualization
    if args.visualize and not settings.platform.BATCH:

        # Prepare file paths
        geom = os.path.join(CALLER_DIR, simID, os.path.basename(meshname)+'_i')
        data = os.path.join(CALLER_DIR, simID, 'vm.igb')
        view = os.path.join(EXAMPLE_DIR, 'view.mshz')
        
        # Call meshalyzer to show Vm
        job.meshalyzer(geom, data, view)

# Define some tests
__tests__ = []
desc = ('Run a short activation sequence on a tissue wedge and compare '
        ':math:`phie` against reference.')
test = testing.Test('default', run, ['--np', '4'], description=desc,
                     tags=[testing.tag.MEDIUM])
test.add_filecmp_check('phie_recovery.igb', testing.max_error, 0.001)
__tests__.append(test)

desc = ('Run longer simulation (activation + repolarization) on a tissue '
        'wedge and compare :math:`phie` against reference.')
test = testing.Test('long', run, ['--np', '4', '--tend', '500.'], description=desc,
                     tags=[testing.tag.LONG])
test.add_filecmp_check('phie_recovery.igb', testing.max_error, 0.001)
__tests__.append(test)


if __name__ == '__main__':
    run()    
