#!/usr/bin/env python

"""
Example of different types of stimulation either through the application of extracellular voltages (Dirichlet)
or through the injection/withdrawal of extracellular currents (Neumann).

Problem Setup
=============

A thin strand of tissue of 1 cm lenght is generated. Electrodes are located
at both caps of the strand with an additional auxiliary electrode in the very center of the strand.
Electrode locations are shown belows:

.. figure:: /images/strand_electrodes.png
     :scale: 70 %

Each electrode can be used to inject/withdraw current, prescribe an extracellular potential
or ground the extracellular potential.


Experiments
===========

Several experiments are defined:

* ``extra_V`` - Stimulation through application of extracellular voltage.
  The potential on the left cap is clamped to 2000 mV for a duration of 2 ms.
* ``extra_V_bal`` - Stimulation through application of extracellular voltage.
  The potential on the left cap is clamped to 1000 mV for a duration of 2 ms
  and the right hand electrode to -1000mV.
* ``extra_V_OL`` - Stimulation through application of extracellular voltage.
  The potential on the left cap is clamped to 2000 mV for a duration of xx ms.
  In contrast to the ``extra_V`` case the left hand electrode is allowed to float
  after the end of the shock.
* ``extra_I`` - Stimulation through application of extracellular current.
  Extracellular current is injected into the left hand electrode
  to cause an extracellular potential drop of 2000 mV across the strand.
* ``extra_I_bal`` - Stimulation through application of extracellular current.
  Extracellular current is injected into the left hand electrode
  and the same amount is withdrawn from the right hand side electrode.
  Overall this also causes an extracellular potential drop of 2000 mV across the strand,
  albeit in a symmetric way.


Optional Arguments
==================

* ``grounded`` - A ground can be used or not. In the latter case, no grounding electrode
   is used, that is, a pure Neumann-problem is solved. A rank-one stabilization is used
   in this case which essentially enforces the integral over the extracellular potential
   to be zero at each instant in time, but no specific point is clamped.
"""
import os

EXAMPLE_DESCRIPTIVE_NAME = 'Stimulation'
EXAMPLE_AUTHOR = 'Gernot Plank <gernot.plank@medunigraz.at>'
EXAMPLE_DIR = os.path.dirname(__file__)

from datetime import date

from carputils import settings
from carputils import tools
from carputils import mesh
from carputils import testing

def parser():
    parser = tools.standard_parser()

    parser.add_argument('--stimulus',
                        default='extra_V',
                        choices=['extra_V',
                                 'extra_V_bal',
                                 'extra_V_OL',
                                 'extra_I',
                                 'extra_I_bal',
                                 'extra_I_mono'],
                        help='pick stimulus type')
    parser.add_argument('--grounded',
                        default='on',
                        choices=['on','off'],
                        help='turn on/off use of ground whereever possible')
    return parser

def jobID(args):
    """
    Generate name of top level output directory.
    """
    today = date.today()
    return '{}_stim_{}_{}_np{}'.format(today.isoformat(), args.stimulus,
                                       args.flv, args.np)

@tools.carpexample(parser, jobID)
def run(args, job):

    # Generate mesh
    # Block which is thin in z direction
    geom = mesh.Block(size=(10, 0.1, 0.1))
    # Set fibre angle to 0, sheet angle to 0
    geom.set_fibres(0, 0, 90, 90)
    # Generate and return base name
    meshname = mesh.generate(geom)

    # define electrode locations
    lstm = mesh.block_bc_opencarp(geom, 'stim', 0, 'x', lower=True, verbose=not args.silent)
    rstm = mesh.block_bc_opencarp(geom, 'stim', 1, 'x', lower=False, verbose=not args.silent)
    astm = []
    mod  = []

    # define default view settings
    view = os.path.join(EXAMPLE_DIR, 'stimulation_V.mshz')

    # make subtest specific settings
    if args.stimulus == 'extra_V':
        # left electrode imposes extracellular voltage
        lstm += [ '-stim[0].crct.type', '2',
                  '-stim[0].pulse.strength', '2e3']

        if args.grounded == 'off':
            rstm = []

    elif args.stimulus == 'extra_V_bal':
        # left electrode imposes extracellular voltage
        lstm += ['-stim[0].crct.type', '2',
                 '-stim[0].pulse.strength', '1e3']
        rstm += ['-stim[1].crct.type', '2',
                 '-stim[1].pulse.strength','-1e3']

        if args.grounded == 'on':
            astm += [ '-stim[2].crct.type', '3' ]

    elif args.stimulus == 'extra_V_OL':
        # left electrode imposes extracellular voltage
        # circuit open before and after stimulus delivery
        lstm += [ '-stim[0].crct.type', '5',
                  '-stim[0].pulse.strength', '2e3']

        if args.grounded == 'off':
            rstm = []

    elif args.stimulus == 'extra_I':
        # left electrode injects extracellular current
        # right electrode is grounded
        lstm += [ '-stim[0].crct.type', '1',
                  '-stim[0].pulse.strength', '3.1e6']

    elif args.stimulus == 'extra_I_bal':
        # left electrode injects extracellular current
        # right electrode withdraws extracellular current
        # auxiliary electrode in the middle is grounded
        lstm += [ '-stim[0].crct.type', '1',
                  '-stim[0].pulse.strength', '3.1e6']
        rstm += [ '-stim[1].crct.type', '1',
                  '-stim[1].crct.balance',  '0' ]
        if args.grounded == 'on':
            astm += [ '-stim[2].crct.type', '3' ]

    elif args.stimulus == 'extra_I_mono':
        # left electrode injects extracellular current
        # right electrode withdraws extracellular current
        # auxiliary electrode in the middle may be grounded
        # but we run a monodomain simulation
        lstm += [ '-stim[0].crct.type', '1',
                  '-stim[0].pulse.strength', '3.1e6']
        rstm += [ '-stim[1].crct.type', '1',
                  '-stim[1].crct.balance',  '0' ]
        mod  += [ '-extracell_monodomain_stim', '1',
                  '-bidomain',                  '0' ]

        if args.grounded == 'on':
            astm += [ '-stim[2].crct.type', '3' ]

    else:
        raise Exception('Unknown stimulus type!')

    # Add all the non-general arguments
    cmd = tools.carp_cmd(os.path.join(EXAMPLE_DIR, 'stimtest.par'))
    cmd += tools.gen_physics_opts(ExtraTags=[0, 1], IntraTags=[1])

    cmd += ['-meshname',  meshname,
            '-gridout_i', 3,
            '-simID',     job.ID]

    # Add specific Ginkgo configuration file as the default
    # Ginkgo AMG preconditioner used for elliptic solver not able to
    # handle the nullspace of the system matrix
    if args.flv == 'ginkgo' and args.stimulus == 'extra_I_bal' and args.grounded == 'off':
        i = cmd._mainopts.index('-ellip_options_file')
        cmd._mainopts[i+1] = os.path.join(EXAMPLE_DIR, 'ginkgo_ilu_cg_opts.json')

    # add PDE solver options
    cmd += lstm + rstm + astm

    if args.visualize:
        vis_cmd = ['-gridout_i', 3,
                   '-gridout_e', 3]
        cmd += vis_cmd

    # Run simulations
    job.carp(cmd)

    # Do visualization
    if args.visualize and not settings.platform.BATCH:

        geom = os.path.join(job.ID, os.path.basename(meshname)+'_i')
        data = os.path.join(job.ID, 'phie.igb')
        view = os.path.join(EXAMPLE_DIR, 'stimulation.mshz')

        job.gunzip(data)
        job.meshalyzer(geom, data, view)


# Define some tests
__tests__ = []

# test extracellular voltage stimulation
test = testing.Test('extra_V', run, ['--stimulus', 'extra_V' ],
                    tags=[testing.tag.FAST, testing.tag.SERIAL])
test.add_filecmp_check('phie.igb.gz', testing.max_error, 0.001)
test.add_filecmp_check('vm.igb.gz',   testing.max_error, 0.001)
__tests__.append(test)


# test extracellular voltage stimulation with interrupting the circuit (open loop)
test = testing.Test('extra_V_OL', run,
                   ['--stimulus', 'extra_V_OL'],
                    tags=[testing.tag.FAST, testing.tag.SERIAL])
test.add_filecmp_check('phie.igb.gz', testing.max_error, 0.001)
test.add_filecmp_check('vm.igb.gz',   testing.max_error, 0.001)
__tests__.append(test)


# test extracellular current stimulation
test = testing.Test('extra_I', run,
                   ['--stimulus', 'extra_I'],
                    tags=[testing.tag.FAST, testing.tag.SERIAL])
test.add_filecmp_check('phie.igb.gz', testing.max_error, 0.001)
test.add_filecmp_check('vm.igb.gz',   testing.max_error, 0.001)
__tests__.append(test)


# test extracellular current stimulation with two balanced currents,
# ground electrode is in the middle of the strand
test = testing.Test('extra_IbalGnd', run,
                   ['--stimulus', 'extra_I_bal', '--grounded', 'on'],
                    tags=[testing.tag.FAST, testing.tag.SERIAL])
test.add_filecmp_check('phie.igb.gz', testing.max_error, 0.001)
test.add_filecmp_check('vm.igb.gz',   testing.max_error, 0.001)
__tests__.append(test)


# test extracellular current stimulation with two balanced currents,
# ground electrode is in the middle of the strand
test = testing.Test('extra_IbalNGnd', run,
                   ['--stimulus', 'extra_I_bal', '--grounded', 'off'],
                    tags=[testing.tag.FAST, testing.tag.SERIAL])
test.add_filecmp_check('phie.igb.gz', testing.max_error, 0.001)
test.add_filecmp_check('vm.igb.gz',   testing.max_error, 0.001)
__tests__.append(test)


if __name__ == '__main__':
    run()
