#!/usr/bin/env python

"""
Description: ECG computed based on a tissue strand simulation.
The strand contains transmural (Boukens et al. Cardiovasc Res 2015) as well as apico-basal heterogeneity.

.. image:: /images/ecg_setup.png
    :scale: 60%
"""
import os

EXAMPLE_DESCRIPTIVE_NAME = 'SCR-mediated ectopy'
EXAMPLE_AUTHOR = 'Fernando Campos <fernando.campos@kcl.ac.uk>'
EXAMPLE_DIR = os.path.dirname(__file__)

from datetime import date

from carputils import settings
from carputils import tools
from carputils import mesh
from carputils import testing

def parser():
    parser = tools.standard_parser()
    parser.add_argument('--tend',
                        type=float, default=250.0,
                        help='Duration of simulation (ms)')
    return parser

def jobID(args):
    """
    Generate name of top level output directory.
    """
    today = date.today()
    return '{}_scr_{}_{}_np{}'.format(today.isoformat(), args.tend,
                                          args.flv, args.np)

@tools.carpexample(parser, jobID)
def run(args, job):

    # Generate mesh
    # Size of the strand (mm)
    strand_x = 10
    strand_y = 0.1
    strand_z = 0.1

    # Spatial (mm) and time (us) discretization
    dx = 0.1
    dt = 100
    geom = mesh.Block(size=(strand_x,strand_y,strand_z),resolution=dx,etype='hexa')

    # Put the corner of the tissue block at the origin
    geom.corner_at_origin()

    # Set fibre angle to 0, sheet angle to 0
    geom.set_fibres(0, 0, 90, 90)

    # Generate and return base name
    meshname = mesh.generate(geom)

    # IONIC SETUP
    imp_reg = ['-num_imp_regions', 1,
               '-imp_region[0].im', 'Campos',
               '-imp_region[0].im_param', 'GK1*0.3,INaCamax*2.0,Ca_o*2.7,CSR=1600',
               '-imp_region[0].im_sv_init',os.path.join(EXAMPLE_DIR,
                                                        'UCLA_RAB_SCR_DAD_CSR_1600uM_100npls_400bcl_INIT.st')]

    # TISSUE SETUP
    g_reg = ['-bidomain',0,
             '-bidm_eqv_mono',1,
             '-num_gregions', 1,
             '-gregion[0].g_il', 0.174,
             '-gregion[0].g_it', 0.019,
             '-gregion[0].g_in', 0.019,
             '-gregion[0].g_el', 0.625,
             '-gregion[0].g_et', 0.236,
             '-gregion[0].g_en', 0.236]

    # PROVIDING THE SEEDS FOR THE RANDOM NUMBER GENERATOR
    # NOTE: here we provide seeds for the random number generator (without them you can't reproduce a simulation)
    imp_adj = ['-num_adjustments', 1,
               '-adjustment[0].variable', 'UCLA_RAB_SCR.seed',
               '-adjustment[0].file', os.path.join(EXAMPLE_DIR, 'Cardiac-Strand.seeds')]

    # NO STIMULUS
    stim = ['-num_stim', 0]

    # NUMERICAL PARAMETERS
    num_par = ['-dt', dt,
               '-parab_solve', 1]

    # I/O
    IO_par = ['-spacedt', 0.1,
              '-timedt', 10.0]

    # LATs
    lat = ['-num_LATs', 1,
           '-lats[0].ID', "LATs",
           '-lats[0].all', 0,
           '-lats[0].measurand', 0,
           '-lats[0].threshold', -10,
           '-lats[0].method', 1]

    # Get basic command line, including solver options
    cmd = tools.carp_cmd()

    cmd += imp_reg
    cmd += g_reg
    cmd += imp_adj
    cmd += stim
    cmd += num_par
    cmd += IO_par
    cmd += lat
    simID = job.ID

    cmd += ['-meshname', meshname,
            '-tend',     args.tend,
            '-simID',    simID]

    if args.visualize:
        cmd += ['-gridout_i', 2]

    # Run simulation
    job.carp(cmd)

    # Do visualization
    if args.visualize and not settings.platform.BATCH:

        # Prepare file paths
        geom = os.path.join(simID, os.path.basename(meshname)+'_i')
        data = os.path.join(simID, 'vm.igb')
        view = '' #os.path.join(EXAMPLE_DIR, 'view.mshz')

        # Call meshalyzer to show Vm
        job.meshalyzer(geom, data, view)

# Define some tests

# Currently we lack support for ionic models with random variables. As such, I deactivate
# the SCR test for now. -Aurel

# __tests__ = []
# desc = ('Run an ectopic activation sequence on a cardiac 1D strand and compare against reference.')
# test = testing.Test('default', run, description=desc,
#                      tags=[testing.tag.MEDIUM, testing.tag.SERIAL])
# test.add_filecmp_check('init_acts_LATs-thresh.dat', testing.max_error, 0.001)
# __tests__.append(test)


if __name__ == '__main__':
    run()
