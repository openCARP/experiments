# Experiments

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgit.opencarp.org%2FopenCARP%2Fexperiments.git/master?labpath=tutorials%2Findex.ipynb)

Example experiments for openCARP based on the carputils framework.

For more details, see:
* [Rendered examples on webpage](https://opencarp.org/documentation/examples)
* [carputils repository](https://git.opencarp.org/openCARP/carputils)
* [openCARP respository](https://git.opencarp.org/openCARP/openCARP)
* [Instructions for running regression tests](./TESTS.md)
* [Instructions on converting tutorials to HTML locally](./TUTORIALS.md)
