## Running the regression tests with autotester

### Run openCARP regression tests

openCARP uses a tool named [autotester](https://autotester.readthedocs.io/en/latest/) to run its regression tests. Here are the steps to follow in order to run these tests:

1. [Download](https://git.opencarp.org/iam-cms/autotester/-/archive/master/autotester-master.tar.gz) and build autotester
2. In `experiments/autotester`, run the following scripts (they will generate the files read by autotester to run the regression tests):
```
python3 atc_template_limpet.py
python3 atc_template_bidomain.py
```
3. In the `experiments` directory, run autotester.
```
<path_to_autotester>/autotester -h
```
to see what options are available. A good start for standard execution of autotester is
```
<path_to_autotester>/autotester --logfile=atresults -X
```
In the file atresults is a summary of your autotester run. In the file output.xml is more detailed information.

### More information about the regression tests

The openCARP regression tests are located in the `experiments/regression` directory. This directory contains the simulations run for the tests. There are three categories of tests:
- ionic models (located at experiments/regression/devtests/LIMPET/ionic_model)
- plugins (located at experiments/regression/devtests/LIMPET/plugin)
- bidomain simulations (located at experiments/regression/devtests/bidomain)

The reference solutions for these simulations are located in `regression-references/devtests`

### Add your own tests

You can add your own tests to the test suite. The next sections explain how to do it, depending on the kind of test you want to add.

#### Tests on ionic models and plug-ins

* Add the reference of the model to the reference folder.
* Run the script `experiments/autotester/atc_template_limpet.py`

#### Tests on bidomain simulations

* Add your carputils script to the regression folder `experiments/regression/devtests/bidomain` and define a test in it (see the other tests for an example)
* Add your reference solution to `experiments/regression-references/devtests`
* Run the script `experiments/autotester/atc_template_bidomain.py`

You can now run autotester in the `experiments` directory.


## How to run openCARP regression tests with `carptests` (deprecated)

openCARP regression tests should now be run using autotester (see the previous section)

### Prerequisites

The current testing framework of openCARP is run using carputils. As a consequence, carputils should be installed before being able to execute the regression tests.

If you installed openCARP via an installation package or if you compiled it using the `-DBUILD_EXTERNAL=ON` CMake option, then carputils is already installed.
Otherwise, you should install it manually (see [the documentation](https://opencarp.org/download/installation#installing-carputils-separately)).


### Organization of the regression tests

The regression tests are located in the following git repository:
```
https://git.opencarp.org/openCARP/experiments
```

Relatively to the root of this repository, the test experiments are located in the `regression` folder.
The reference outputs for these tests are located in the `regression-references` folder.

If you installed openCARP via an installation package, then you've already got the `experiments` folder, it should be located in the installation directory.

If you compiled it using the `-DBUILD_EXTERNAL=ON` CMake option, then you've already got the `experiments` folder at
```
$OPENCARP_ROOT/external/experiments
```

Otherwise, you can clone the `experiments` directory on your machine by using the following command:
```
git clone https://git.opencarp.org/openCARP/experiments.git
```


### Configuration of the environment

In order to be able to run the test suite, you have to configure your environment:

- In your carputils settings file (usually located at `$HOME/.config/carputils/settings.yaml`), set the `REGRESSION_REF` field to the location of the `regression-references` folder.
For example:
  ```
  REGRESSION_REF: "$HOME/experiments/regression-references"
  ```

- In your carputils settings file, set the `REGRESSION_PKG` field to:
```
REGRESSION_PKG:
    - devtests
```

- To avoid some possible issues with openMPI, you can also set the `MAX_NP` field in your settings file to the number of _physical_ cores on your machine.

- Add the location of the `experiments/regression` folder to your `PYTHONPATH`.
Typically, you will have to add this line to your `$HOME/.bashrc` or `$HOME/.zshrc`:
  ```
  export PYTHONPATH=$PYTHONPATH:$HOME/experiments/regression
  ```
  
Then ensure that the location is immediately added to your `PYTHONPATH` by sourcing the file:
```
source $HOME/.bashrc
```


### Running the tests

You should now be able to run the tests by running `carptests`. 

If the executable is not in your path, you can find it at
```
$CARPUTILS_DIR/bin/carptests
```
If you installed openCARP via an installation package or if you compiled it using the `-DBUILD_EXTERNAL=ON` CMake option,
carputils should be located in the openCARP installation directory.

