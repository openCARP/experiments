#!/usr/bin/env python3
#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the 
# Apache License, Version 2.0 (the "License"); 
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#

"""
This file generates the file `additional_limpet.atc` located at `experiments/regression/devtests/LIMPET/stim_assign`.

`additional_limpet.atc` describes the regression tests run by autotester for the additional limpet tests on simulations.
"""

import os
import sys
import argparse
from string import Template
from carputils import settings


parser = argparse.ArgumentParser()

parser.add_argument('--keep-output',
                    action='store_true',
                    help='Do not delete temporary simulation output')

args = parser.parse_args()


# Get name of directory containing the current file
dirname, filename = os.path.split(os.path.abspath(__file__))
# Get parent directory of the current file
parentpath = os.path.abspath(os.path.join(dirname, os.pardir))
# Get reference directory
refdirname = settings.config.REGRESSION_REF
# Define imp
imp = 'OHara'
# Get folder of reference solutions
reffolder = refdirname + '/devtests/LIMPET.stim_assign.run/' + imp + '/'


# Header of a test
atc_template_header = '''<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<atc>'''

# First part of a test definition: run the simulation
atc_template_test_header = '''  
  <test name="$imp" group="LIMPET-SA">
    <call>
      <command>python3 $dirname/regression/devtests/LIMPET/stim_assign/LIMPET_SA.py --model=$imp</command>
    </call>
    <validations>'''

atc_template_validation = '''
      <validation>
        <variables>
          <variable name="reference_folder">"$refdirname/devtests/LIMPET.stim_assign.run</variable>
          <variable name="tol">1e-6</variable>
        </variables>
        <description>Checking maximum error between the test result and the reference for one Ionic Model</description>
        <call>
          <command>python3 $dirname/autotester/max_error.py --result $imp/$reffilename --reference $reference_folder$/$imp/$reffilename"</command>
        </call>
      </validation>'''

# Last part of a test definition: delete the folder containing the results of the simulation
# unless keep-output option was set
if args.keep_output:
    atc_template_test_footer = '''
    </validations>
  </test>'''
else:
    # Delete results generated during test
    atc_template_test_footer = '''
      <validation>
        <description>Remove test folder</description>
        <call>
          <command>rm -r $imp</command>
        </call>
      </validation>
    </validations>
  </test>'''

atc_template_footer ='''
</atc>'''


# Open atc file describing autotester regression tests for bidomain simulations
file = open(parentpath+'/regression/devtests/LIMPET/stim_assign/stim_assign.atc', 'w')
file.write(atc_template_header)
atc_test_header = Template(atc_template_test_header)
atc_validation = Template(atc_template_validation)
atc_test_footer = Template(atc_template_test_footer)

# Write each regression test in atc file
for refroot, refdirs, reffiles in os.walk(reffolder):
  for reffile in reffiles:
    file.write(atc_test_header.safe_substitute(imp=imp, dirname=parentpath))
    file.write(atc_validation.safe_substitute(imp=imp, reffilename=reffile, refdirname=refdirname, dirname=parentpath))
    file.write(atc_test_footer.safe_substitute(imp=imp))

file.write(atc_template_footer)
file.close()