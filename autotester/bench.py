#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the 
# Apache License, Version 2.0 (the "License"); 
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#

"""
Some tools for interfacing with the bench executable.
"""

import subprocess


_bench_model_cache = None

def _bench_models():

    global _bench_model_cache

    if _bench_model_cache is not None:
        return _bench_model_cache

    _bench_model_cache = {}

    # Get bench to list its models
    out = subprocess.check_output(['bench',
                                   '--list', '--plugin-outputs'])

    for line in out.decode('utf-8').split('\n'):
        
        if 'Bench ' in line:
            # ignoring unnecessary openCARP output
            continue
            
        if 'Ionic models:' in line:
            current = _bench_model_cache['imp'] = []
            continue

        if 'Plug-ins:' in line:
            current = _bench_model_cache['plugin'] = {}
            continue

        if len(line) == 0:
            continue

        if isinstance(current, list):
            current.append(line.strip())
        else:
            parts = line.split()
            current[parts[0]] = parts[1:]

    return _bench_model_cache

def imps():
    return _bench_models()['imp']

def plugins():
    return _bench_models()['plugin']
