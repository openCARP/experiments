#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the 
# Apache License, Version 2.0 (the "License"); 
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#

import argparse
import pathlib
import sys
import numpy as np
from carputils.carpio import bin
from carputils.carpio import igb
from carputils.testing import check


def max_error(array1, array2):
    return np.max(np.abs(array1 - array2))

def error_norm(array1, array2):
    return np.sqrt(np.sum((array1 - array2)**2))


parser = argparse.ArgumentParser(description='Function to evalute the maximum error')
parser.add_argument('--result', type=str,
                    help='path to the test file')
parser.add_argument('--reference', type=str,
                    help='path to the reference file')
parser.add_argument('--tol', type=float, default=1e-6, nargs='?',
                    help='tolerance to determine if the test passes or fails')

args = parser.parse_args()

data_res = check.load_data(args.result, 'Simulation output')
data_ref = check.load_data(args.reference, 'Reference solution')
error = max_error(data_res, data_ref)

if error<=args.tol:
	print("Deviation (%.2e) smaller than tolerance (%.2e). Passing."%(error,args.tol))
	sys.exit(0)
else:
	print("Deviation (%.2e) bigger than tolerance (%.2e). Failing."%(error,args.tol))
	sys.exit(1)

