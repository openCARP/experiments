### What does this MR do?

<!-- Briefly describe what this MR is about. -->

### Related issues

<!-- Link related issues below. Insert the issue link or reference after the word "Closes" if merging this should automatically close it. -->

### Author's checklist

- [ ] Follow the [Contribution Guidelines](https://opencarp.org/community/contribute).
- [ ] Apply the appropriate labels.
- [ ] Does this commit add new dependencies (e.g. Python modules)? If yes, please evaluate carefully if the additional dependency is a more sustainable option than including the functionality in own code. If the former, describe the dependency clearly, add them to the Software Management Plan and the installation instructions (*.md files, CMake files, Docker files etc.).

### Reviewer's checklist

All reviewers can help ensure accuracy, clarity, completeness, and adherence to the [Contribution Guidelines](https://opencarp.org/community/contribute).

**Reviewer**

- [ ] Review by a code reviewer or other selected colleague to confirm accuracy, clarity, and completeness. This can be skipped for minor fixes without substantive content changes.

**Maintainer**

- [ ] Review by assigned maintainer, who can always request/require the above reviews.
- [ ] Ensure a release milestone is set.

